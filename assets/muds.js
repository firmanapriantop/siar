function CallModal(page, bgmodal, modal) {
  $.ajax({
    url: page,
    beforeSend: function () {
      //showLoading();
      //$('#loader').fadeOut(1000);
    },
    success: function (response) {
      $("#" + bgmodal).html(response);
      $("#" + modal).modal("show");
    },
    dataType: "html",
  });
}

function LoadData(page, id) {
  $.ajax({
    url: page,
    beforeSend: function () {
      //showLoading2(id);
    },
  }).done(function (data) {
    $("#" + id).html(data);
  });
}

notif = function () {
  $.notify("You can not close me!");
};
