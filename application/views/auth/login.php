<!-- BEGIN LOGO -->
<div class="logo">
	<h4><strong>SISTEM INFORMASI ANGGARAN</strong></h4>
	<h4><strong>(SIAR)</strong></h4>
</div>
<!-- END LOGO -->
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGIN -->
<div class="content">
	<!-- BEGIN LOGIN FORM -->
	<form action="<?= base_url('auth'); ?>" method="post">
		<h3 class="form-title">Login to your account</h3>
		<?= $this->session->flashdata('message'); ?>
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label visible-ie8 visible-ie9">Email</label>
			<div class="input-icon">
				<i class="fa fa-user"></i>
				<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" id="email" value="<?= set_value('email'); ?>" />
			</div>
			<?= form_error('email',  '<small class="text-danger">', '</small>'); ?>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Password</label>
			<div class="input-icon">
				<i class="fa fa-lock"></i>
				<input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" id="password" />
			</div>
			<?= form_error('password',  '<small class="text-danger">', '</small>'); ?>
		</div>
		<div class="form-actions">
			<button type="submit" class="btn blue pull-right">Login <i class="m-icon-swapright m-icon-white"></i></button>
		</div>
		<!--<div class="forget-password">
			<h4>Forgot your password ?</h4>
			<p>
				 no worries, click <a href="javascript:;" id="forget-password">
				here </a>
				to reset your password.
			</p>
		</div> -->
	</form>
	<!-- END LOGIN FORM -->
	<!--<div class="create-account">
			<p>
				 Don't have an account yet ?&nbsp; <a href="<?= base_url('auth/registration'); ?>" id="register-btn">
				Create an account </a>
			</p>
		</div>
	<!-- BEGIN FORGOT PASSWORD FORM -->
	<form class="forget-form" action="index.html" method="post">
		<h3>Forget Password ?</h3>
		<p>
			Enter your e-mail address below to reset your password.
		</p>
		<div class="form-group">
			<div class="input-icon">
				<i class="fa fa-envelope"></i>
				<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" />
			</div>
		</div>
		<div class="form-actions">
			<button type="button" id="back-btn" class="btn">
				<i class="m-icon-swapleft"></i> Back </button>
			<button type="submit" class="btn blue pull-right">
				Submit <i class="m-icon-swapright m-icon-white"></i>
			</button>
		</div>
	</form>
	<!-- END FORGOT PASSWORD FORM -->
</div>
<!-- END LOGIN -->