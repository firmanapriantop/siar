<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
		<div class="m-alert__icon">
			<i class="flaticon-exclamation m--font-brand"></i>
		</div>
		<div class="m-alert__text">
			This example is almost identical to text based individual column example and provides the same functionality.
			With server-side processing enabled, all paging, searching, ordering actions that DataTables performs are handed off to a server where an SQL engine (or similar) can perform these actions on the large data set.
			See official documentation <a href="https://datatables.net/examples/api/multi_filter_select.html" target="_blank">here</a>.
		</div>
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Data
					</h3>
				</div>
			</div>
			
		</div>
		<div class="m-portlet__body" id="tabelx">

			<!--begin: Datatable -->
			
		</div>
	</div>

	<!-- END EXAMPLE TABLE PORTLET-->
</div>
<!--begin::Modal-->


<script src="<?= base_url()?>/theme/vendors/jquery/dist/jquery.js" type="text/javascript"></script>
<script src="<?= base_url()?>/assets/muds.js" type="text/javascript"></script>

<script src="<?= base_url()?>/theme/assets/jquery.dataTables.min.js"></script>
<script src="<?= base_url()?>/theme/assets/dataTables.buttons.min.js"></script>
    <script src="<?= base_url()?>/theme/assets/buttons.flash.min.js"></script>
    <script src="<?= base_url()?>/theme/assets/jszip.min.js"></script>
    <script src="<?= base_url()?>/theme/assets/pdfmake.min.js"></script>
    <script src="<?= base_url()?>/theme/assets/vfs_fonts.js"></script>
    <script src="<?= base_url()?>/theme/assets/buttons.html5.min.js"></script>
    <script src="<?= base_url()?>/theme/assets/buttons.print.min.js"></script>



<script>
    $(document).ready(function(){
				LoadData("<?= base_url('laporan_perijinan_pestisida/show_data') ?>", "tabelx");
    });
</script> 


