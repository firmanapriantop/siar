<table class="table table-striped- table-bordered table-hover" id="tabel">
    <thead>
					<tr>
							<th></th>
							<th>No. Pendaftaran</th>
							<th>Nama</th>
							<th>Jenis Formula</th>
							<th>Bidang Penggunaan</th>
							<th>Perusahaan</th>							
							<th>Kategori</th>
							<th>Jenis Ijin</th>
							<th>No. Ijin / SK Menteri</th>
							<th>Tanggal Expired</th>
							<th>Expired</th>
							<th></th>
						</tr>
					</thead>
					
					<tbody>


		</tbody>
					
</table>




<script>
	$(document).ready(function(){
		
		$('#tabel thead tr').clone(true).appendTo( '#tabel thead' );
        $('#tabel thead tr:eq(1) th').each( function (i) {
			var title = $(this).text();
			switch(title){
				case"No. Pendaftaran":
				case"Nama":
				case"Jenis Formula":
				case"Bidang Penggunaan" :
				case"Kategori":
				case"Jenis Ijin":
				case"Perusahaan":
			    case"No. Ijin / SK Menteri":
				case "Tanggal Expired":
				case "Expired":
					$(this).html( '<input type="text" class="form-control form-control-sm form-filter m-input" placeholder="Search '+title+'" />' );
				break;
			};

			$( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                }
			});
        });
		
		 var table = $('#tabel').DataTable({
			
			serverSide: false,
			//processing: true,
			fixedHeader: true,
			ajax: {
				type: "POST",
				url: '<?= base_url('laporan_perijinan_pestisida/get_data_by_json') ?>',
				data: { typ: 1 },
				dataType: "json",
				dataSrc: function (result) {
					var json = result;
					return json;
				}
			}
			,
			columns: [
				{ data: "perijinanid" },
				{ data: "no_pendaftaran" },
				{ data: "nama" },
				{ data: "jenis" },
				{ data: "bidang" },
				{ data: "perusahaan" },
				{ data: "text_kategori" },
				{ data: "kategori_ijin" },
				{ data: "no_ijin" },
				{ data: "tgl_akhir_ijin" },
				{ data: "expired" }
			],
			orderCellsTop: true,
            responsive:!0,
            "columnDefs": [{
                "targets"  : -1,
                "orderable": false
            }],
			dom: 'lBfrtip',
			buttons: [
			 {
					extend: 'pdfHtml5',
					orientation: 'landscape',
					pageSize: 'LEGAL',
					title: 'Daftar Usulan Tahun '
				},
				'excelHtml5',
				'print'
			],
		});
		
		
		
	
  });
</script>
