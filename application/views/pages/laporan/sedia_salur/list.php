<table class="table table-striped- table-bordered table-hover" id="tabel">
    <thead>
						<tr>							
							<th>Tahun</th>						
							<th>Nama Formulasi</th>
							<th>Perusahaan</th>
							<th>No. Pendaftaran</th>
							<th>No. Ijin / SK Menteri</th>
							<th>Jenis Formulasi</th>							
							<th>Sisa Stock Sebelumnya</th>
							<th>Pengadaan</th>
							<th>Penyaluran</th>
							<th>Sisa Stock</th>
							<th>Satuan</th>
							<th>Expired</th>
						</tr>
					</thead>
					
					<tbody>


		</tbody>
					
</table>




<script>
	$(document).ready(function(){
		
		$('#tabel thead tr').clone(true).appendTo( '#tabel thead' );
        $('#tabel thead tr:eq(1) th').each( function (i) {
			var title = $(this).text();
			switch(title){
				
				case"Tahun":
				case"No. Pendaftaran":
				case"Nama Formulasi":
				case"Jenis Formulasi":
				case"Sisa Stock Sebelumnya" :
				case"Pengadaan":
				case"Penyaluran":
				case"Perusahaan":
				case"Sisa Stock":
				case"Satuan":
			    case"No. Ijin / SK Menteri":
				case "Expired":
					$(this).html( '<input type="text" class="form-control form-control-sm form-filter m-input" placeholder="Search '+title+'" />' );
				break;
			};

			$( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                }
			});
        });
		
		 var table = $('#tabel').DataTable({
			
			serverSide: false,
			//processing: true,
			fixedHeader: true,
			ajax: {
				type: "POST",
				url: '<?= base_url('laporan_sedia_salur/get_data_by_json') ?>',
				data: { typ: 1 },
				dataType: "json",
				dataSrc: function (result) {
					var json = result;
					return json;
				}
			}
			,
			columns: [
				{ data: "tahun" },
				{ data: "nama" },
				{ data: "perusahaan" },
				{ data: "no_pendaftaran" },
				{ data: "no_ijin" },
				{ data: "jenis" },
				{ data: "stockawal" },
				{ data: "pengadaan" },
				{ data: "penyaluran" },
				{ data: "stockakhir" },
				{ data: "satuan" },
				{ data: "tgl_akhir_ijin" },
			],
			orderCellsTop: true,
            responsive:!0,
            "columnDefs": [{
                "targets"  : -1,
                "orderable": false
            }],
			dom: 'lBfrtip',
			buttons: [
			 {
					extend: 'pdfHtml5',
					orientation: 'landscape',
					pageSize: 'LEGAL',
					title: 'Daftar Usulan Tahun '
				},
				'excelHtml5',
				'print'
			],
		});
		
		
		
	
  });
</script>
