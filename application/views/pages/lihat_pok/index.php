<h3 class="page-title">
	Revisi POK <small>blank page</small>
</h3>
<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">FILTER </span>
				</div>
				
			</div>
			<div class="portlet-body form">
				<form class="form-horizontal" role="forms">
					<div class="form-body">
						<div class="form-group">
							<label class="col-md-3 control-label">Pilih Tahun RKA *</label>
							<div class="col-md-9">
								<select class="form-control" id="m_rka" name="m_rka">
									<?php foreach ($rka->result() as $i) : ?>
										<option value="<?= $i->tahun ?>"><?= $i->tahun ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3 control-label">Pilih Kegiatan *</label>
							<div class="col-md-9">
								<select class="form-control" id="m_kegiatan" name="m_kegiatan">
									<option></option>
									<?php foreach ($kegiatan->result() as $k) : ?>
										<option value="<?= $k->idgiat ?>"><?= $k->kdgiat . "-" . $k->nmgiat ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						
					</div>

				</form>
				<div class="form-actions">
					<div class="row">
						<div class="col-md-offset-3 col-md-9">
							<button type="button" class="btn btn-success" id="btn_tampil">Tampilkan</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12" id="tabelx1">

		</div>
	</div>


	<!-- <script src="<?= base_url() ?>assets/muds.js" type="text/javascript"></script> -->

	<script>
		 $(document).ready(function(){
			 
			 $('#m_rka').select2({
				placeholder: "Pilih Tahun",
				allowClear: true
			});


			$('#m_kegiatan').select2({
				placeholder: "Pilih Kegiatan",
				allowClear: true
			});
			 
			 $('#btn_tampil').click(function() {

				
				//console.log(idkomponen.length);
				//if (idkomponen.length == 0) {
				//	alert('Komponen belum dipilih.');
				//} else {
				//	var link = "<?= base_url('lihat_pok/show_data?x=') ?>" + idgiat + "+y=" + kdrka;

					LoadData("<?= base_url('lihat_pok/show_data') ?>", "tabelx1");;


				//}

			});
			//	LoadData("<?= base_url('lihat_pok/show_data') ?>", "tabelx");
    });
	</script>