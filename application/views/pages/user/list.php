<!--begin: Datatable -->
<table class="table table-striped- table-bordered table-hover" id="tabel">
    <thead>
        <tr>
            <th></th>
            <th>Nama</th>
            <th>Email</th>
            <th>No HP</th>
            <th>Status</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 0;
        foreach ($data->result() as $row) : $no++; ?>
            <tr>
                <td><?= $no; ?></td>
                <td><?= $row->fullname ?></td>
                <td><?= $row->email; ?></td>
                <td><?= $row->no_hp; ?></td>
                <td class="text-center">
                    <?php
                    if ($row->is_active == 1) {
                        echo "<span class='label label-success'>Akif</span>";
                    } else {
                        echo "<span class='label label-danger'>Non Akif</span>";
                    }

                    ?>
                </td>
                <td class="text-center">
                    <div class="btn-group btn-group-xs">
                        <button type="button" class="btn btn-success" onclick="CallModal('user/edit/<?= $row->userid; ?>', 'tmpModal', 'modalEdit')"><i class="fa fa-edit"></i></button>
                        <button type="button" class="btn btn-danger" onclick="hapus('<?= $row->userid ?>')"><i class="fa fa-trash-o"></i></button>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>


<script>
    function hapus(id) {
        swal({
                title: "Hapus Pengguna",
                text: "Apakah Anda yakin untuk menghapus pengguna ini?",
                type: "warning",
                showCancelButton: true,
                cancelButtonClass: "btn-danger",
                cancelButtonText: "Tidak",
                confirmButtonText: "Ya",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: '<?= base_url() ?>user/hapus',
                        type: 'post',
                        data: {
                            userid: id
                        },
                        error: function() {
                            alert('Something is wrong');
                        },
                        success: function(data) {
                            var link = "user/show_data";
                            LoadData(link, "tabelx");
                            swal("", "User berhasil dinonaktifkan", "success");
                        }
                    });
                } else {
                    swal("", "Hapus dibatalkan", "error");
                }
            });
    }

    $(document).ready(function() {
        $('#tabel thead tr').clone(true).appendTo('#tabel thead');
        $('#tabel thead tr:eq(1) th').each(function(i) {
            var title = $(this).text();
            switch (title) {
                case "Nama":
                case "Email":
                case "No HP":
                case "Status":
                    $(this).html('<input type="text" class="form-control form-control-sm form-filter m-input" placeholder="Search ' + title + '" />');
                    break;
            };

            $('input', this).on('keyup change', function() {
                if (table.column(i).search() !== this.value) {
                    table
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });
        });

        var table = $('#tabel').DataTable({
            orderCellsTop: true,
            fixedHeader: true,
            responsive: !0,
            "columnDefs": [{
                "targets": -1,
                "orderable": false
            }]
        });



        $('#confirm-delete').on('show.bs.modal', function(e) {
            // get information to update quickly to modal view as loading begins
            var opener = e.relatedTarget; //this holds the element who called the modal

            //we get details from attributes
            var id = $(opener).attr('data-id');
            var fullname = $(opener).attr('full-name');

            $('.title').text(' ' + fullname + ' ');
            $('.confirm-hapus').data('id', id);
        });

        $('.confirm-hapus').click(function() {
            var ID = $(this).data('id');
            $.ajax({
                url: "<?php echo base_url(); ?>dokumen_koordinasi/delete/" + ID,
                type: "post",
                data: ID,
                success: function(data) {
                    $("#confirm-delete").modal('hide');
                    //$("#deleted").modal('show');
                    LoadData("<?php echo base_url(); ?>dokumen_koordinasi/show_data", "tabelx");
                }
            });
        });
    });
</script>