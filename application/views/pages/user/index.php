</br>
<h3 class="page-title">
	User <small>Daftar Pengguna</small>
</h3>
<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">Data </span>
				</div>
				<div class="tools">
					<?php if ($is_admin) { ?>
						<button type="button" class="btn btn-success" id="btn_add"><i class="fa fa-plus-square"></i> Tambah</button>
					<?php } ?>
				</div>
			</div>
			<div class="portlet-body" id="tabelx">
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		LoadData("<?= base_url('user/show_data') ?>", "tabelx");

		$('#btn_add').click(function() {
			CallModal('<?= base_url() ?>user/add', 'tmpModal', 'modalEdit')
		});
	});
</script>