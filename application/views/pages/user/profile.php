<div class="m-subheader ">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h3 class="m-subheader__title text-capitalize">My Profile</h3>
		</div>
        <div>
            <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                    <i class="la la-plus m--hide"></i>
                    <i class="la la-ellipsis-h"></i>
                </a>
                <div class="m-dropdown__wrapper">
                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                    <div class="m-dropdown__inner">
                        <div class="m-dropdown__body">
                            <div class="m-dropdown__content">
                                <ul class="m-nav">
                                    <li class="m-nav__section m-nav__section--first m--hide">
                                        <span class="m-nav__section-text"></span>
                                    </li>
                                    <li class="m-nav__item">
                                        <a href="" class="m-nav__link">
                                            <i class="m-nav__link-icon flaticon-share"></i>
                                            <span class="m-nav__link-text">Edit Profile</span>
                                        </a>
                                    </li>
                                    <li class="m-nav__item">
                                        <a href="" class="m-nav__link">
                                            <i class="m-nav__link-icon flaticon-chat-1"></i>
                                            <span class="m-nav__link-text">Change Password</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>


<!-- END: Subheader -->
<div class="m-content">
    <div class="row">
        <div class="col-md-4">
            <div class="m-portlet m-portlet--full-height  ">
                <div class="m-portlet__body">
                    <div class="m-card-profile">
                        <div class="m-card-profile__title m--hide">
                            Your Profile
                        </div>
                        <div class="m-card-profile__pic">
                            <div class="m-card-profile__pic-wrapper">
                                <img src="<?= base_url('assets/img/profile/'.$hasil->image.''); ?>" alt="">
                            </div>
                        </div>
                        <div class="m-card-profile__details">
                            <span class="m-card-profile__name"><?= $hasil->fullname ?></span>
                            <a href="" class="m-card-profile__email m-link"><?= $hasil->email ?></a>
                        </div>
                    </div>
                    <div class="m-portlet__body-separator"></div>
                    <div class="m-widget1 m-widget1--paddingless">
                        <div class="m-widget1__item">
                            <div class="row m-row--no-padding align-items-center">
                                <div class="col">
                                    <h3 class="m-widget1__title">Role</h3>
                                    <span class="m-widget1__desc"><?= $hasil->role ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="m-widget1__item">
                            <div class="row m-row--no-padding align-items-center">
                                <div class="col">
                                    <h3 class="m-widget1__title">Perusahaan</h3>
                                    <span class="m-widget1__desc"><?= $hasil->perusahaan ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="m-widget1__item">
                            <div class="row m-row--no-padding align-items-center">
                                <div class="col">
                                    <h3 class="m-widget1__title">No Seluler</h3>
                                    <span class="m-widget1__desc"><?= $hasil->no_hp ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="m-widget1__item">
                            <div class="row m-row--no-padding align-items-center">
                                <div class="col">
                                    <h3 class="m-widget1__title">Created Date</h3>
                                    <span class="m-widget1__desc"><?= $hasil->date_created ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            
            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Edit Profile
                            </h3>
                        </div>
                    </div>
                </div>
                <form class="m-form m-form--fit m-form--label-align-right" id="FormProfile" method="post" action="#">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <label for="fullname">Full Name</label>
                            <input class="form-control m-input" type="text" value="<?= $hasil->fullname ?>" name="fullname" id="fullname">
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="email">Email</label>
                            <input class="form-control m-input" type="text" value="<?= $hasil->email ?>" name="email" id="email">
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="no_hp">Phone No.</label>
                            <input class="form-control m-input" type="text" value="<?= $hasil->no_hp ?>" name="no_hp" id="no_hp">
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="perusahaanid">Perusahaan</label>
                            <select class="form-control m-select2" id="perusahaanid" name="perusahaanid">
											
                                <option value="<?= $hasil->perusahaanid ?>"><?= $hasil->perusahaan ?></option>
                                <?php foreach($get_perusahaan->result() as $data){ ?>
                                    <option value="<?php echo $data->perusahaanid; ?>"><?php echo $data->nama; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <!-- 
                        <div class="form-group m-form__group row">
                            <label for="image">Image Profile</label>
                            <input class="form-control m-input" type="text" value="<?php //= $hasil->image ?>" name="image" id="image">
                        </div>
                        -->
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions">
                            <button type="submit" class="btn btn-success">Save Profile</button>
                        </div>
                    </div>
                </form>
                
            </div>
        </div>

        <div class="col-md-4">
            
            <div class="m-portlet m-portlet--full-height">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Change Password
                            </h3>
                        </div>
                    </div>
                </div>

                
                <form class="m-form m-form--fit m-form--label-align-right" id="FormPassword" method="post" action="#">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <label for="exampleInputEmail1">Old Password</label>
                            <input type="password" class="form-control m-input" id="old_pass" placeholder="Old Password">
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="exampleInputPassword1">New Password</label>
                            <input type="password" class="form-control m-input" id="new_pass" placeholder="New Password">
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="exampleInputPassword1">Confirm New Password</label>
                            <input type="password" class="form-control m-input" id="confirmed_pass" placeholder="New Password">
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions">
                            <button type="submit" class="btn btn-primary">Change Password</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

<script src="<?= base_url()?>/theme/vendors/jquery/dist/jquery.js" type="text/javascript"></script>
<script src="<?= base_url()?>/assets/muds.js" type="text/javascript"></script>

<script>
    
    function get_kab(provid){
        $.ajax({
            type: "post",
            url: "<?= base_url() ?>user/get_data_kab_by_provid/"+provid,
            success: function (response) {
                //get_csrf();
                
                //console.log(response);
                var data= JSON.parse(response);
                
                var $el = $("#kabid");
                $el.empty(); // remove old options
                $el.append('<option value="">Pilih Kabupaten</option>');
                $.each(data,function(i,value){
                    $el.append('<option value="'+value.kabid+'">'+value.name+'</option>');
                });
                
            }
        });	
    }  
        
    $(document).ready(function(){

        $('#perusahaanid').select2();

        $('#provid').change(function(e){
            get_kab($(this).val());
        });

        $('#FormProfile').submit(function(e) {
			e.preventDefault();

			var me 			    = $(this);
            
            //var userid      = $("#userid").val();
            var fullname        = $("#fullname").val();
            var email           = $("#email").val();
            var no_hp           = $("#no_hp ").val();
            var perusahaanid    = $("#perusahaanid").val();
           // var image             = $("#image").val();
			
            var form_data 	= new FormData();
            
            //form_data.append('userid', userid);
            form_data.append('fullname', fullname);
            form_data.append('email', email);
            form_data.append('no_hp', no_hp);
            form_data.append('perusahaanid', perusahaanid);
            //form_data.append('image', image);
    
            $.ajax({
                url: '<?= base_url('user/profile_save') ?>',
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(response){
                    if (response.success == true) {
						swal("Good job!");
						
					}
					else {
                        console.log(response);
						$.each(response.messages, function(key, value) {
							var element = $('#' + key);
							
							element.closest('div.form-group')
							.removeClass('has-error')
							.addClass(value.length > 0 ? 'has-error' : 'has-success')
							.find('.text-danger')
							.remove();

							element.after(value);
						});
					}
                }
            });
        });

        $('#FormPassword').submit(function(e) {
			e.preventDefault();

			var me 			    = $(this);
            
            var old_pass       = $("#old_pass").val();
            var new_pass       = $("#new_pass").val();
            var confirmed_pass = $("#confirmed_pass ").val();
			
            var form_data 	= new FormData();
            
            form_data.append('old_pass', old_pass);
            form_data.append('new_pass', new_pass);
            form_data.append('confirmed_pass', confirmed_pass);
    
            $.ajax({
                url: '<?= base_url('user/profile_save_pass') ?>',
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(response){
                    if (response.success == true) {
						swal("Password has been changed!");
					}
					else {
                        console.log(response);
						$.each(response.messages, function(key, value) {
							var element = $('#' + key);
							
							element.closest('div.form-group')
							.removeClass('has-error')
							.addClass(value.length > 0 ? 'has-error' : 'has-success')
							.find('.text-danger')
							.remove();

							element.after(value);
						});
					}
                }
            });
        });
		
	});
</script> 