<div class="modal fade" id="modalEdit" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tambah Pengguna</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i aria-hidden="true" class="ki ki-close"></i>
				</button>
			</div>
			<!--begin::Form-->
			<form id="form_modal" method="post">
				<div class="modal-body">
					<div class="form-group" id='err_div_fullname'>
						<label>Nama Lengkap *</label>
						<input type="text" class="form-control" placeholder="Isi nama lengkap" name="fullname" id="fullname" required>
						<label class='control-label' id="err_fullname"></label>
					</div>
					<div class="form-group" id='err_div_email'>
						<label>Alamat Surel *</label>
						<input type="email" class="form-control" placeholder="Isi nama surel" name="email" id="email" required>
						<label class='control-label' id="err_email"></label>
					</div>
					<div class="form-group" id='err_div_no_hp'>
						<label>No Seluler *</label>
						<input type="text" class="form-control" placeholder="Isi nomor seluler" name="no_hp" id="no_hp" required>
						<label class='control-label' id="err_no_hp"></label>
					</div>
					<div class="form-group" id='err_div_password'>
						<label>Kata Kunci *</label>
						<input type="password" class="form-control" name="password" id="password" required>
						<label class='control-label' id="err_password"></label>
					</div>
					<div class="form-group" id='err_div_confirm_password'>
						<label>Konfirmasi Kata Kunci *</label>
						<input type="password" class="form-control" name="confirm_password" id="confirm_password" required>
						<label class='control-label' id="err_confirm_password"></label>
					</div>


				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
				</div>
			</form>
			<!--end::Form-->
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {

		$('#form_modal').submit(function(e) {
			e.preventDefault(); //gunanya untuk supaya ga refresh

			var form_data = new FormData();

			form_data.append('fullname', $('#fullname').val());
			form_data.append('email', $('#email').val());
			form_data.append('no_hp', $('#no_hp').val());
			form_data.append('password', $('#password').val());
			form_data.append('confirm_password', $('#confirm_password').val());

			$.ajax({
				url: '<?php echo base_url(); ?>user/save/add',
				dataType: 'json',
				data: form_data,
				type: 'post',
				cache: false,
				contentType: false,
				processData: false,
				success: function(response) {
					console.log(response);
					if (response.success == true) {
						toastr["success"]("Data berhasil disimpan.", "Perhatian");
						$('#modalEdit').modal('hide');
						var link = "user/show_data";
						LoadData(link, "tabelx");
					} else {
						$.each(response.messages, function(nama_field, value) {
							//console.log('nama field: ' + nama_field);
							//console.log('pesan field: ' + value);
							$('#err_div_' + nama_field).addClass('has-error');
							$('#err_' + nama_field).html(value);

						});

					}
				}

			});
		});
	});
</script>