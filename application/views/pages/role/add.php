<div class="modal fade" id="modalEdit" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tambah User Role</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i aria-hidden="true" class="ki ki-close"></i>
				</button>
			</div>
			<!--begin::Form-->
			<form id="form_modal" method="post">
				<div class="modal-body">

					<div class="form-group" id='err_div_user_id'>
						<label class="control-label">Pilih User *</label>
						<div class="input-group col-md-12">
							<select class="form-control select2" id="user_id" name="user_id" required>
								<option></option>
								<?php
								foreach ($user->result() as $row) :
									echo '<option value="' . $row->userid . '">' . $row->fullname . ' (' . $row->email . ')</option>';
								endforeach;
								?>
							</select>
						</div>
						<label class='control-label' id="err_user_id"></label>
					</div>
					<div class="form-group" id='err_div_role'>
						<label class="control-label">Pilih Role *</label>
						<div class="input-group col-md-12">
							<select class="form-control select2" id="role" name="role" required>
								<option></option>
								<option value="staf">Staf</option>
								<option value="kasubdit">Kasubdit</option>
								<option value="pokjacan">Pokjacan</option>
								<option value="direktur">Direktur</option>
								<option value="bagcan">Bagcan</option>
							</select>
						</div>
						<label class='control-label' id="err_role"></label>
					</div>
					<div class="form-group" id='err_div_unit_kerja_id'>
						<label>Pilih Unit Kerja *</label>
						<div class="input-group col-md-12">
							<select id="unit_kerja_id" name="unit_kerja_id" class="form-control select2">
								<option></option>
								<?php
								foreach ($unit_kerja->result() as $r) :
									echo '<option value="' . $r->id . '">' . $r->nama . '</option>';
								endforeach;
								?>
							</select>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
				</div>
			</form>
			<!--end::Form-->
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {

		$('#user_id').select2({});

		$('#unit_kerja_id').select2({});

		$('#role').select2({});

		$('#role').change(function() {
			var value = $(this).val();
			if (value == 'direktur' || value == 'bagcan') {
				$('#unit_kerja_id').val(null).trigger('change');
				$('#unit_kerja_id').select2("enable", false);
			} else {
				$('#unit_kerja_id').select2("enable");
			}
		});

		$('#form_modal').submit(function(e) {
			e.preventDefault(); //gunanya untuk supaya ga refresh

			var form_data = new FormData();

			form_data.append('user_id', $('#user_id').val());
			form_data.append('role', $('#role').val());
			form_data.append('unit_kerja_id', $('#unit_kerja_id').val());

			$.ajax({
				url: '<?php echo base_url(); ?>role/save/add',
				dataType: 'json',
				data: form_data,
				type: 'post',
				cache: false,
				contentType: false,
				processData: false,
				success: function(response) {
					if (response.success == true) {
						toastr["success"]("Data berhasil disimpan.", "Perhatian");
						$('#modalEdit').modal('hide');
						var link = "role/load_role";
						LoadData(link, "tabelx");
					} else {
						$.each(response.messages, function(nama_field, value) {
							//console.log('nama field: ' + nama_field);
							//console.log('pesan field: ' + value);
							$('#err_div_' + nama_field).addClass('has-error');
							$('#err_' + nama_field).html(value);

						});

					}
				}

			});
		});
	});
</script>