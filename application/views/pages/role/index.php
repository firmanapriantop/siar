</br>
<h3 class="page-title">
	User Role <small>Manajamen peran pengguna</small>
</h3>
<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">Data </span>
				</div>
				<div class="tools">
					<button type="button" class="btn btn-success" id="btn_add"><i class="fa fa-plus-square"></i> Tambah</button>
				</div>
			</div>
			<div class="portlet-body" id="tabelx">
			</div>
		</div>
	</div>
</div>

<script src="<?= base_url() ?>assets/muds.js" type="text/javascript"></script>

<script>
	$(document).ready(function() {

		LoadData('<?= base_url('role/load_role') ?>', "tabelx");

		$("#pilihbanyak").select2({});
		$("#pilihbanyak").on('change', function(e) {
			console.log($('#pilihbanyak').val());
			alert($('#pilihbanyak').val());

		});

		$('#btn_add').click(function() {
			CallModal('<?= base_url() ?>role/add_role', 'tmpModal', 'modalEdit')
		});

		$('#btn_unsubmit_kasubdit').click(function(e) {
			swal({
					title: "Unsubmit",
					text: "Apakah Anda yakin untuk Unsubmit Hasil Penelaahan?",
					type: "warning",
					showCancelButton: true,
					cancelButtonClass: "btn-danger",
					cancelButtonText: "Tidak",
					confirmButtonText: "Ya",
					closeOnConfirm: false,
					closeOnCancel: false
				},
				function(isConfirm) {
					if (isConfirm) {
						$.ajax({
							url: '<?= base_url() ?>revisi_pok/ubah_state',
							type: 'post',
							data: {
								aksi: '3'
							},
							error: function() {
								alert('Something is wrong');
							},
							success: function(data) {

								console.log(data);
								swal("", "Unsubmit Kasubdit berhasil dilakukan.", "success");
							}
						});
					} else {
						swal("", "Unsubmit dibatalkan", "error");
					}
				});
		});


		$('#btn_tampilkan').click(function() {

			var idkomponen = $('#m_komponen').val();
			var kdrka = $('#m_rka').val();
			var kdprogram = $('#m_program').val();
			console.log(idkomponen.length);
			if (idkomponen.length == 0) {
				alert('Komponen belum dipilih.');
			} else {
				var link = "revisi_pok/load_sub_komponen/" + idkomponen + "/" + kdrka + "/" + kdprogram;

			}

		});

	});
</script>