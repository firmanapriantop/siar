<?php //print_r($get_kab);
	$nama = $data->nama;
	$email = $data->email;
	$telp = $data->no_telp;
	$fax = $data->no_fax;
	$provid = $data->provid;
	$kabid = $data->kabid;
	$kodepos = $data->kode_pos;
	$alamat = $data->alamat;
	$id = $data->perusahaanid;
?>

<div class="m-content">

						<!--begin::Portlet-->
						<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											Tambah Perusahan
										</h3>
									</div>
								</div>
							</div>

							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" id="m_form_1" method="POST" action="<?php echo base_url(); ?>perusahaan/save/<?=$id?>">
								<div class="m-portlet__body">
									<div class="m-form__content">
										<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_1_msg">
											<div class="m-alert__icon">
												<i class="la la-warning"></i>
											</div>
											<div class="m-alert__text">
												Silahkan perbaiki dan lengkapi data anda!
											</div>
											<div class="m-alert__close">
												<button type="button" class="close" data-close="alert" aria-label="Close">
												</button>
											</div>
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Nama Perusahaan *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<input type="text" class="form-control m-input" name="nama" placeholder="Nama Perusahaan" data-toggle="m-tooltip" value="<?=$nama?>">
											<span class="m-form__help">Isi nama perusahaan</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Email *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<input type="text" class="form-control m-input" name="email" placeholder="Email" data-toggle="m-tooltip" value="<?=$email?>">
											<span class="m-form__help">Isi alamat email</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">No. Telepon *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<div class="input-group">
												<input type="text" class="form-control m-input" name="phone" placeholder="No. Telepon" value="<?=$telp?>">
												<div class="input-group-append"><a href="#" class="btn btn-brand"><i class="la la-phone"></i></a></div>
											</div>
											<span class="m-form__help">Isi no. telepon</span>
										</div>
									</div><div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">No. Fax</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<div class="input-group">
												<input type="text" class="form-control m-input" name="fax" placeholder="No. Fax"value="<?=$fax?>">
												<div class="input-group-append"><a href="#" class="btn btn-brand"><i class="la la-fax"></i></a></div>
											</div>
											<span class="m-form__help">Isi no. fax</span>
										</div>
									</div>
									
									<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Alamat Lengkap *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<textarea class="form-control m-input" name="alamat" placeholder="Alamat Lengkap"><?=$alamat?></textarea>
											<span class="m-form__help">Isi Alamat Lengkap</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Provinsi *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<select class="form-control m-select2" id="m_select2_1" name="provinsi">
											
												<option value="">Pilih Provinsi</option>
												<?php foreach($get_prov as $row => $data){ ?>
													<option value="<?php echo $data['provid']; ?>"  <?php echo ($data['provid']==$provid ? 'selected' : '');?>><?php echo $data['name']; ?></option>
												<?php } ?>
											</select>
											<span class="m-form__help">Pilih Provinsi</span>
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Kabupaten *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<select class="form-control m-select2" id="m_select2_2" name="kabupaten">
											
												<option value="">Pilih Kabupaten</option>
												<?php foreach($get_kab as $row => $data){ ?>
													<option value="<?php echo $data['kabid']; ?>"  <?php echo ($data['kabid']==$kabid ? 'selected' : '');?>><?php echo $data['name']; ?></option>
												<?php } ?>
											</select>
											<span class="m-form__help">Pilih Kabupaten</span>
										</div>
									</div>
									
								</div>
								
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Kode Pos </label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<input type="text" class="form-control m-input" name="kodepos" placeholder="Kode Pos" data-toggle="m-tooltip" value="<?=$kodepos?>">
											<span class="m-form__help">Isi kode pos</span>
										</div>
									</div>
								
								
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-success">Simpan</button>
												<button type="reset" class="btn btn-secondary" id="batal">Batal</button>
											</div>
										</div>
									</div>
								</div>
							</form>

							<!--end::Form-->
						</div>
</div>


<script src="<?= base_url()?>/theme/vendors/jquery/dist/jquery.js" type="text/javascript"></script>

<script  type="text/javascript">

//==untuk validasi form
var FormControls = function () {
    //== Private functions
 
	var demo1 = function () {
        $( "#m_form_1" ).validate({
            // define validation rules
            rules: {
				alamat: {
                    required: true                     
                },
                nama: {
                    required: true 
                },
                 email: {
                    required: true,
                    email: true
                },
				phone: {
                    required: true
                },
                provinsi: {
                    required: true
                },
                kabupaten: {
                    required: true
                }
            },
            
			//override massage
			messages: {
				nama: "Silahkan isi nama perusahaan",
				
				alamat: "Silahkan isi alamat lengkap",
				provinsi: "Silahkan pilih provinsi",
				kabupaten: "Silahkan pilih kabupaten",
				email: "Silahkan isi email yg valid ",
				phone: "Silahkan isi nomor telepon",
			},
			
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                var alert = $('#m_form_1_msg');
                alert.removeClass('m--hide').show();
                mUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
				
            }
        });       
    }
 
		
	return {
        // public functions
        init: function() {
            demo1(); 
        }
    };
}();

//== untuk option with search
var Select2 = function() {
    //== Private functions
    var demos = function() {

        // basic
        $('#m_select2_1').select2({
            placeholder: "Pilih Provinsi",
            allowClear: true
        });

		// basic
        $('#m_select2_2').select2({
            placeholder: "Pilih Kabupaten",
            allowClear: true
        });
			
		
    }


    //== Public functions
    return {
        init: function() {
            demos();
        }
    };
}();



	$(document).ready(function(){
		function get_kab($id,$param){
			$.ajax({
						type: "get",
						url: "<?= base_url() ?>user/get_data_by_id_json",
						data: "id=" + $id + "&param=" + $param,
						success: function (response) {
							//get_csrf();
							
							//console.log(response);
							var data= JSON.parse(response);
							
							if($param=='kabupaten'){
								//alert('test');
								var $el = $("#m_select2_2");
								$el.empty(); // remove old options
								$el.append('<option value="none">Pilih Kabupaten</option>');
								$.each(data,function(i,value){
									$el.append('<option value="'+value.kabid+'">'+value.name+'</option>');
								});
							}else if ($param=='kecamatan'){
								var $el = $("#m_kecamatan");
								$el.empty(); // remove old options
								$el.append('<option value="none">Pilih Kecamatan</option>');
								$.each(data,function(i,value){
									$el.append('<option value="'+value.kecid+'">'+value.name+'</option>');
								});
							}else if ($param=='desa'){
								var $el = $("#m_desa");
								$el.empty(); // remove old options
								$el.append('<option value="none">Pilih Desa</option>');
								$.each(data,function(i,value){
									$el.append('<option value="'+value.desaid+'">'+value.name+'</option>');
								});
							};
						}
					});
			
		}
		//panggil list kabupaten
		$('#m_select2_1').change(function(e){
			var thisId=$(this).val();
			//alert(thisId);
			
			get_kab(thisId,"kabupaten")
			
		});
		
		$('#batal').click(function(){
			window.location.href='<?= base_url() ?>perusahaan';
		});
		
		FormControls.init();
		Select2.init();
        
    });
</script>
