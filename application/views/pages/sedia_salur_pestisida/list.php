<!--begin: Datatable -->
<table class="table table-striped- table-bordered table-hover" id="tabel">
    <thead>
						<tr>
							<th></th>							
							<th>Nama</th>
							<th>No. Pendaftaran</th>
							<th>Jenis Formula</th>
							<th>Bidang Penggunaan</th>
							<th>Perusahaan</th>							
							<th>Kategori</th>
							<th>Jenis Ijin</th>
							<th>No. Ijin / SK Menteri</th>
							<th>Expired</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php $no=0; foreach ($data->result() as $row): $no++; ?>
						<tr>
							<td><?= $no; ?></td>
							<td><?= $row->no_pendaftaran; ?></td>
							<td><?= $row->nama; ?></td>
							<td><?= $row->jenis; ?></td>
							<td><?= $row->bidang; ?></td>
							<td><?= $row->perusahaan; ?></td>
							<td><?= $row->text_kategori; ?></td>
							<td>
								<?= $row->kategori_ijin; ?>
							</td>
							<td>
								<?= $row->no_ijin; ?>
							</td>
							<td>
								<?= $row->tgl_akhir_ijin; ?>
							</td>
							<td>
								<span class="dropdown">
									<a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true"><i class="la la-ellipsis-h"></i></a>
									<div class="dropdown-menu dropdown-menu-right">
										<a class="dropdown-item" href="<?= base_url('perijinan/edit/'.$row->perijinanid) ?>" ><i class="la la-edit"></i> Edit</a>
										<a class="dropdown-item text-danger hapus" href="#" data-toggle="modal" data-target="#confirm-delete" data-id="<?=$row->perijinanid?>" full-name="<?=$row->no_ijin?>"><i class="la 	la-trash text-danger"></i> Hapus</a>
									</div>
								</span>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
</table>



<script>
	$(document).ready(function(){
		$('#tabel thead tr').clone(true).appendTo( '#tabel thead' );
        $('#tabel thead tr:eq(1) th').each( function (i) {
			var title = $(this).text();
			switch(title){
				case"No. Pendaftaran":
				case"Nama":
				case"Jenis Formula":
				case"Bidang Penggunaan" :
				case"Kategori":
				case"Jenis Ijin":
				case"Perusahaan":
			    case"No. Ijin / SK Menteri":
				case "Expired":
					$(this).html( '<input type="text" class="form-control form-control-sm form-filter m-input" placeholder="Search '+title+'" />' );
				break;
			};

			$( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                }
			});
        });
 
        var table = $('#tabel').DataTable({
            orderCellsTop: true,
            fixedHeader: true,
            responsive:!0,
            "columnDefs": [{
                "targets"  : -1,
                "orderable": false
            }],
			processing: true,
		});
		
		
			
		$('#confirm-delete').on('show.bs.modal', function (e) {
			// get information to update quickly to modal view as loading begins
			var opener=e.relatedTarget;//this holds the element who called the modal
			   
			//we get details from attributes
			var id=$(opener).attr('data-id');
			var fullname=$(opener).attr('full-name');
			
			$('.title').text(' ' + fullname + ' ');
			$('.confirm-hapus').data('id', id);
		});
		
		$('.confirm-hapus').click(function(){
			var ID = $(this).data('id');
			//alert(ID);
			$.ajax({
				url: "<?php echo base_url(); ?>sedia_salur_pestisida/delete/"+ID,
				type: "post",
				data:ID,
				success: function (data) {
					$("#confirm-delete").modal('hide');
					//$("#deleted").modal('show');
					LoadData("<?php echo base_url(); ?>sedia_salur_pestisida/show_data", "tabelx");
				}
			  });
		});
  });
</script>
