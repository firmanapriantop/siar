<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
		<div class="m-alert__icon">
			<i class="flaticon-exclamation m--font-brand"></i>
		</div>
		<div class="m-alert__text">
			This example is almost identical to text based individual column example and provides the same functionality.
			With server-side processing enabled, all paging, searching, ordering actions that DataTables performs are handed off to a server where an SQL engine (or similar) can perform these actions on the large data set.
			See official documentation <a href="https://datatables.net/examples/api/multi_filter_select.html" target="_blank">here</a>.
		</div>
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Data
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
				<ul class="m-portlet__nav">
					<li class="m-portlet__nav-item">
						<a href="<?= base_url()?>perijinan/add" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
							<span>
								<i class="la la-plus"></i>
								<span>Tambah</span>
							</span>
						</a>
					</li>
					<li class="m-portlet__nav-item"></li>
					
				</ul>
			</div>
		</div>
		<div class="m-portlet__body" id="tabelx">

			<!--begin: Datatable -->
			
		</div>
	</div>

	<!-- END EXAMPLE TABLE PORTLET-->
</div>
<!--begin::Modal-->
						<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel">HAPUS DATA</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										Apakah anda ingin menghapus data persediaan pestisida  <b><i class="title"> </i></b> ?
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
										<button type="button" class="btn btn-danger confirm-hapus">Hapus</button>
									</div>
								</div>
							</div>
						</div>

						<!--end::Modal-->


<script src="<?= base_url()?>/theme/vendors/jquery/dist/jquery.js" type="text/javascript"></script>
<script src="<?= base_url()?>/assets/muds.js" type="text/javascript"></script>

<script>
    $(document).ready(function(){
				LoadData("<?= base_url('sedia_salur_pestisida/show_data') ?>", "tabelx");
    });
</script> 


