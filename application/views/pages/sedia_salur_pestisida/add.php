<div class="m-content">

						<!--begin::Portlet-->
						<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											Tambah Pengadaan dan Penyaluran Pestisida
										</h3>
									</div>
								</div>
							</div>

							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" id="m_form_1" method="POST" action="<?php echo base_url(); ?>perijinan/save">
								<div class="m-portlet__body">
									<div class="m-form__content">
										<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_1_msg">
											<div class="m-alert__icon">
												<i class="la la-warning"></i>
											</div>
											<div class="m-alert__text">
												Silahkan perbaiki dan lengkapi data anda!
											</div>
											<div class="m-alert__close">
												<button type="button" class="close" data-close="alert" aria-label="Close">
												</button>
											</div>
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Nama Formulasi *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<select class="form-control m-select2" id="m_nama" name="nama">
											
												<option value="">Pilih Formulasi</option>
												
											</select>
											<span class="m-form__help">Pilih formulasi</span>
										</div>
									</div>
									
												
									<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Jenis Ijin *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
												
											<select class="form-control m-select2" id="m_jenis" name="jenis">
												<option value="">Pilih Jenis</option>
												<option value="1" >Tetap</option>
												<option value="2" >Sementara</option>
												<option value="3" >Percobaan</option>
											</select>
											<span class="m-form__help">Pilih jenis ijin</span>
										</div>
									</div>
																		
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">No. Ijin / SK Menteri *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<input type="text" class="form-control m-input" name="nomor" placeholder="No. Ijin / SK Menteri" data-toggle="m-tooltip" >
											<span class="m-form__help">Isi No. Ijin / SK Menteri</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Tanggal Akhir Ijin *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<div class="input-group date">
												<input type="text" class="form-control m-input" readonly id="m_datepicker_3" placeholder="Tanggal Akhir Ijin" name="tanggal"/>
												<div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-calendar"></i>
													</span>
												</div>
											</div>
											<span class="m-form__help">Isi Tanggal Akhir Ijin</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Keterangan</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<textarea class="form-control m-input" name="keterangan" placeholder="Keterangan" ></textarea>
											<span class="m-form__help">Isi Keterangan</span>
										</div>
									</div>
								</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-success">Simpan</button>
												<button type="reset" class="btn btn-secondary" id="batal">Batal</button>
											</div>
										</div>
									</div>
								</div>
							</form>

							<!--end::Form-->
						</div>
</div>


<script src="<?= base_url()?>/theme/vendors/jquery/dist/jquery.js" type="text/javascript"></script>
<script src="<?= base_url()?>/theme/datepicker.js" type="text/javascript"></script>

<script  type="text/javascript">

//==untuk validasi form
var FormControls = function () {
    //== Private functions
 
	var demo1 = function () {
        $( "#m_form_1" ).validate({
            // define validation rules
            rules: {
                nomor: {
                    required: true
                },
                nama: {
                    required: true
                },
                jenis: {
                    required: true
                },
				tanggal: {
                    required: true
                },
				kategori: {	
                    required: true
                },
				
            },
            
			//override massage
			messages: {
				nomor: "Silahkan isi nomor pendaftaran formulasi",
				nama: "Silahkan isi nama formulasi",
				perusahaan: "Silahkan pilih perusahaan pemegang nomor pendaftaran",
				jenis: "Silahkan pilih jenis formulasi",
				penggunaan: "Silahkan pilih penggunaan formulasi",
				kategori: "Silahkan pilih kategori formulasi",
			},
			
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                var alert = $('#m_form_1_msg');
                alert.removeClass('m--hide').show();
                mUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
				
            }
        });       
    }
 
		
	return {
        // public functions
        init: function() {
            demo1(); 
        }
    };
}();

//== untuk option with search
var Select2 = function() {
    //== Private functions
    var demos = function() {

        // basic
        $('#m_nama').select2({
            placeholder: "Pilih Formulasi",
            allowClear: true
        });
		
		 // basic
        $('#m_jenis').select2({
            placeholder: "Pilih Jenis Ijin",
            allowClear: true
        });
		
    }


    //== Public functions
    return {
        init: function() {
            demos();
        }
    };
}();

	$(document).ready(function(){
		function get_formulasi(){
			$.ajax({
						type: "get",
						url: "<?= base_url() ?>perijinan/get_formulas_jason",
						data: "",
						success: function (response) {
							//get_csrf();
							
							//console.log(response);
							var data= JSON.parse(response);
							
							
								var $el = $("#m_nama");
								$el.empty(); // remove old options
								$el.append('<option value="none">Pilih Formulasi</option>');
								$.each(data,function(i,value){
									$el.append('<option value="'+value.formulasiid+'">'+value.nama+' ('+value.perusahaan+')'+'</option>');
								});
						}
					});
			
		}
		
		
		$('#batal').click(function(){
			window.location.href='<?= base_url() ?>formulasi';
		});
		
		get_formulasi();
		FormControls.init();
		Select2.init();
        
    });
</script>
