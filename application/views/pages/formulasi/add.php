<div class="m-content">

						<!--begin::Portlet-->
						<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											Tambah Formulasi
										</h3>
									</div>
								</div>
							</div>

							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" id="m_form_1" method="POST" action="<?php echo base_url(); ?>formulasi/save">
								<div class="m-portlet__body">
									<div class="m-form__content">
										<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_1_msg">
											<div class="m-alert__icon">
												<i class="la la-warning"></i>
											</div>
											<div class="m-alert__text">
												Silahkan perbaiki dan lengkapi data anda!
											</div>
											<div class="m-alert__close">
												<button type="button" class="close" data-close="alert" aria-label="Close">
												</button>
											</div>
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Nomor Pendaftaran *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<input type="text" class="form-control m-input" name="nomor" placeholder="Nomor Pendaftaran " data-toggle="m-tooltip" >
											<span class="m-form__help">Isi nomor pendaftaran</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Nama Produk *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<input type="text" class="form-control m-input" name="nama" placeholder="Nama Produk " data-toggle="m-tooltip" >
											<span class="m-form__help">Isi nama Produk</span>
										</div>
									</div>
									
									<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Jenis Produk *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<select class="form-control m-select2" id="m_jenis" name="jenis">
											
												<option value="">Pilih Jenis</option>
												<?php foreach($get_jenis as $row => $data){ ?>
													<option value="<?php echo $data['jenisformulaid']; ?>"><?php echo $data['jenis']; ?></option>
												<?php } ?>
											</select>
											<span class="m-form__help">Pilih jenis produk</span>
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Penggunaan *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<select class="form-control m-select2" id="m_penggunaan" name="penggunaan">
											
												<option value="">Pilih Penggunaan</option>
												<?php foreach($get_penggunaan as $row => $data){ ?>
													<option value="<?php echo $data['penggunaan_id']; ?>"><?php echo $data['bidang']; ?></option>
												<?php } ?>
											</select>
											<span class="m-form__help">Pilih penggunaan</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Perusahaan *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<select class="form-control m-select2" id="m_perusahaan" name="perusahaan">
											
												<option value="">Pilih Perusahaan</option>
												<?php foreach($get_perusahaan as $row => $data){ ?>
													<option value="<?php echo $data['perusahaanid']; ?>"><?php echo $data['nama']; ?></option>
												<?php } ?>
											</select>
											<span class="m-form__help">Pilih Perusahaan</span>
										</div>
									</div>
									
									<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Kategori *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
												
											<select class="form-control m-select2" id="m_kategori" name="kategori">
												<option value="">Pilih Kategori</option>
												<option value="1" >Umum</option>
												<option value="2" >Teknis</option>
												<option value="3" >Ekspor</option>
											</select>
											<span class="m-form__help">Pilih Kategori</span>
										</div>
									</div>
																		
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Deskripsi Singkat</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<textarea class="form-control m-input" name="deskripsi" placeholder="Deskripsi Singkat" ></textarea>
											<span class="m-form__help">Isi deskripsi singkat produk</span>
										</div>
									</div>
									
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Peringatan</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<textarea class="form-control m-input" name="peringatan" placeholder="Peringatan" ></textarea>
											<span class="m-form__help">Isi peringatan produk</span>
										</div>
									</div>
								</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-success">Simpan</button>
												<button type="reset" class="btn btn-secondary" id="batal">Batal</button>
											</div>
										</div>
									</div>
								</div>
							</form>

							<!--end::Form-->
						</div>
</div>


<script src="<?= base_url()?>/theme/vendors/jquery/dist/jquery.js" type="text/javascript"></script>

<script  type="text/javascript">

//==untuk validasi form
var FormControls = function () {
    //== Private functions
 
	var demo1 = function () {
        $( "#m_form_1" ).validate({
            // define validation rules
            rules: {
                nomor: {
                    required: true
                },
                nama: {
                    required: true
                },				
                perusahaan: {
                    required: true
                },
                jenis: {
                    required: true
                },
				penggunaan: {
                    required: true
                },
				kategori: {	
                    required: true
                },
				
            },
            
			//override massage
			messages: {
				nomor: "Silahkan isi nomor pendaftaran produk",
				nama: "Silahkan isi nama produk",
				perusahaan: "Silahkan pilih perusahaan pemegang nomor pendaftaran",
				jenis: "Silahkan pilih jenis produk",
				penggunaan: "Silahkan pilih penggunaan produk",
				kategori: "Silahkan pilih kategori produk",
			},
			
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                var alert = $('#m_form_1_msg');
                alert.removeClass('m--hide').show();
                mUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
				
            }
        });       
    }
 
		
	return {
        // public functions
        init: function() {
            demo1(); 
        }
    };
}();

//== untuk option with search
var Select2 = function() {
    //== Private functions
    var demos = function() {

        // basic
        $('#m_jenis').select2({
            placeholder: "Pilih Jenis Produk",
            allowClear: true
        });

		// basic
        $('#m_penggunaan').select2({
            placeholder: "Pilih Penggunaan",
            allowClear: true
        });
		
		// basic
        $('#m_perusahaan').select2({
            placeholder: "Pilih Perusahaan",
            allowClear: true
        });
		// basic
        $('#m_kategori').select2({
            placeholder: "Pilih Kategori",
            allowClear: true
        });
		
    }


    //== Public functions
    return {
        init: function() {
            demos();
        }
    };
}();

	$(document).ready(function(){
				
		$('#batal').click(function(){
			window.location.href='<?= base_url() ?>formulasi';
		});
		
		FormControls.init();
		Select2.init();
        
    });
</script>
