<div class="modal fade" id="modalEdit" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tambah Dokumen Koordinasi</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i aria-hidden="true" class="ki ki-close"></i>
				</button>
			</div>
			<!--begin::Form-->
			<form id="form_modal" method="post">
				<div class="modal-body">

					<div class="form-group" id='err_div_kategori'>
						<label class="control-label">Kategori *</label>
						<div class="input-group col-md-12">
							<select class="form-control" id="kategori" name="kategori" required>
								<option value="">Pilih kategori dokumen</option>
								<option value="Nota Dinas">Nota Dinas</option>
								<option value="Surat">Surat</option>
								<option value="Lainnya">Lainnya</option>
							</select>
						</div>
					</div>
					<div class="form-group" id='err_div_file'>
						<label>Unggah File *</label>
						<input type="file" class="form-control" name="file" id="file" placeholder="file">
						<label class='control-label' id="err_file"></label>
					</div>
					<div class="form-group" id='err_div_tanggal'>
						<label>Tanggal *</label>
						<input type="text" class="form-control" name="tanggal" id="tanggal" required>
						<label class='control-label' id="err_tanggal"></label>
					</div>
					<div class="form-group" id='err_div_nomor'>
						<label>Nomor *</label>
						<input type="text" class="form-control" placeholder="Isi nomor dokumen" name="nomor" id="nomor" required>
						<label class='control-label' id="err_nomor"></label>
					</div>
					<div class="form-group" id='err_div_dari'>
						<label>Dari *</label>
						<input type="text" class="form-control" placeholder="Isi nama dokumen berasal" name="dari" id="dari" required>
						<label class='control-label' id="err_nomor"></label>
					</div>
					<div class="form-group" id='err_div_perihal'>
						<label>Perihal *</label>
						<textarea class="form-control" name="perihal" id="perihal" placeholder="Isi perihal dokumen" required></textarea>
						<label class='control-label' id="err_perihal"></label>
					</div>
					<div class="form-group" id='err_div_keterangan'>
						<label>Keterangan *</label>
						<textarea class="form-control" name="keterangan" id="keterangan" placeholder="Isi keterangan dokumen" required></textarea>
						<label class='control-label' id="err_keterangan"></label>
					</div>


				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
				</div>
			</form>
			<!--end::Form-->
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		$('#kategori').select2({});

		$('#tanggal').datepicker({
			autoclose: true
		});

		$('#btn_tanggal').click(function() {
			var tgl = $('#tanggal').val();
			alert(tgl);
		});

		$('#form_modal').submit(function(e) {
			e.preventDefault(); //gunanya untuk supaya ga refresh

			var form_data = new FormData();

			form_data.append('kategori', $('#kategori').val());
			form_data.append('nomor', $('#nomor').val());
			form_data.append('perihal', $('#perihal').val());
			form_data.append('dari', $('#dari').val());
			form_data.append('tanggal', $('#tanggal').val());
			form_data.append('keterangan', $('#keterangan').val());
			form_data.append('file', $('#file').prop('files')[0]);


			$.ajax({
				url: '<?php echo base_url(); ?>dokumen_koordinasi/save/add',
				dataType: 'json',
				data: form_data,
				type: 'post',
				cache: false,
				contentType: false,
				processData: false,
				success: function(response) {
					console.log(response);
					if (response.success == true) {



						toastr["success"]("Data berhasil disimpan.", "Perhatian");
						$('#modalEdit').modal('hide');
						var link = "dokumen_koordinasi/show_data";
						LoadData(link, "tabelx");
					} else {
						$.each(response.messages, function(nama_field, value) {
							//console.log('nama field: ' + nama_field);
							//console.log('pesan field: ' + value);
							$('#err_div_' + nama_field).addClass('has-error');
							$('#err_' + nama_field).html(value);

						});

					}
				}

			});
		});
	});
</script>