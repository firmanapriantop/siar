<!--begin: Datatable -->
<table class="table table-striped- table-bordered table-hover" id="tabel">
	<thead>
		<tr>
			<th></th>
			<th>Kategori</th>
			<th>Tanggal</th>
			<th>Nomor</th>
			<th>Perihal</th>
			<th>Dari</th>
			<th>File</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php $no = 0;
		foreach ($data->result() as $row) : $no++; ?>
			<tr>
				<td><?= $no; ?></td>
				<td class="text-center"><span class="label label-info"><?= $row->kategori; ?></span></td>
				<td><?= date_format(date_create($row->tanggal), "d M Y"); ?></td>
				<td><?= $row->nomor; ?></td>
				<td><?= $row->perihal; ?></td>
				<td><?= $row->dari; ?></td>
				<td class=" text-center"><a href="<?= base_url() . 'uploads/dokumen_koordinasi/' . $row->file; ?>" target="_blank"><i class="fa fa-download"></i></a></td>
				<td class="text-center">
					<?php if ($can_edit) { ?>
						<div class="btn-group btn-group-xs">
							<button type="button" class="btn btn-success" onclick="CallModal('dokumen_koordinasi/edit/<?= $row->id; ?>', 'tmpModal', 'modalEdit')"><i class="fa fa-edit"></i></button>
							<button type="button" class="btn btn-danger" onclick="hapus('<?= $row->id ?>')"><i class="fa fa-trash-o"></i></button>
						</div>
					<?php } ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>


<script>
	function hapus(id) {
		swal({
				title: "Hapus Dokumen",
				text: "Apakah Anda yakin untuk menghapus dokumen ini?",
				type: "warning",
				showCancelButton: true,
				cancelButtonClass: "btn-danger",
				cancelButtonText: "Tidak",
				confirmButtonText: "Ya",
				closeOnConfirm: false,
				closeOnCancel: false
			},
			function(isConfirm) {
				if (isConfirm) {
					$.ajax({
						url: '<?= base_url() ?>dokumen_koordinasi/hapus',
						type: 'post',
						data: {
							id: id
						},
						error: function() {
							alert('Something is wrong');
						},
						success: function(data) {
							var link = "dokumen_koordinasi/show_data";
							LoadData(link, "tabelx");
							swal("", "Dokumen berhasi dihapus", "success");
						}
					});
				} else {
					swal("", "Hapus dibatalkan", "error");
				}
			});
	}

	$(document).ready(function() {
		$('#tabel thead tr').clone(true).appendTo('#tabel thead');
		$('#tabel thead tr:eq(1) th').each(function(i) {
			var title = $(this).text();
			switch (title) {
				case "Kategori":
				case "Tanggal":
				case "Nomor":
				case "Perihal":
				case "Dari":
				case "File":
					$(this).html('<input type="text" class="form-control form-control-sm form-filter m-input" placeholder="Search ' + title + '" />');
					break;
			};

			$('input', this).on('keyup change', function() {
				if (table.column(i).search() !== this.value) {
					table
						.column(i)
						.search(this.value)
						.draw();
				}
			});
		});

		var table = $('#tabel').DataTable({
			orderCellsTop: true,
			fixedHeader: true,
			responsive: !0,
			"columnDefs": [{
				"targets": -1,
				"orderable": false
			}]
		});



		$('#confirm-delete').on('show.bs.modal', function(e) {
			// get information to update quickly to modal view as loading begins
			var opener = e.relatedTarget; //this holds the element who called the modal

			//we get details from attributes
			var id = $(opener).attr('data-id');
			var fullname = $(opener).attr('full-name');

			$('.title').text(' ' + fullname + ' ');
			$('.confirm-hapus').data('id', id);
		});

		$('.confirm-hapus').click(function() {
			var ID = $(this).data('id');
			$.ajax({
				url: "<?php echo base_url(); ?>dokumen_koordinasi/delete/" + ID,
				type: "post",
				data: ID,
				success: function(data) {
					$("#confirm-delete").modal('hide');
					//$("#deleted").modal('show');
					LoadData("<?php echo base_url(); ?>dokumen_koordinasi/show_data", "tabelx");
				}
			});
		});
	});
</script>