<div class="m-content">

						<!--begin::Portlet-->
						<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											Tambah Komoditas
										</h3>
									</div>
								</div>
							</div>

							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" id="m_form_1" method="POST" action="<?php echo base_url(); ?>komoditas/save">
								<div class="m-portlet__body">
									<div class="m-form__content">
										<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_1_msg">
											<div class="m-alert__icon">
												<i class="la la-warning"></i>
											</div>
											<div class="m-alert__text">
												Silahkan perbaiki dan lengkapi data anda!
											</div>
											<div class="m-alert__close">
												<button type="button" class="close" data-close="alert" aria-label="Close">
												</button>
											</div>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Komoditas *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<input type="text" class="form-control m-input" name="komoditas" placeholder="Jenis Komoditas" data-toggle="m-tooltip" >
											<span class="m-form__help">Isi Jenis Komoditas</span>
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Keterangan</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<textarea class="form-control m-input" name="keterangan" placeholder="Keterangan"></textarea>
											<span class="m-form__help">Maksimal 150 Karakter</span>
										</div>
									</div>
									
								</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-success">Simpan</button>
												<button type="reset" class="btn btn-secondary" id="batal">Batal</button>
											</div>
										</div>
									</div>
								</div>
							</form>

							<!--end::Form-->
						</div>
</div>


<script src="<?= base_url()?>/theme/vendors/jquery/dist/jquery.js" type="text/javascript"></script>

<script  type="text/javascript">

//==untuk validasi form
var FormControls = function () {
    //== Private functions
 
	var demo1 = function () {
        $( "#m_form_1" ).validate({
			
			//ignore: ":hidden:not(#mketerangan),.note-editable.panel-body",
            // define validation rules
            rules: {
                komoditas: {
                    required: true 
                },
				keterangan: {
                    maxlength: 150 
                },
            },
            
			//override massage
			messages: {
				komoditas: "silahkan isi jenis komoditas",
			},
			
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                var alert = $('#m_form_1_msg');
                alert.removeClass('m--hide').show();
                mUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
				
            }
			
			
        });    

				
		//$('#m_form_1').each(function () {
		//	if ($(this).data('validator')) $(this).data('validator').settings.ignore = ".note-editor *";
		//});
		
    }
 
		
	return {
        // public functions
        init: function() {
            demo1(); 
        }
    };
}();

/*var SummernoteDemo = function () {    
    //== Private functions
    var demos = function () {
        $('.keterangan').summernote({
            height: 150
        });
    }

    return {
        // public functions
        init: function() {
            demos(); 
        }
    };
}();
*/
	$(document).ready(function(){
				
		$('#batal').click(function(){
			window.location.href='<?= base_url() ?>komoditas';
		});
		
		//SummernoteDemo.init();
		FormControls.init();
    });
</script>
