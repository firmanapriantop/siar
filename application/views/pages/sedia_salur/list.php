<!--begin: Datatable -->
<table class="table table-striped- table-bordered table-hover" id="tabel">
    <thead>
						<tr>
							<th></th>							
							<th>Tahun</th>						
							<th>Nama Formulasi</th>
							<th>Perusahaan</th>
							<th>No. Pendaftaran</th>
							<th>No. Ijin / SK Menteri</th>
							<th>Jenis Formulasi</th>							
							<th>Sisa Stock Sebelumnya</th>
							<th>Pengadaan</th>
							<th>Penyaluran</th>
							<th>Sisa Stock</th>
							<th>Satuan</th>
							<th>Expired</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
</table>



<script>
	$(document).ready(function(){
		
		$('#tabel thead tr').clone(true).appendTo( '#tabel thead' );
        $('#tabel thead tr:eq(1) th').each( function (i) {
			var title = $(this).text();
			switch(title){
				
				case"Tahun":
				case"No. Pendaftaran":
				case"Nama Formulasi":
				case"Jenis Formulasi":
				case"Sisa Stock Sebelumnya" :
				case"Pengadaan":
				case"Penyaluran":
				case"Perusahaan":
				case"Sisa Stock":
				case"Satuan":
			    case"No. Ijin / SK Menteri":
				case "Expired":
					$(this).html( '<input type="text" class="form-control form-control-sm form-filter m-input" placeholder="Search '+title+'" />' );
				break;
			};

			$( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                }
			});
        });
 
        var edit = '';
		var id = '';
		var nama='';
 
		
        var table = $('#tabel').DataTable({
			
			serverSide: false,
			//processing: true,
			fixedHeader: true,
			ajax: {
				type: "POST",
				url: '<?= base_url('sedia_salur_pestisida/get_data_by_json') ?>',
				data: { typ: 1 },
				dataType: "json",
				dataSrc: function (result) {
					var json = result;
					return json;
				}
			}
			,
			columns: [
			
				{ data: "sediasalurid" },
				{ data: "tahun" },
				{ data: "nama" },
				{ data: "perusahaan" },
				{ data: "no_pendaftaran" },
				{ data: "no_ijin" },
				{ data: "jenis" },
				{ data: "stockawal", className : "right" },
				{ data: "pengadaan" },
				{ data: "penyaluran" },
				{ data: "stockakhir" },
				{ data: "satuan" },
				{ data: "tgl_akhir_ijin" },
				{
					
					data: null,
					className: "center",
					defaultContent: ' <span class="dropdown">'+
									'<a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true"><i class="la la-ellipsis-h"></i></a>'+
									'<div class="dropdown-menu dropdown-menu-right">'+
										'<a class="dropdown-item" href="#" id="Edit_link"><i class="la la-edit"></i> Edit</a>'+
										'<a class="dropdown-item text-danger hapus" href="#" data-toggle="modal" data-target="#confirm-delete" data-id="'+id+'" full-name="'+nama+'"><i class="la 	la-trash text-danger"></i> Hapus</a>'+
									'</div>'+
								'</span>'
				},
			],
			orderCellsTop: true,
            responsive:!0,
            "columnDefs": [{
                "targets"  : -1,
                "orderable": false,
            }],
		});
		
		table.column(0).visible(false);
		
		$(document).on('click','#Edit_link',function(e){
			 e.preventDefault();  

				var currentRow = $(this).closest("tr");

				var data = $('#tabel').DataTable().row(currentRow).data();
				//alert(data['perijinanid']);
				
				var link="<?php echo base_url(); ?>sedia_salur_pestisida/edit/"+data['sediasalurid'];
				//alert(link);
				window.location.replace(link);
		  })
		
		$('#confirm-delete').on('show.bs.modal', function (e) {
			var currentRow = $(".hapus").closest("tr");

			var data = $('#tabel').DataTable().row(currentRow).data();
			//alert(data['perijinanid']);
			//console.log(data['nivel_puesto']);
			
			// get information to update quickly to modal view as loading begins
			var opener=e.relatedTarget;//this holds the element who called the modal
			   
			//we get details from attributes
			var id=data['sediasalurid'];
			var fullname=data['nama'];
			
			$('.title').text(' ' + fullname + ' ');
			$('.confirm-hapus').data('id', id);
		});
		
		
		
		$('.confirm-hapus').click(function(){
			var ID = $(this).data('id');
			//alert(ID);
			$.ajax({
				url: "<?php echo base_url(); ?>sedia_salur_pestisida/delete/"+ID,
				type: "post",
				data:ID,
				success: function (data) {
					$("#confirm-delete").modal('hide');
					//$("#deleted").modal('show');
					LoadData("<?php echo base_url(); ?>sedia_salur_pestisida/show_data", "tabelx");
				}
			  });
		});
  });
</script>
