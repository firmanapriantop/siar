<?php //print_r($get_kab);
	$formulasiid = $data->formulasiid;
	$nomor = $data->no_ijin;
	$id = $data->sediasalurid;
	
	$pengadaan =$data->pengadaan;
	$penyaluran = $data->penyaluran;
	$stock_awal=$data->stockawal;
	$stock_akhir=$data->stockakhir;
	$satuan=$data->satuan;
	
	$no_daftar=$data->no_pendaftaran;	
	$jenis = $data->jenis;
	$expired =TglUK($data->tgl_akhir_ijin);
	$keterangan = $data->keterangan;
	$tahun=$data->tahun;
	
	//echo $formulasiid;
?>

<div class="m-content">

						<!--begin::Portlet-->
						<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											Tambah Pengadaan dan Penyaluran Pestisida
										</h3>
									</div>
								</div>
							</div>

							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" id="m_form_1" method="POST" action="<?php echo base_url(); ?>sedia_salur_pestisida/save/<?=$id?> ">
								<div class="m-portlet__body">
									<div class="m-form__content">
										<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_1_msg">
											<div class="m-alert__icon">
												<i class="la la-warning"></i>
											</div>
											<div class="m-alert__text">
												Silahkan perbaiki dan lengkapi data anda!
											</div>
											<div class="m-alert__close">
												<button type="button" class="close" data-close="alert" aria-label="Close">
												</button>
											</div>
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Nama Formulasi *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<select class="form-control m-select2" id="m_nama" name="nama">
											
												
											</select>
											<span class="m-form__help">Pilih formulasi</span>
										</div>
									</div>
									
												
									<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Jenis Formulasi </label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<input type="text" class="form-control m-input" name="jenis" placeholder="Jenis Ijin" id="m_jenis" data-toggle="m-tooltip" value="<?=$jenis?>" disabled>
											<span class="m-form__help">Jenis Formulasi</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Nomor Pendaftaran </label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<input type="text" class="form-control m-input" name="no_daftar" placeholder="Nomor Pendaftaran" id="m_no_daftar" data-toggle="m-tooltip" value="<?=$no_daftar?>" disabled>
											<span class="m-form__help">Nomor Pendaftaran</span>
										</div>
									</div>								
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">No. Ijin / SK Menteri </label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<input type="text" class="form-control m-input" name="nomor" placeholder="No. Ijin / SK Menteri" data-toggle="m-tooltip" id="m_no_ijin" value="<?=$nomor?>" disabled>
											<span class="m-form__help">Nomor Ijin / SK Menteri</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Tanggal Akhir Ijin </label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<div class="input-group date">
												<input type="text" class="form-control m-input" placeholder="Tanggal Akhir Ijin" name="tanggal" id="m_expired" disabled value="<?=$expired?>"/>
												<div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-calendar"></i>
													</span>
												</div>
											</div>
											<span class="m-form__help">Tanggal Akhir Ijin</span>
										</div>
									</div>
									<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Tahun Lapor *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<select class="form-control m-select2" id="m_tahun" name="tahun">
												<option value="">Pilih Tahun</option>
												<?php
													$x = 1; 
													$current_year=date(Y);
													while($x <= 4) {
														$year=$current_year-$x;
												?>
														<option value="<?php echo $year;?>" <?=($year==$tahun ? "selected" : "" )?>><?php echo $year;?></option>
												<?php
													$x++;
													} 
												?>
											</select>
											<span class="m-form__help">Pilih Tahun</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Sisa Stock Formulasi Tahun Sebelumnya *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<input type="text" class="form-control m-input" name="stock_awal" placeholder="Sisa Stock Tahun Sebelumnya" data-toggle="m-tooltip" value="<?=$stock_awal?>">
											<span class="m-form__help">Isi sisa stock tahun sebelumnya</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Pengadaan *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<input type="text" class="form-control m-input" name="pengadaan" placeholder="Pengadaan" data-toggle="m-tooltip" value="<?=$pengadaan?>">
											<span class="m-form__help">Isi pengadaan formulasi</span>
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Penyaluran *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<input type="text" class="form-control m-input" name="penyaluran" placeholder="Penyaluran" data-toggle="m-tooltip" value="<?=$penyaluran?>">
											<span class="m-form__help">Isi penyaluran formulasi</span>
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Sisa Stock Formulasi *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<input type="text" class="form-control m-input" name="stock_akhir" placeholder="Sisa Stock Akhir Formulasi" data-toggle="m-tooltip" value="<?=$stock_akhir?>">
											<span class="m-form__help">Isi stock formulasi</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Satuan *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<select class="form-control m-select2" id="m_satuan" name="satuan">
												<option value="">Pilih Satuan</option>
												<option value="1" <?=($satuan==1 ? "selected" : "")?>>Kg</option>
												<option value="2" <?=($satuan==2 ? "selected" : "")?>>Liter</option>
											</select>
											<span class="m-form__help">Isi satuan formulasi</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Keterangan</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<textarea class="form-control m-input" name="keterangan" placeholder="Keterangan" ><?=$keterangan?></textarea>
											<span class="m-form__help">Isi Keterangan</span>
										</div>
									</div>
								</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-success">Simpan</button>
												<button type="reset" class="btn btn-secondary" id="batal">Batal</button>
											</div>
										</div>
									</div>
								</div>
							</form>

							<!--end::Form-->
						</div>
</div>


<script src="<?= base_url()?>/theme/vendors/jquery/dist/jquery.js" type="text/javascript"></script>

<script  type="text/javascript">

//==untuk validasi form
var FormControls = function () {
    //== Private functions
 
	var demo1 = function () {
        $( "#m_form_1" ).validate({
            // define validation rules
            rules: {
                nama: {
                    required: true
                },
                stock_awal: {
                    required: true,decimal: true
                },
                pengadaan: {
                    required: true,decimal: true
                },
				penyaluran: {
                    required: true,decimal: true
                },
				stock_akhir: {	
                    required: true,decimal: true
                },
				tahun: {	
                    required: true
                },
				satuan : {
					required:true
				}
				
            },
            
			//override massage
			messages: {
				stock_awal: {
					required: "Silahkan isi jumlah sisa stock tahun sebelumnya",
					decimal: "Silahkan isi angka, jika dalam desimal format 0.00",
				},
				nama: "Silahkan pilih nama formulasi",
				pengadaan: {
					required:"Silahkan isi jumlah pengadaan formulasi",
					decimal: "Silahkan isi angka, jika dalam desimal format 0.00",
				},
				penyaluran: {
					required:"Silahkan isi jumlah penyaluran formulasi",
					decimal: "Silahkan isi angka, jika dalam desimal format 0.00",
				},
				stock_akhir: {
					required:"Silahkan isi jumlah stock akhir formulasi",		
					decimal: "Silahkan isi angka, jika dalam desimal format 0.00",
				},		
				tahun: "Silahkan pilih tahun laporan",
				satuan: "Silahkan pilih satuan formulasi"
			},
			
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                var alert = $('#m_form_1_msg');
                alert.removeClass('m--hide').show();
                mUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
				
            }
        });       
    }
 
		
	return {
        // public functions
        init: function() {
            demo1(); 
        }
    };
}();

//== untuk option with search
var Select2 = function() {
    //== Private functions
    var demos = function() {

        // basic
        $('#m_nama').select2({
            placeholder: "Pilih Formulasi",
            allowClear: true
        });
		
		 // basic
        $('#m_tahun').select2({
            placeholder: "Pilih Tahun",
            allowClear: true
        });
		 $('#m_satuan').select2({
            placeholder: "Pilih Satuan",
            allowClear: true
        });
		
    }


    //== Public functions
    return {
        init: function() {
            demos();
        }
    };
}();

	$(document).ready(function(){
		
		$.validator.addMethod('decimal', function(value, element) {
		  return this.optional(element) || /^((\d+(\\.\d{0,2})?)|((\d*(\.\d{1,2}))))$/.test(value);
		}, "Please enter a correct number, format 0.00");
		
		function get_formulasi(){
			$.ajax({
						type: "get",
						url: "<?= base_url() ?>sedia_salur_pestisida/get_formulas_jason",
						data: "",
						success: function (response) {
							//get_csrf();
							
							//console.log(response);
							var data= JSON.parse(response);
							var formulaid='<?php echo $formulasiid; ?>'
							//alert (formulaid);
								var $el = $("#m_nama");
								$el.empty(); // remove old options
								$el.append('<option value="none">Pilih Formulasi</option>');
								$.each(data,function(i,value){
									if(formulaid==value.formulasiid){
										var pilih = 'selected'
										//alert (formulaid);
									}
									$el.append('<option value="'+value.formulasiid+'" ijin="'+value.perijinanid+'" '+pilih+'>'+value.nama+' ('+value.perusahaan+')'+'</option>');
								});
								
								//$el.select2('data', null)
								//$el.select2('data', null)
						}
					});
			
		}
		
		function get_formulasi_by_id($id){
			$.ajax({
						type: "get",
						url: "<?= base_url() ?>sedia_salur_pestisida/get_formulas_jason",
						data: "perijinanid="+$id,
						success: function (response) {
							//get_csrf();
							//alert($id);
							//console.log(response);
							var data= JSON.parse(response);
							
								$.each(data,function(i,x){
									$("#m_jenis").val(x.jenis);
									$("#m_no_ijin").val(x.no_ijin);
									$("#m_expired").val(formatDate(x.tgl_akhir_ijin));									
									$("#m_no_daftar").val(x.no_pendaftaran);
									
								});
						}
					});
		}
		
		
		$('#batal').click(function(){
			window.location.href='<?= base_url() ?>sedia_salur_pestisida';
		});
		
		get_formulasi();
		FormControls.init();
		Select2.init();
		
			
		
		$('#m_nama').change(function(e){
			var thisId = $(this).children("option:selected").attr("ijin");
			
			get_formulasi_by_id(thisId)
			
		});
		
		
		function formatDate(date) {
			 var d = new Date(date),
				 month = '' + (d.getMonth() + 1),
				 day = '' + d.getDate(),
				 year = d.getFullYear();

			 if (month.length < 2) month = '0' + month;
			 if (day.length < 2) day = '0' + day;

			 return [day, month, year].join('-');
		 }
        
    });
</script>
