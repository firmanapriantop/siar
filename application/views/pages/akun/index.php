</br>
<h3 class="page-title">
    Daftar Akun <small></small>
</h3>
</br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-green-sharp bold uppercase">Data </span>
                </div>
                <div class="tools">
                    <button type="button" class="btn btn-success" id="btn_add"><i class="fa fa-plus-square"></i> Tambah</button>
                </div>
            </div>
            <div class="portlet-body" id="tabelx">
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        LoadData("<?= base_url('akun/show_data') ?>", "tabelx");
        $('#btn_add').click(function() {
            CallModal('akun/add', 'tmpModal', 'modalEdit');
        });
    });
</script>