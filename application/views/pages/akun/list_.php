<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<span class="caption-subject font-green-sharp bold uppercase">Data </span>
		</div>
		<div class="tools">
			
		</div>
	</div>
	<div class="portlet-body">
		<table class="table table-striped- table-bordered table-hover" id="tabel">
							<thead>
								<tr>
									<th></th>
									<th>Kode</th>
									<th>Nama / Uraian</th>
									<th>Volume</th>
									<th>Satuan</th>
									<th>Harga Satuan</th>							
									<th>Jumlah</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
		</table>
	</div>
</div>

<style>
	.dropdown-menu {
		position: relative;
		float: none;
		width: 160px;
	}
</style>

<script>
	$(document).ready(function(){
		var idgiat = $('#m_kegiatan').val();
		var kdrka = $('#m_rka').val();
		
		$('#tabel thead tr').clone(true).appendTo( '#tabel thead' );
        $('#tabel thead tr:eq(1) th').each( function (i) {
			var title = $(this).text();
			switch(title){
				case"Kode":
				case"Nama / Uraian":
				case"Volume":
				case"Satuan" :
				case"Harga Satuan":				
				case"Jumlah":
					$(this).html( '<input type="text" class="form-control form-control-sm form-filter m-input" placeholder="Search '+title+'" />' );
				break;
			};

			$( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                }
			});
        });
		
		var edit = '';
		var id = '';
		var nama='';
 
		
        var table = $('#tabel').DataTable({
			paging:   false,
			ordering: false,
			info:     false,
			serverSide: false,
			//processing: true,
			fixedHeader: true,
			ajax: {
				type: "post",
				url: '<?= base_url('lihat_pok/get_data_by_json') ?>',
				data: {"idgiat" : idgiat, "kdrka" : kdrka},
				
				dataType: "json",
				dataSrc: function (result) {
					var json = result;
					return json;
				}
			}
			,
			columns: [
			
				{ data: "lev" },
				{ data: "kode" },
				{ data: "nama" },
				{ data: "volkeg",
					render:function (data, type, row, meta) {
						if(!data){
							return data;
						}else{
							
							return $.fn.dataTable.render.number('.', ',', 2, '').display(data);
						}
					}
				},
				{ data: "satkeg" },
				{ data: "hargasat",
					render:function (data, type, row, meta) {
						if(!data){
							return data;
						}else{
							
							return $.fn.dataTable.render.number('.', ',', 2, '').display(data);
						}
					}},
				{ data: "jumlah", 
					render:function (data, type, row, meta) {
						if(!data){
							return data;
						}else{
							
							return $.fn.dataTable.render.number('.', ',', 2, '').display(data);
						}
					}
				},
				
			],
			orderCellsTop: true,
            responsive:!0,
            "columnDefs": [{
                "targets"  : -1,
                "orderable": false,
            },
			{ className: 'text-right', targets: [3,5,6] },
			{ className: 'text-center', targets: [4] }],
		});
		
		table.column(0).visible(false);
				
  });
</script>