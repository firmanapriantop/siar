<table class="table table-striped- table-bordered table-hover" id="tabel">
	<thead>
		<tr>
			<th></th>
			<th>Kode Akun</th>
			<th>Nama / Uraian</th>
			<th></th>
		</tr>
	</thead>
	<tbody>

	</tbody>
</table>

<style>
	.dropdown-menu {
		position: relative;
		float: none;
		width: 160px;
	}
</style>

<script>
	function editakun(id) {
		CallModal('akun/edit/' + id, 'tmpModal', 'modalEdit');
	}

	$(document).ready(function() {

		$('#tabel thead tr').clone(true).appendTo('#tabel thead');
		$('#tabel thead tr:eq(1) th').each(function(i) {
			var title = $(this).text();
			switch (title) {
				case "Kode Akun":
				case "Nama / Uraian":
					$(this).html('<input type="text" class="form-control form-control-sm form-filter m-input" placeholder="Search ' + title + '" />');
					break;
			};

			$('input', this).on('keyup change', function() {
				if (table.column(i).search() !== this.value) {
					table
						.column(i)
						.search(this.value)
						.draw();
				}
			});
		});

		var edit = '';
		var id = '';
		var nama = '';


		var table = $('#tabel').DataTable({

			serverSide: false,
			//processing: true,
			fixedHeader: true,
			ajax: {
				type: "POST",
				url: '<?= base_url('akun/get_data_by_json') ?>',
				data: {
					typ: 1
				},
				dataType: "json",
				dataSrc: function(result) {
					var json = result;
					return json;
				}
			},
			columns: [

				{
					data: "idakun"
				},
				{
					data: "kdakun"
				},
				{
					data: "nmakun"
				},
				{

					data: null,
					className: "center",

					"render": function(data, type, row, meta) {
						return '<div class="btn-group btn-group-sm" id="dropdown-menu">' +
							'<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i></button>' +
							'<ul class="dropdown-menu" role="menu">' +
							'<li><a class="dropdown-item" href="#" onclick="editakun(' + data.idakun + ')">Edit</a></li>' +
							'<li><a href="#" data-toggle="modal" data-target="#confirm-delete" data-id="' + data.idakun + '" full-name="' + data.kdakun + '" >Hapus</a></li>' +

							'</ul>' +
							'</div>'
					}
				},
			],
			orderCellsTop: true,
			responsive: !0,
			"columnDefs": [{
				"targets": -1,
				"orderable": false,
			}],
		});

		table.column(0).visible(false);



		$('#modalEdit').on('show.bs.modal', function(e) {

			var currentRow = $(".hapus").closest("tr");

			var data = $('#tabel').DataTable().row(currentRow).data();
			//alert(data['perijinanid']);
			//console.log(data['nivel_puesto']);

			// get information to update quickly to modal view as loading begins
			var opener = e.relatedTarget; //this holds the element who called the modal

			//we get details from attributes
			var formulaid = $(opener).attr('data-id');
			var fullname = $(opener).attr('full-name');

			//alert (formulaid + ' , ' + fullname +' --- ' + $(opener).attr('data-id'));

			$('.title').text(' ' + fullname + ' ');
			$('.confirm-hapus').data('id', formulaid);
		});

	});
</script>