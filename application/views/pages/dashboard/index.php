<div class="clearfix"></br></div>
<h3>Dashboard</h3>
<div class="clearfix"></br></div>
<!-- BEGIN DASHBOARD STATS -->
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat blue-madison">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?= number_format($total_alokasi / 1000000000, 2) . ' Miliar'; ?>
                </div>
                <div class="desc">
                    Total Alokasi
                </div>
            </div>
            <div class="more"></div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat purple-plum">
            <div class="visual">
                <i class="fa fa-globe"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?= number_format($revisi_ke, 0) ?>
                </div>
                <div class="desc">
                    Revisi ke
                </div>
            </div>
            <div class="more"></div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat red-intense">
            <div class="visual">
                <i class="fa fa-bar-chart-o"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?= number_format($total_sub_komponen, 0) ?>
                </div>
                <div class="desc">
                    Total Sub Komponen Revisi
                </div>
            </div>
            <div class="more"></div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat green-haze">
            <div class="visual">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?= number_format($total_item, 0) ?>
                </div>
                <div class="desc">
                    Total Item Revisi
                </div>
            </div>
            <div class="more"></div>
        </div>
    </div>

</div>
<!-- END DASHBOARD STATS -->
<div class="row">

    <div class="col-md-3 col-sm-3 text-center">
        <div class="portlet light">
            <div class="profile-userpic">
                <img src="<?= base_url() ?>assets/img/profile/default.png" class="img-responsive" alt="">
            </div>
            <!-- SIDEBAR USER TITLE -->
            <div class="profile-usertitle">
                <div class="profile-usertitle-name">
                    <?= $this->session->userdata('fullname') ?>
                </div>
                <div class="profile-usertitle-job">
                    <?= $this->role ?>
                </div>
                <?php if (!empty($subkomponen_ids)) { ?>
                    <div>
                        <h4 class="profile-desc-title">Unit Kerja : <?= $unit_kerja ?></h4>
                        <ul class="text-left">
                            <?php foreach ($nmsubkomponen->result() as $i) : ?>
                                <li><?= $i->nmsubkomponen ?></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php } ?>
            </div>
            <!-- END SIDEBAR USER TITLE -->
        </div>

    </div>
    <div class="col-md-9 col-sm-9">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-sharp hide"></i>
                            <span class="caption-subject font-green-sharp bold uppercase">Grafik Belanja</span>
                            <span class="caption-helper">10 besar</span>
                        </div>
                    </div>
                    <div class="portlet-body" id="belanja_chart">
                    </div>
                </div>
            </div>
            <!--
            <div class="col-md-6 col-sm-6">
                <div class="portlet light ">
                </div>
            </div>
-->
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        console.log(<?= json_encode($cat) ?>);
        $('#belanja_chart').highcharts({
            chart: {
                type: 'column',
                margin: 75,
                options3d: {
                    enabled: false,
                    alpha: 10,
                    beta: 25,
                    depth: 70
                }
            },
            title: {
                text: '',
                style: {
                    fontSize: '18px',
                    fontFamily: 'Verdana, sans-serif'
                }
            },
            subtitle: {
                text: '',
                style: {
                    fontSize: '15px',
                    fontFamily: 'Verdana, sans-serif'
                }
            },
            plotOptions: {
                column: {
                    depth: 25
                }
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: <?= json_encode($nama) ?>
            },
            exporting: {
                enabled: false
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Total Alokasi (dalam juta Rupiah)'
                },
            },
            tooltip: {
                formatter: function() {
                    return 'Total nilai alokasi untuk belanja <b>' + this.x + '</b> sebesar <b> Rp' + Highcharts.numberFormat(this.y, 2) + ' juta</b>, dalam ' + this.series.name;
                }
            },
            series: [{
                name: 'Belanja Akun',
                data: <?= json_encode($nilai) ?>

            }]
        });
    });
</script>