<h3 class="page-title">
	Revisi POK <small>blank page</small>
</h3>
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN PORTLET-->
		<div class="portlet light bordered form-fit">
			<div class="portlet-title">
				<div class="caption font-green-sharp">
					<i class="icon-speech font-green-sharp"></i>
					<span class="caption-subject bold uppercase"> Pilih Parameter</span>
					<span class="caption-helper"></span>
				</div>
				<div class="actions">
					<span class="label label-success">Revisi dimulai</span>
				</div>
			</div>
			<div class="portlet-body">

				<div class="form-group">
					<label class="col-form-label col-lg-3">Pilih Tahun RKA *</label>
					<div class="col-lg-8">
						<select class="form-control m-select2" id="m_rka">
						</select>
					</div>
				</div>
				<div class="form-group ">
					<label class="col-form-label col-lg-3">Pilih Program *</label>
					<div class="col-lg-8">
						<select class="form-control m-select2" id="m_program">
						</select>
					</div>
				</div>
				<div class="form-group m-form__group row">
					<label class="col-form-label col-lg-3">Pilih Kegiatan *</label>
					<div class="col-lg-8">
						<select class="form-control m-select2" id="m_kegiatan" name="kegiatan">
							<option></option>
						</select>
					</div>
				</div>
				<div class="form-group m-form__group row">
					<label class="col-form-label col-lg-3">Pilih Output *</label>
					<div class="col-lg-8">
						<select class="form-control m-select2" id="m_output" name="output">
						</select>
					</div>
				</div>
				<div class="form-group m-form__group row">
					<label class="col-form-label col-lg-3">Pilih Sub Output *</label>
					<div class="col-lg-8">
						<select class="form-control m-select2" id="m_sub_output" name="sub_output">
						</select>
					</div>
				</div>
				<div class="form-group m-form__group row">
					<label class="col-form-label col-lg-3">Pilih Komponen *</label>
					<div class="col-lg-8">
						<select class="form-control m-select2" id="m_komponen" name="komponen">
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-9 ml-lg-auto">
						<button type="button" class="btn btn-success" id="btn_tampilkan">Tampilkan</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Revisi POK
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
				<span class="btn btn-primary">Tahapan Revisi Dimulai</span>
				<ul class="m-portlet__nav">
					<li class="m-portlet__nav-item">

					</li>
					<li class="m-portlet__nav-item"></li>

				</ul>
			</div>
		</div>
		<div class="m-portlet__body">
			<!-- id="tabelx" -->

			<!--begin: Datatable -->
		</div>
		<div class="m-portlet__body" id="tabelx">

		</div>
	</div>

	<!-- END EXAMPLE TABLE PORTLET-->
</div>
<!--begin::Modal-->
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">HAPUS DATA</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				Apakah anda ingin menghapus data perijinan no. <b><i class="title"> </i></b> ?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-danger confirm-hapus">Hapus</button>
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->


<script src="<?= base_url() ?>/theme/vendors/jquery/dist/jquery.js" type="text/javascript"></script>
<script src="<?= base_url() ?>/assets/muds.js" type="text/javascript"></script>

<script>
	//== untuk option with search
	var Select2 = function() {
		//== Private functions
		var demos = function() {

			// basic

			$('#m_rka').select2({
				placeholder: "Pilih RKA",
				allowClear: true
			});

			$('#m_program').select2({
				placeholder: "Pilih Program",
				allowClear: true
			});

			// basic
			$('#m_kegiatan').select2({
				placeholder: "Pilih Kegiatan",
				//allowClear: true
			});

			$('#m_output').select2({
				placeholder: "Pilih Output",
				//allowClear: true
			});

			$('#m_sub_output').select2({
				placeholder: "Pilih Sub Output",
				//allowClear: true
			});

			$('#m_komponen').select2({
				placeholder: "Pilih Komponen",
				//allowClear: true
			});

		}


		//== Public functions
		return {
			init: function() {
				demos();
			}
		};
	}();

	function loadrka() {
		var idgiat = '';
		$.ajax({
			url: '<?= base_url("revisi_pok/load_rka") ?>',
			type: 'post',
			datatype: 'json',
			success: function(response) {
				var data = JSON.parse(response);
				var len = data.length;
				$("#m_rka").empty();
				$('#m_rka').append("<option value='" + data[3]['tahun'] + "'>" + data[3]['tahun'] + "</option>");
				for (var i = 0; i < len; i++) {
					var id = data[i]['tahun'];
					var nama = data[i]['tahun'];
					$("#m_rka").append("<option value='" + id + "'>" + nama + "</option>");
				}
			}
		});
	}

	function loadprogram() {
		var idgiat = '';
		$.ajax({
			url: '<?= base_url("revisi_pok/load_program") ?>',
			type: 'post',
			datatype: 'json',
			success: function(response) {
				var data = JSON.parse(response);
				var len = data.length;
				$("#m_program").empty();
				$('#m_program').append("<option value='" + data[0]['kdprogram'] + "'>" + data[0]['kdprogram'] + "-" + data[0]['nmprogram'] + "</option>");
				for (var i = 0; i < len; i++) {
					var id = data[i]['kdprogram'];
					var kode = data[i]['kdprogram'];
					var nama = data[i]['nmprogram'];
					$("#m_program").append("<option value='" + id + "'>" + kode + "-" + nama + "</option>");
				}
			}
		});
	}

	function loadkegiatan() {
		var idgiat = '';
		$.ajax({
			url: '<?= base_url("revisi_pok/load_kegiatan") ?>',
			type: 'post',
			datatype: 'json',
			success: function(response) {
				var data = JSON.parse(response);
				var len = data.length;
				$("#m_kegiatan").empty();
				$('#m_kegiatan').append("<option value=''></option>");
				for (var i = 0; i < len; i++) {
					var id = data[i]['idgiat'];
					var kode = data[i]['kdgiat'];
					var nama = data[i]['nmgiat'];
					$("#m_kegiatan").append("<option value='" + id + "'>" + kode + "-" + nama + "</option>");
				}
			}
		});
	}

	function loadoutput() {
		var idgiat = $('#m_kegiatan').val();
		$.ajax({
			url: '<?= base_url("revisi_pok/load_output") ?>',
			type: 'post',
			data: {
				idgiat: idgiat
			},
			datatype: 'json',
			success: function(response) {
				var data = JSON.parse(response);
				var len = data.length;
				$("#m_output").empty();
				$('#m_output').append("<option value=''></option>");
				for (var i = 0; i < len; i++) {
					var id = data[i]['idoutput'];
					var kode = data[i]['kdoutput'];
					var nama = data[i]['nmoutput'];
					$("#m_output").append("<option value='" + id + "'>" + kode + "-" + nama + "</option>");
				}
			}
		});
	}

	function loadsuboutput() {
		var idoutput = $('#m_output').val();
		$.ajax({
			url: '<?= base_url("revisi_pok/load_sub_output") ?>',
			type: 'post',
			data: {
				idoutput: idoutput
			},
			datatype: 'json',
			success: function(response) {
				var data = JSON.parse(response);
				var len = data.length;
				$("#m_sub_output").empty();
				$('#m_sub_output').append("<option value=''></option>");
				for (var i = 0; i < len; i++) {
					var id = data[i]['idsuboutput'];
					var kode = data[i]['kdsuboutput'];
					var nama = data[i]['nmsuboutput'];
					$("#m_sub_output").append("<option value='" + id + "'>" + kode + "-" + nama + "</option>");
				}
			}
		});
	}

	function loadkomponen() {
		var idsuboutput = $('#m_sub_output').val();
		$.ajax({
			url: '<?= base_url("revisi_pok/load_komponen") ?>',
			type: 'post',
			data: {
				idsuboutput: idsuboutput
			},
			datatype: 'json',
			success: function(response) {
				var data = JSON.parse(response);
				var len = data.length;
				$("#m_komponen").empty();
				$('#m_komponen').append("<option value=''></option>");
				for (var i = 0; i < len; i++) {
					var id = data[i]['idkomponen'];
					var kode = data[i]['kdkomponen'];
					var nama = data[i]['nmkomponen'];
					$("#m_komponen").append("<option value='" + id + "'>" + kode + "-" + nama + "</option>");
				}
			}
		});
	}

	function loadsubkomponen() {
		var kdrka = $('#m_rka').val();
		var kdprogram = $('#m_program').val();
		var idkomponen = $('#m_komponen').val();
		$.ajax({
			url: '<?= base_url("revisi_pok/load_sub_komponen") ?>',
			type: 'post',
			data: {
				idkomponen: idkomponen,
				kdrka: kdrka,
				kdprogram: kdprogram
			},
			datatype: 'json',
			success: function(response) {
				var data = JSON.parse(response);
				var len = data.length;
				$("#m_komponen").empty();
				$('#m_komponen').append("<option value=''></option>");
				for (var i = 0; i < len; i++) {
					var id = data[i]['idkomponen'];
					var kode = data[i]['kdkomponen'];
					var nama = data[i]['nmkomponen'];
					$("#m_komponen").append("<option value='" + id + "'>" + kode + "-" + nama + "</option>");
				}
			}
		});
	}


	$(document).ready(function() {

		Select2.init();

		loadrka();

		loadprogram();

		loadkegiatan();

		$('#m_kegiatan').change(function() {
			loadoutput();
		});

		$('#m_output').change(function() {
			loadsuboutput();
		});

		$('#m_sub_output').change(function() {
			loadkomponen();
		});

		$('#btn_tampilkan').click(function() {
			var idkomponen = $('#m_komponen').val();
			var kdrka = $('#m_rka').val();
			var kdprogram = $('#m_program').val();

			if (idkomponen == null) {
				alert('Komponen belum dipilih.');
			} else {
				var link = "revisi_pok/load_sub_komponen/" + idkomponen + "/" + kdrka + "/" + kdprogram;

				LoadData(link, "tabelx");


			}

		});
	});
</script>