<div class="modal fade" id="modalEdit" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Sub Komponen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <!--begin::Form-->
            <form id="form_modal" method="post">
                <div class="modal-body">

                    <div class="form-group" id='err_div_kdsubkomponen'>
                        <label class="control-label">Kode Sub Komponen *</label>
                        <input type="text" class="form-control" placeholder="Silahkan isi kode sub komponen" name="kdsubkomponen" id="kdsubkomponen" required>
                        <label class="control-label" id="err_kdsubkomponen"></label>
                    </div>
                    <div class="form-group" id='err_div_ursubkomponen'>
                        <label>Nama Sub Komponen *</label>
                        <input type="text" class="form-control" placeholder="Isi nama sub komponen" name="ursubkomponen" id="ursubkomponen" required>
                        <label class='control-label' id="err_ursubkomponen"></label>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#form_modal').submit(function(e) {
            e.preventDefault(); //gunanya untuk supaya ga refresh

            var idkomponen = $('#m_komponen').val();

            var form_data = new FormData();

            form_data.append('kdsubkomponen', $('#kdsubkomponen').val());
            form_data.append('ursubkomponen', $('#ursubkomponen').val());
            form_data.append('idkomponen', idkomponen);
            form_data.append('idsubkomponen', idkomponen);


            console.log($('#kdsubkomponen').val());

            $.ajax({
                url: '<?php echo base_url(); ?>revisi_pok/save_subkomponen/add',
                dataType: 'json',
                data: form_data,
                type: 'post',
                cache: false,
                contentType: false,
                processData: false,
                success: function(response) {
                    console.log(response);
                    if (response.success == true) {


                        if (idkomponen == null) {
                            alert('Komponen belum dipilih.');
                        } else {
                            toastr["success"]("Data berhasil disimpan.", "Perhatian");
                            $('#modalEdit').modal('hide');
                            var link = "revisi_pok/load_sub_komponen/" + idkomponen;
                            LoadData(link, "tabelx");
                        }
                    } else {
                        $.each(response.messages, function(nama_field, value) {
                            //console.log('nama field: ' + nama_field);
                            //console.log('pesan field: ' + value);
                            $('#err_div_' + nama_field).addClass('has-error');
                            $('#err_' + nama_field).html(value);

                        });

                    }
                }

            });
        });
    });
</script>