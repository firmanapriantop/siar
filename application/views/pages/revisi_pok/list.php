<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<span class="caption-subject font-green-sharp bold uppercase">Data </span>
		</div>
		<div class="tools">
			<?php
			if ($role == 'staf' && $state = 'open') { ?>
				<button type="button" class="btn btn-success" onclick="CallModal('<?= base_url() ?>revisi_pok/add_subkomponen/<?= $idkomponen ?>', 'tmpModal', 'modalEdit')"><i class="fa fa-plus-square"></i> Tambah Sub Komponen</button>
			<?php } ?>

		</div>
	</div>
	<div class="portlet-body">
		<table class="table table-striped- table-bordered table-hover" id="tabel">
			<thead>
				<tr>
					<th>Kode</th>
					<th>Sub Komponen / Item</th>
					<th>Volume</th>
					<th>Satuan</th>
					<th>Harga Satuan</th>
					<th>Nilai</th>
					<th>Status</th>
					<th>Kasubdit</th>
					<th>Pokjacan</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php
				//subkomponen
				$submit = '';
				foreach ($data->result() as $row) :
					$kode_until_komp = $kdprogram . "." . $row->kdgiat . "." . $row->kdib . "." . $row->kdoutput . "." . $row->kdsuboutput . "." . $row->kdkomponen . ".";
					$submit = $row->submit;
				?>

					<tr class="warning">
						<td><?= $kode_until_komp . $row->kode; ?></td>
						<td><?= $row->nama; ?></td>
						<td></td>
						<td></td>
						<td></td>
						<td class="text-right"><?= number_format($row->nilai, 2); ?></td>
						<td class="text-center">
							<?php
							if (is_null($row->status) || empty($row->status)) {
								$status = "ready";
							} else {
								$status = $row->status;
							}
							if ($status == 'ready') {
								$status_class = 'primary';
							} elseif ($status == 'pending-add' || $status == 'pending-edit') {
								$status_class = 'warning';
							} elseif ($status == 'suspended' || $status == 'pending-remove') {
								$status_class = 'danger';
							}
							?>
							<span class="label label-sm label-rounded label-<?= $status_class ?>"><?= $status ?></span>
							<?php
							if ($row->submit == 1) {
								echo '<i class="fa fa-send-o"></i>';
							}
							?>
						</td>
						<td class="text-center">
							<?php
							if ($row->approval_kasubdit == 1) {
								$class_aksi = "text-success";
								$hasil_aksi =  '<i class="approval fa fa-check-square"></i>';
							} elseif ($row->approval_kasubdit == 2) {
								$class_aksi = "text-danger";
								$hasil_aksi =  '<i class="approval fa fa-times-circle"></i>';
							} else {
								$class_aksi = "text-default";
								$hasil_aksi =  '<i class="approval fa fa-square"></i>';
							}

							?>
							<a class="<?= $class_aksi ?>" href="#" onclick="approval_kasubdit('subkomponen', '<?= $row->id ?>', '<?= $row->submit ?>', '<?= $role; ?>', '<?= $state ?>')"><?= $hasil_aksi ?>
							</a>
						</td>
						<td class="text-center">
							<?php
							if ($row->approval_pokjacan == 1) {
								$class_aksi = "text-success";
								$hasil_aksi =  '<i class="approval fa fa-check-square"></i>';
							} elseif ($row->approval_pokjacan == 2) {
								$class_aksi = "text-danger";
								$hasil_aksi =  '<i class="approval fa fa-times-circle"></i>';
							} else {
								$class_aksi = "text-default";
								$hasil_aksi =  '<i class="approval fa fa-square"></i>';
							}

							?>
							<a class="<?= $class_aksi ?>" href="#" onclick="approval_pokjacan('subkomponen', '<?= $row->id ?>', '<?= $row->submit ?>', '<?= $row->approval_kasubdit ?>', '<?= $role ?>', '<?= $state ?>')"><?= $hasil_aksi ?>
							</a>
						</td>
						<td>
							<div class="btn-group" id="dropdown-menu">
								<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
								<ul class="dropdown-menu" role="menu">
									<?php
									if ($row->submit <> 1 && $role == 'staf' && $state == 'open') {
									?>
										<li><a href="#" onclick="CallModal('revisi_pok/edit_subkomponen/<?php echo $row->id; ?>', 'tmpModal', 'modalEdit')">Edit</a></li>
										<li><a href="#" onclick="hapus_sub_komponen('<?= $row->id ?>', '<?= $row->status ?>')">Hapus</a></li>

										<li class="divider"></li>
										<li>
											<a href="#" onclick="CallModal('revisi_pok/add_item/<?php echo $row->id; ?>', 'tmpModal', 'modalEdit')">Tambah Item</a>
										</li>

										<li>
											<a href="#" onclick="submit_sub_komponen('<?php echo $row->id; ?>', '1')"><i class="fa fa-send-o"></i> Submit</a>
										</li>
									<?php } elseif ($row->submit == 1 && $state == 'open' && ($role == 'kasubdit' || $role == 'pokjacan')) { ?>
										<li>
											<a href="#" onclick="submit_sub_komponen('<?php echo $row->id; ?>', '0')"><i class="fa fa-send-o"></i> UnSubmit</a>
										</li>
									<?php } ?>
									<li><a href="#" onclick="CallModal('revisi_pok/lihat_rekam/subkomponen/<?php echo $row->id; ?>', 'tmpModal', 'modalEdit')">Lihat Rekam</a></li>
								</ul>
							</div>
						</td>

					</tr>
					<?php


					$no_c = 0;
					$CI = &get_instance();

					//kode akun
					/*$query = "
						select 
							distinct(a.kdakun) as kdakun,
							a.nmakun,
							sum(i.jumlah) as jumlah,
							i.idakun as idakun
						from rt_item i
						inner join rm_akun a on a.idakun = i.idakun
						where i.idsubkomponen = '$row->id'
						order by a.nmakun, i.noitem, i.nmitem
					";*/

					$query = "
						SELECT 
							i.idakun as idakun,
							a.kdakun as kdakun,
							a.nmakun as nmakun,
						sum(i.jumlah) as jumlah
						from rt_item as i
						left join rm_akun a on a.idakun = i.idakun
						where i.idsubkomponen='$row->id'
						group by a.kdakun";

					//cek akun per idsubkomponen
					$baris = $CI->db->query($query);

					foreach ($baris->result() as $i) :
						if (is_null($i->idakun)) {
							//no action
						} else {
							echo "
						<tr class='danger'>
							<td>" . $kode_until_komp  . $row->kode . "." . $i->kdakun . "</td>
							<td>" .  $i->nmakun . "</td>
							<td class='text-right'></td>
							<td class='text-right'></td>
							<td class='text-right'></td>
							<td class='text-right'>" . number_format($i->jumlah, 2) . "</td>
							<td class='text-center'></td>
							<td><!-- kolom approval kasubdit --></td>
							<td><!-- kolom approval pokjacan --></td>
							<td><!-- kolom aksi --></td>
						</tr>
							";
					?>

							<?php

							//list_item
							$query_item = "
										select 
											i.iditem as id,
											a.nmakun as nama_akun,
											i.noitem as kode,
											i.nmitem as nama,
											i.volkeg as volume,
											i.satkeg as satkeg,
											i.hargasat as hargasatuan,
											i.jumlah as nilai,
											i.status,
											i.approval_kasubdit,
											i.cat_approval_kasubdit,
											i.approval_pokjacan,
											i.cat_approval_pokjacan
										from rt_item i
										left join rm_akun a on a.idakun = i.idakun
										where i.idsubkomponen = '$row->id' and i.idakun ='$i->idakun'
										order by i.idakun
									";
							$baris_item = $CI->db->query($query_item);
							foreach ($baris_item->result() as $j) :
								if (is_null($j->status) || empty($j->status)) {
									$status_item = 'ready';
								} else {
									$status_item = $j->status;
								}
								if ($status_item == 'ready') {
									$status_class = 'primary';
								} elseif ($status_item == 'pending-add' || $status_item == 'pending-edit') {
									$status_class = 'warning';
								} elseif ($status_item == 'suspended' || $status_item == 'pending-remove') {
									$status_class = 'danger';
								}
								echo "
					<tr>
						<td>" . $kode_until_komp . $row->kode . "." . $i->kdakun . "." . $j->kode . "</td>
						<td>" . $j->nama . "</td>
						<td class='text-right'>" . number_format($j->volume, 2) . "</td>
						<td class='text-left'>" . $j->satkeg . "</td>
						<td class='text-right'>" . number_format($j->hargasatuan, 2) . "</td>
						<td class='text-right'>" . number_format($j->nilai, 2) . "</td>
						<td class='text-center'><span class='label label-sm label-" . $status_class . "'>" . $status_item . "</span></td>
						"; ?>

								<td class="text-center">
									<?php

									if ($j->approval_kasubdit == 1) {
										$class_aksi = "text-success";
										$hasil_aksi =  '<i class="approval fa fa-check-square"></i>';
									} elseif ($j->approval_kasubdit == 2) {
										$class_aksi = "text-danger";
										$hasil_aksi =  '<i class="approval fa fa-times-circle"></i>';
									} else {
										$class_aksi = "text-default";
										$hasil_aksi =  '<i class="approval fa fa-square"></i>';
									}
									?>
									<a class="<?= $class_aksi ?>" href="#" onclick="approval_kasubdit('item', '<?= $j->id ?>', '<?= $submit ?>', '<?= $role ?>', '<?= $state ?>')"><?= $hasil_aksi ?>
									</a>
								</td>
								<td class="text-center">
									<?php

									if ($j->approval_pokjacan == 1) {
										$class_aksi = "text-success";
										$hasil_aksi =  '<i class="approval fa fa-check-square"></i>';
									} elseif ($j->approval_pokjacan == 2) {
										$class_aksi = "text-danger";
										$hasil_aksi =  '<i class="approval fa fa-times-circle"></i>';
									} else {
										$class_aksi = "text-default";
										$hasil_aksi =  '<i class="approval fa fa-square"></i>';
									}
									?>
									<a class="<?= $class_aksi ?>" href="#" onclick="approval_pokjacan('item', '<?= $j->id ?>', '<?= $submit ?>', '<?= $j->approval_kasubdit ?>', '<?= $role ?>', '<?= $state ?>')"><?= $hasil_aksi ?>
									</a>
								</td>
								<td>
									<div class="btn-group" id="dropdown-menu">
										<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
										<ul class="dropdown-menu" role="menu">
											<?php if ($row->submit <> 1 && $role == 'staf' && $state == 'open') { ?>
												<li><a class='dropdown-item' href='#' onclick="CallModal('revisi_pok/edit_item/<?php echo $j->id; ?>', 'tmpModal', 'modalEdit')">Edit</a></li>
												<li><a href="#" onclick="hapus_item('<?= $j->id ?>', '<?= $j->status ?>')">Hapus</a></li>
											<?php } ?>
											<li><a href="#" onclick="CallModal('revisi_pok/lihat_rekam/item/<?php echo $j->id; ?>', 'tmpModal', 'modalEdit')">Lihat Rekam</a></li>
										</ul>
									</div>
								</td>

								</tr>
							<?php endforeach; ?>
						<?php
						}

						?>
					<?php endforeach; ?>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>

<style>
	.dropdown-menu {
		position: relative;
		float: none;
		width: 160px;
	}

	.approval {
		position: relative;
		font-size: 20px;
	}
</style>

<script>
	function approval_pokjacan(tipe, id, submit, kasubdit, role, state) {
		if (submit == 1 && kasubdit != 0 && role == 'pokjacan' && state == 'open') {
			var url = '<?= base_url() ?>revisi_pok/approval_pokjacan/' + tipe + '/' + id;
			CallModal(url, 'tmpModal', 'modalEdit')
		} else {
			swal("Perhatian!", "Belum dapat melakukan penelaahan. Cek submit dan penelaahan kasubdit. Approval ini hanya dapat dilakukan oleh Pokjacan", "warning");
		}
		console.log(submit + ' , ' + kasubdit + ' , ' + role + ' , ' + state);
	}

	function approval_kasubdit(tipe, id, submit, role, state) {

		if (submit == 1 && role == 'kasubdit' && state == 'open') {
			var url = '<?= base_url() ?>revisi_pok/approval_kasubdit/' + tipe + '/' + id;
			CallModal(url, 'tmpModal', 'modalEdit')
		} else {
			swal("Perhatian!", tipe + " belum disubmit. Kasubdit tidak dapat melakukan penelaahaan. Cek Role.", "warning");
		}
		console.log(tipe = ' , ' + id + ' , ' + submit + ' ,  ' + role + ' , ' + state);

	}

	function edit_subkomponen(id) {
		$.ajax({
			url: '<?= "revisi_pok/edit_subkomponen" ?>',
			beforeSend: function() {
				//showLoading();
				//$('#loader').fadeOut(1000);
			},
			success: function(response) {
				$("#" + bgmodal).html(response);
				$('#' + modal).modal('show');
			},
			dataType: "html"
		});
	}

	function submit_sub_komponen(id, value) {
		swal({
				title: "Submit",
				text: "Apakah Anda yakin untuk submit?",
				type: "warning",
				showCancelButton: true,
				cancelButtonClass: "btn-warning",
				cancelButtonText: "Tidak",
				confirmButtonText: "Ya",
				closeOnConfirm: false,
				closeOnCancel: false
			},
			function(isConfirm) {
				if (isConfirm) {
					$.ajax({
						url: '<?= base_url() ?>revisi_pok/submit_sub_komponen',
						type: 'post',
						data: {
							id: id,
							value: value
						},
						error: function() {
							alert('Something is wrong');
						},
						success: function(data) {
							var idkomponen = $('#m_komponen').val();
							var link = "revisi_pok/load_sub_komponen/" + idkomponen;
							LoadData(link, "tabelx");
							console.log(data);
							swal("", "Submit Sub Komponen berhasil dilakukan.", "success");
						}
					});
				} else {
					swal("", "Hapus dibatalkan", "error");
				}
			});
	}

	function hapus_sub_komponen(id, status) {
		swal({
				title: "Hapus Sub Komponen",
				text: "Apakah Anda yakin untuk menghapus sub komponen ini?",
				type: "warning",
				showCancelButton: true,
				cancelButtonClass: "btn-danger",
				cancelButtonText: "Tidak",
				confirmButtonText: "Ya",
				closeOnConfirm: false,
				closeOnCancel: false
			},
			function(isConfirm) {
				if (isConfirm) {
					$.ajax({
						url: '<?= base_url() ?>revisi_pok/hapus_sub_komponen',
						type: 'post',
						data: {
							id: id,
							status: status
						},
						error: function() {
							alert('Something is wrong');
						},
						success: function(data) {
							var idkomponen = $('#m_komponen').val();
							var link = "revisi_pok/load_sub_komponen/" + idkomponen;
							LoadData(link, "tabelx");
							console.log(data);
							swal("", "Hapus sub komponen berhasil dilakukan.", "success");
						}
					});
				} else {
					swal("", "Hapus dibatalkan", "error");
				}
			});
	}

	function hapus_item(id, status) {
		swal({
				title: "Hapus Items",
				text: "Apakah Anda yakin untuk menghapus Item ini?",
				type: "warning",
				showCancelButton: true,
				cancelButtonClass: "btn-danger",
				cancelButtonText: "Tidak",
				confirmButtonText: "Ya",
				closeOnConfirm: false,
				closeOnCancel: false
			},
			function(isConfirm) {
				if (isConfirm) {
					$.ajax({
						url: '<?= base_url() ?>revisi_pok/hapus_item',
						type: 'post',
						data: {
							id: id,
							status: status
						},
						error: function() {
							alert('Something is wrong');
						},
						success: function(data) {
							var idkomponen = $('#m_komponen').val();
							var link = "revisi_pok/load_sub_komponen/" + idkomponen;
							LoadData(link, "tabelx");
							console.log(data);
							swal("", "Hapus item berhasil dilakukan.", "success");
						}
					});
				} else {
					swal("", "Hapus dibatalkan", "error");
				}
			});
	}

	$(document).ready(function() {
		$('#tabel thead tr').clone(true).appendTo('#tabel thead');
		$('#tabel thead tr:eq(1) th').each(function(i) {
			var title = $(this).text();
			switch (title) {
				case "Kode":
				case "Sub Komponen / Item":
				case "Volume":
				case "Satuan":
				case "Harga Satuan":
				case "Nilai":
				case "Status":
				case "Kasubdit":
				case "Pokjacan":
					$(this).html('<input type="text" class="form-control form-control-sm form-filter m-input" placeholder="Cari..." />');
					break;
			};

			$('input', this).on('keyup change', function() {
				if (table.column(i).search() !== this.value) {
					table
						.column(i)
						.search(this.value)
						.draw();
				}
			});
		});

		var table = $('#tabel').DataTable({
			orderCellsTop: true,
			fixedHeader: true,
			responsive: !0,
			"columnDefs": [{
				"targets": -1,
				"orderable": false
			}]
		});



		$('#confirm-delete').on('show.bs.modal', function(e) {
			// get information to update quickly to modal view as loading begins
			var opener = e.relatedTarget; //this holds the element who called the modal

			//we get details from attributes
			var id = $(opener).attr('data-id');
			var fullname = $(opener).attr('full-name');

			$('.title').text(' ' + fullname + ' ');
			$('.confirm-hapus').data('id', id);
		});

		$('#confirm-hapus').click(function() {
			var ID = $(this).data('id');
			$.ajax({
				url: "<?php echo base_url(); ?>dokumen_koordinasi/delete/" + ID,
				type: "post",
				data: ID,
				success: function(data) {
					$("#confirm-delete").modal('hide');
					//$("#deleted").modal('show');
					LoadData("<?php echo base_url(); ?>dokumen_koordinasi/show_data", "tabelx");
				}
			});
		});


	});
</script>