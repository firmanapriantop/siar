<div class="modal fade" id="modalEdit" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Item</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <!--begin::Form-->
            <form id="form_modal" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Kode Akun *</label>
                        <div class="input-group col-md-12">
                            <select class="form-control" id="idakun" name="idakun">
                            </select>
                        </div>
                        <div class='invalid-feedback' id="err_idakun"></div>
                    </div>
                    <div class="form-group">
                        <label>Kode Item *</label>
                        <input type="text" class="form-control" placeholder="Silahkan isi kode item" name="noitem" id="noitem" required>
                        <div class='invalid-feedback' id="err_noitem"></div>
                    </div>
                    <div class="form-group">
                        <label>Nama Item *</label>
                        <input type="text" class="form-control" placeholder="Isi nama item" name="nmitem" id="nmitem" required>
                        <div class='invalid-feedback' id="err_nmitem"></div>
                    </div>
                    <div class="form-group">
                        <label>Volume *</label>
                        <input type="number" class="form-control" placeholder="Isi volume" name="volkeg" id="volkeg" required>
                        <div class='invalid-feedback' id="err_volkeg"></div>
                    </div>
                    <div class="form-group">
                        <label>Satuan *</label>
                        <input type="text" class="form-control" placeholder="Isi satuan" name="satkeg" id="satkeg" required>
                        <div class='invalid-feedback' id="err_satkeg"></div>
                    </div>
                    <div class="form-group">
                        <label>Harga Satuan *</label>
                        <input type="number" class="form-control" placeholder="Isi harga satuan" name="hargasat" id="hargasat" required>
                        <div class='invalid-feedback' id="err_hargasat"></div>
                    </div>
                    <div class="form-group">
                        <label>Nilai *</label>
                        <input type="text" class="form-control" name="jumlah" id="jumlah" disabled="disabled" required>
                        <div class='invalid-feedback' id="err_jumlah"></div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
</div>

<script type="text/javascript">
    function loadakun() {
        $.ajax({
            url: '<?= base_url("revisi_pok/load_akun") ?>',
            type: 'post',
            datatype: 'json',
            success: function(response) {
                var data = JSON.parse(response);
                var len = data.length;
                $("#idakun").empty();
                $('#idakun').append("<option value=''></option>");
                for (var i = 0; i < len; i++) {
                    var id = data[i]['idakun'];
                    var kode = data[i]['kdakun'];
                    var nama = data[i]['nmakun'];
                    $("#idakun").append("<option value='" + id + "'>" + kode + "-" + nama + "</option>");
                }
            }
        });
    }

    $(document).ready(function() {
        loadakun();

        $('#idakun').select2({});


        $('#volkeg, #hargasat').on('input', function(e) {
            var volkeg = $('#volkeg').val();
            var hargasat = $('#hargasat').val();
            var jumlah = volkeg * hargasat;

            var output_jumlah = parseInt(jumlah).toLocaleString();

            $('#jumlah').val(jumlah);

        });

        $('#form_modal').submit(function(e) {
            e.preventDefault(); //gunanya untuk supaya ga refresh

            var idkomponen = $('#m_komponen').val();
            var idsubkomponen = <?= $idsubkomponen; ?>;

            var form_data = new FormData();

            form_data.append('idakun', $('#idakun').val());
            form_data.append('idsubkomponen', idsubkomponen);
            form_data.append('noitem', $('#noitem').val());
            form_data.append('nmitem', $('#nmitem').val());
            form_data.append('volkeg', $('#volkeg').val());
            form_data.append('satkeg', $('#satkeg').val());
            form_data.append('hargasat', $('#hargasat').val());
            form_data.append('jumlah', $('#jumlah').val());


            console.log(form_data);

            $.ajax({
                url: '<?php echo base_url(); ?>revisi_pok/save_item/add',
                dataType: 'json',
                data: form_data,
                type: 'post',
                cache: false,
                contentType: false,
                processData: false,
                success: function(response) {
                    console.log(response);
                    if (response.success == true) {

                        if (idkomponen == null) {
                            alert('Komponen belum dipilih.');
                        } else {
                            toastr["success"]("Data berhasil disimpan.", "Perhatian");
                            $('#modalEdit').modal('hide');
                            var link = "revisi_pok/load_sub_komponen/" + idkomponen;
                            LoadData(link, "tabelx");
                        }
                    } else {
                        $.each(response.messages, function(nama_field, value) {
                            console.log('nama field: ' + nama_field);
                            console.log('pesan field: ' + value);

                            var element = $('#' + nama_field);
                            element.addClass('is-invalid');
                            $('#err_' + nama_field).html(value);
                        });

                    }
                }

            });
        });
    });
</script>