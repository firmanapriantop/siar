<div class="m-content">

	<!--begin::Portlet-->
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Edit Dokumen Koordinasi
					</h3>
				</div>
			</div>
		</div>

		<!--begin::Form-->
		<form class="m-form m-form--fit m-form--label-align-right" id="m_form_1" method="POST" action="<?php echo base_url(); ?>dokumen_koordinasi/save/<?= $data->id ?>">
			<div class="m-portlet__body">
				<div class="m-form__content">
					<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_1_msg">
						<div class="m-alert__icon">
							<i class="la la-warning"></i>
						</div>
						<div class="m-alert__text">
							Silahkan perbaiki dan lengkapi data anda!
						</div>
						<div class="m-alert__close">
							<button type="button" class="close" data-close="alert" aria-label="Close">
							</button>
						</div>
					</div>
				</div>

				<div class="form-group m-form__group row">
					<label class="col-form-label col-lg-3 col-sm-12">Kategori *</label>
					<div class="col-lg-4 col-md-9 col-sm-12">
						<select class="form-control m-select2" id="m_kategori" name="kategori">
							<option value="<?= $data->kategori ?>"><?= $data->kategori ?></option>
							<option value="Nota Dinas">Nota Dinas</option>
							<option value="Surat">Surat</option>
							<option value="Lainnya">Lainnya</option>
						</select>
						<span class="m-form__help">Pilih kategori dokumen</span>
					</div>
				</div>

				<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>

				<div class="form-group m-form__group row">
					<label class="col-form-label col-lg-3 col-sm-12">Nomor *</label>
					<div class="col-lg-4 col-md-9 col-sm-12">
						<input type="text" class="form-control m-input" name="nomor" placeholder="Nomor Dokumen" data-toggle="m-tooltip" value="<?= $data->nomor ?>">
						<span class="m-form__help">Isi Nomor Dokumen</span>
					</div>
				</div>

				<div class="form-group m-form__group row">
					<label class="col-form-label col-lg-3 col-sm-12">Perihal *</label>
					<div class="col-lg-4 col-md-9 col-sm-12">
						<input type="text" class="form-control m-input" name="perihal" placeholder="Perihal Dokumen" data-toggle="m-tooltip" value="<?= $data->perihal ?>">
						<span class="m-form__help">Perihal Dokumen</span>
					</div>
				</div>

				<div class="form-group m-form__group row">
					<label class="col-form-label col-lg-3 col-sm-12">Dari *</label>
					<div class="col-lg-4 col-md-9 col-sm-12">
						<input type="text" class="form-control m-input" name="dari" placeholder="Dari" data-toggle="m-tooltip" value="<?= $data->dari ?>">
						<span class="m-form__help">Dari</span>
					</div>
				</div>

				<div class="form-group m-form__group row">
					<label class="col-form-label col-lg-3 col-sm-12">Tanggal *</label>
					<div class="col-lg-4 col-md-9 col-sm-12">
						<div class="input-group date">
							<input type="text" class="form-control m-input" readonly id="m_datepicker_3" placeholder="Tanggal Dokuen" name="tanggal" value="<?= $data->tanggal ?>" />
							<div class="input-group-append">
								<span class="input-group-text">
									<i class="la la-calendar"></i>
								</span>
							</div>
						</div>
						<span class="m-form__help">Tanggal Dokumen</span>
					</div>
				</div>

				<div class="form-group m-form__group row">
					<label class="col-form-label col-lg-3 col-sm-12">File *</label>
					<div class="col-lg-4 col-md-9 col-sm-12">
						<input type="file" class="form-control m-input" name="input_gambar" id="input_gambar" placeholder="file" data-toggle="m-tooltip">
						<span class="m-form__help">Upload file dokumen</span>
					</div>
					<div id="msg"></div>
					<button type="button" class="btn btn-success" id="tbl_upload">Simpan</button>
					<td><span id="loading">Uploading...</span></td>

				</div>

				<div class="form-group m-form__group row">
					<label class="col-form-label col-lg-3 col-sm-12">Keterangan</label>
					<div class="col-lg-4 col-md-9 col-sm-12">
						<textarea class="form-control m-input" name="keterangan" placeholder="Keterangan"><?= $data->keterangan ?></textarea>
						<span class="m-form__help">Isi Keterangan</span>
					</div>
				</div>

			</div>
			<div class="m-portlet__foot m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions">
					<div class="row">
						<div class="col-lg-9 ml-lg-auto">
							<button type="submit" class="btn btn-success">Simpan</button>
							<button type="reset" class="btn btn-secondary" id="batal">Batal</button>
						</div>
					</div>
				</div>
			</div>
		</form>

		<!--end::Form-->
	</div>
</div>


<script src="<?= base_url() ?>/theme/vendors/jquery/dist/jquery.js" type="text/javascript"></script>
<script src="<?= base_url() ?>/theme/datepicker.js" type="text/javascript"></script>

<script type="text/javascript">
	//==untuk validasi form
	var FormControls = function() {
		//== Private functions

		var demo1 = function() {
			$("#m_form_1").validate({
				// define validation rules
				rules: {
					nomor: {
						required: true
					},
					perihal: {
						required: true
					},
					dari: {
						required: true
					},
					tanggal: {
						required: true
					},
					kategori: {
						required: true
					},

				},

				//override massage
				messages: {
					nomor: "Silahkan isi nomor pendaftaran formulasi",
					nama: "Silahkan isi nama formulasi",
					perusahaan: "Silahkan pilih perusahaan pemegang nomor pendaftaran",
					jenis: "Silahkan pilih jenis formulasi",
					penggunaan: "Silahkan pilih penggunaan formulasi",
					kategori: "Silahkan pilih kategori formulasi",
				},

				//display error alert on form submit  
				invalidHandler: function(event, validator) {
					var alert = $('#m_form_1_msg');
					alert.removeClass('m--hide').show();
					mUtil.scrollTop();
				},

				submitHandler: function(form) {
					form[0].submit(); // submit the form

				}
			});
		}


		return {
			// public functions
			init: function() {
				demo1();
			}
		};
	}();

	//== untuk option with search
	var Select2 = function() {
		//== Private functions
		var demos = function() {

			// basic
			$('#m_nama').select2({
				placeholder: "Pilih Formulasi",
				allowClear: true
			});

			// basic
			$('#m_kategori').select2({
				placeholder: "Pilih kategori dokumen",
				//allowClear: true
			});

		}


		//== Public functions
		return {
			init: function() {
				demos();
			}
		};
	}();

	function upload(url, loading, msg, view) {
		var data = new FormData();
		// ambil atribut file yg akan diupload, lalu masukkan ke variabel input_gambar
		data.append('input_gambar', $('#input_gambar')[0].files[0]);

		// Ambil isi dari textbox deskripsi
		//data.append('input_deskripsi', $("#input_deskripsi").val());

		$.ajax({
			url: url,
			type: 'POST',
			data: data,
			cache: false,
			processData: false,
			contentType: false,
			beforeSend: function() {
				msg.hide();
				loading.show();
			},
			success: function(response) {
				// Pisahkan hasil (output) proses dengan tanda pemisah <|>
				console.log(response);
				$.each(response.messages, function(key, value) {
					var element = $('#' + key);
				});
				alert(response.messages);
				var result = response.split("<|>");

				if (result[0] == "SUCCESS") { // Jika sukses
					loading.hide(); // sembunyikan loadingnya
					msg.css({
						"color": "green"
					}).html(result[1]).show(); // tampilkan pesan sukses
					view.html(result[2]); // Load ulang tabel view nya
				} else { // Jika gagal
					loading.hide(); // sembunyikan loadingnya
					msg.css({
						"color": "red"
					}).html(result[1]).show(); // tampilkan pesan error
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(xhr.responseText);
			}
		});
	}


	$(document).ready(function() {

		$('#batal').click(function() {
			window.location.href = '<?= base_url() ?>dokumen_koordinasi';
		});


		//get_formulasi();
		FormControls.init();
		Select2.init();
		$('#loading').hide();

		$('#tbl_upload').click(function() {
			upload('<?= base_url() ?>dokumen_koordinasi/upload_file', $('#loading'), $('#msg'), $('#view'));
		});

	});
</script>