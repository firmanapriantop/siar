<div class="modal fade" id="modalEdit" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">History Sub Komponen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-striped- table-bordered table-hover" id="tabel_log">
                    <thead>
                        <tr>
                            <th>Aksi</th>
                            <th>User</th>
                            <th>Timestamp</th>
                            <th>Kode</th>
                            <th>Nama</th>
                            <th>Submit</th>
                            <th>Approval Kasubdit</th>
                            <th>Cat Kasubdit</th>
                            <th>Approval Pokjacan</th>
                            <th>Cat Pokjacan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data->result() as $i) : ?>
                            <tr>
                                <td><?= $i->aksi ?></td>
                                <td><?= $i->user ?></td>
                                <td><?= $i->dateupdate ?></td>
                                <td><?= $i->kdsubkomponen ?></td>
                                <td><?= $i->ursubkomponen ?></td>
                                <td class="text-center"><?= $i->submit ?></td>
                                <td class="text-center"><i class="fa fa-lg <?= $i->kasubdit ?>"></i></td>
                                <td><i class="fa fa-lg <?= $i->cat_approval_kasubdit ?>"></i></td>
                                <td class="text-center"><i class="fa fa-lg <?= $i->pokjacan ?>"></i></td>
                                <td><i class="fa fa-lg <?= $i->cat_approval_pokjacan ?>"></i></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#tabel_log').DataTable({

            responsive: !0,
            ordering: false
        });

    });
</script>