<div class="modal fade" id="modalEdit" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Penelaahan Kasubdit <?= $tipe ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <!--begin::Form-->
            <form id="form_modal" method="post">
                <div class="modal-body">
                    <div class="form-group" id='err_div_approval_kasubdit'>
                        <label class="control-label">Aksi *</label>
                        <div class="input-group col-md-12">
                            <select class="form-control" id="approval_kasubdit" name="approval_kasubdit">
                                <option value="<?= $data->approval_kasubdit ?>"><?= $data->aksi ?></option>
                                <option value="1">Disetujui</option>
                                <option value="2">Ditolak</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" id='err_div_cat_approval_kasubdit'>
                        <label>Catatan *</label>
                        <textarea class="form-control" name="cat_approval_kasubdit" id="cat_approval_kasubdit"><?= $data->cat_approval_kasubdit ?></textarea>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
</div>

<script>
    $('#approval_kasubdit').select2({});

    $(document).ready(function() {
        $('#form_modal').submit(function(e) {
            e.preventDefault(); //gunanya untuk supaya ga refresh

            var idkomponen = $('#m_komponen').val();

            var form_data = new FormData();

            form_data.append('id', '<?= $data->id ?>');
            form_data.append('approval_kasubdit', $('#approval_kasubdit').val());
            form_data.append('cat_approval_kasubdit', $('#cat_approval_kasubdit').val());

            $.ajax({
                url: '<?php echo base_url(); ?>revisi_pok/save_approval_kasubdit/<?= $tipe ?>/kasubdit',
                dataType: 'json',
                data: form_data,
                type: 'post',
                cache: false,
                contentType: false,
                processData: false,
                success: function(response) {
                    console.log(response);
                    if (response.success == true) {


                        if (idkomponen == null) {
                            alert('Komponen belum dipilih.');
                        } else {
                            toastr["success"]("Data berhasil disimpan.", "Perhatian");
                            $('#modalEdit').modal('hide');
                            var link = "revisi_pok/load_sub_komponen/" + idkomponen;
                            LoadData(link, "tabelx");
                        }
                    } else {
                        $.each(response.messages, function(nama_field, value) {
                            //console.log('nama field: ' + nama_field);
                            //console.log('pesan field: ' + value);
                            $('#err_div_' + nama_field).addClass('has-error');
                            $('#err_' + nama_field).html(value);

                        });

                    }
                }

            });
        });
    });
</script>