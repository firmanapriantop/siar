</br>
<h3 class="page-title">
	Revisi POK <small></small>
</h3>
</br>
<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">FILTER </span>
				</div>
				<div class="tools">
					<?php
					if ($role == 'direktur' && $state == 'open') {
						echo '
							<button type="button" class="btn btn-success" id="btn_submit_direktur"><i class="fa fa-send-o"></i> Submit Direktur</button>
							<button type="button" class="btn btn-danger" id="btn_unsubmit_direktur"><i class="fa fa-send-o"></i> UnSubmit Direktur</button>
							';
					} elseif ($role == 'bagcan' && $state == 'locked') {
						echo '
							<button type="button" class="btn btn-success" id="btn_submit_bagcan"><i class="fa fa-send-o"></i> Submit Bagcan</button>
							<button type="button" class="btn btn-danger" id="btn_unsubmit_bagcan"><i class="fa fa-send-o"></i> UnSubmit Bagcan</button>
							';
					} elseif ($role == 'admin' && $state == 'closed') {
						echo '
		
							<button type="button" class="btn btn-danger" id="btn_unsubmit_bagcan"><i class="fa fa-send-o"></i> UnSubmit Bagcan</button>
							';
					}
					?>
				</div>
			</div>
			<div class="portlet-body form">
				<form class="form-horizontal" role="form">
					<div class="form-body">
						<div class="form-group">
							<label class="col-md-3 control-label">Pilih Tahun RKA *</label>
							<div class="col-md-9">
								<select class="form-control" id="m_rka">
									<?php foreach ($rka->result() as $i) : ?>
										<option value="<?= $i->tahun ?>"><?= $i->tahun ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Pilih Program *</label>
							<div class="col-md-9">
								<select class="form-control" id="m_program">
									<?php foreach ($program->result() as $p) : ?>
										<option value="<?= $p->kdprogram ?>"><?= $p->kdprogram . "-" . $p->nmprogram ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Pilih Kegiatan *</label>
							<div class="col-md-9">
								<select class="form-control" id="m_kegiatan">
									<option></option>
									<?php foreach ($kegiatan->result() as $k) : ?>
										<option value="<?= $k->idgiat ?>"><?= $k->kdgiat . "-" . $k->nmgiat ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Pilih Output *</label>
							<div class="col-md-9">
								<select class="form-control" id="m_output">
									<option></option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Pilih Sub Output *</label>
							<div class="col-md-9">
								<select class="form-control" id="m_sub_output">
									<option></option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Pilih Komponen *</label>
							<div class="col-md-9">
								<select class="form-control" id="m_komponen">
									<option></option>
								</select>
							</div>
						</div>
					</div>

				</form>
				<div class="form-actions">
					<div class="row">
						<div class="col-md-offset-3 col-md-9">
							<button type="button" class="btn btn-success" id="btn_tampilkan">Tampilkan</button>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<div class="col-md-12" id="tabelx">

	</div>
</div>


<!-- <script src="<?php //base_url() 
					?>assets/muds.js" type="text/javascript"></script> -->

<script>
	$(document).ready(function() {

		$('#btn_submit_direktur').click(function(e) {
			swal({
					title: "Submit",
					text: "Apakah Anda yakin untuk Submit Hasil Penelaahan?",
					type: "warning",
					showCancelButton: true,
					cancelButtonClass: "btn-warning",
					cancelButtonText: "Tidak",
					confirmButtonText: "Ya",
					closeOnConfirm: false,
					closeOnCancel: false
				},
				function(isConfirm) {
					if (isConfirm) {
						$.ajax({
							url: '<?= base_url() ?>revisi_pok/ubah_state',
							type: 'post',
							data: {
								siapa: 'direktur',
								nilai: '1',
								aksi: 'submit'
							},
							error: function() {
								alert('Something is wrong');
							},
							success: function(data) {

								console.log(data);
								if (data == 0) {
									swal("", "Belum dapat dilakukan Submit. Masih ada Sub Komponen atau Item yang belum selesai ditelaah.", "warning");
								} else if (data == 1) {
									location.reload();
									swal("", "Submit oleh Direktur telah berhasil dilakukan.", "success");
								}
								//
							}
						});
					} else {
						swal("", "Unsubmit dibatalkan", "error");
					}
				});
		});

		$('#btn_unsubmit_direktur').click(function(e) {
			swal({
					title: "Unsubmit",
					text: "Apakah Anda yakin untuk Unsubmit?",
					type: "warning",
					showCancelButton: true,
					cancelButtonClass: "btn-danger",
					cancelButtonText: "Tidak",
					confirmButtonText: "Ya",
					closeOnConfirm: false,
					closeOnCancel: false
				},
				function(isConfirm) {
					if (isConfirm) {
						$.ajax({
							url: '<?= base_url() ?>revisi_pok/ubah_state',
							type: 'post',
							data: {
								siapa: 'direktur',
								nilai: '0',
								aksi: 'unsubmit'
							},
							error: function() {
								alert('Something is wrong');
							},
							success: function(data) {

								console.log(data);
								if (data == 0) {
									swal("", "Belum dapat dilakukan Submit. Masih ada Sub Komponen atau Item yang belum selesai ditelaah.", "warning");
								} else if (data == 1) {
									location.reload();
									swal("", "Unsubmit oleh Direktur telah berhasil dilakukan.", "success");
								}
							}
						});
					} else {
						swal("", "Unsubmit dibatalkan", "error");
					}
				});
		});

		$('#btn_submit_bagcan').click(function(e) {
			swal({
					title: "Submit",
					text: "Apakah Anda yakin untuk Submit Hasil Penelaahan?",
					type: "warning",
					showCancelButton: true,
					cancelButtonClass: "btn-warning",
					cancelButtonText: "Tidak",
					confirmButtonText: "Ya",
					closeOnConfirm: false,
					closeOnCancel: false
				},
				function(isConfirm) {
					if (isConfirm) {
						$.ajax({
							url: '<?= base_url() ?>revisi_pok/ubah_state',
							type: 'post',
							data: {
								siapa: 'bagcan',
								nilai: '1',
								aksi: 'submit'
							},
							error: function() {
								alert('Something is wrong');
							},
							success: function(data) {

								console.log(data);
								if (data == 0) {
									swal("", "Belum dapat dilakukan Submit. Masih ada Sub Komponen atau Item yang belum selesai ditelaah.", "warning");
								} else if (data == 1) {
									location.reload();
									swal("", "Submit oleh Bagcan telah berhasil dilakukan.", "success");
								}
								//
							}
						});
					} else {
						swal("", "Submit dibatalkan", "error");
					}
				});
		});

		$('#btn_unsubmit_bagcan').click(function(e) {
			swal({
					title: "Unsubmit",
					text: "Apakah Anda yakin untuk Unsubmit?",
					type: "warning",
					showCancelButton: true,
					cancelButtonClass: "btn-danger",
					cancelButtonText: "Tidak",
					confirmButtonText: "Ya",
					closeOnConfirm: false,
					closeOnCancel: false
				},
				function(isConfirm) {
					if (isConfirm) {
						$.ajax({
							url: '<?= base_url() ?>revisi_pok/ubah_state',
							type: 'post',
							data: {
								siapa: 'bagcan',
								nilai: '0',
								aksi: 'unsubmit'
							},
							error: function() {
								alert('Something is wrong');
							},
							success: function(data) {

								console.log(data);
								if (data == 0) {
									swal("", "Belum dapat dilakukan Submit. Masih ada Sub Komponen atau Item yang belum selesai ditelaah.", "warning");
								} else if (data == 1) {
									location.reload();
									swal("", "UnSubmit oleh Bagcan telah berhasil dilakukan.", "success");
								}
							}
						});
					} else {
						swal("", "Unsubmit dibatalkan", "error");
					}
				});
		});



		$('#m_rka').select2({
			placeholder: "Pilih RKA",
			allowClear: true
		});

		$('#m_program').select2({
			placeholder: "Pilih Program",
			allowClear: true
		});

		$('#m_kegiatan').select2({
			placeholder: "Pilih Kegiatan",
			allowClear: true
		});

		$('#m_output').select2({
			placeholder: "Pilih Output",
			allowClear: true
		});

		$('#m_sub_output').select2({
			placeholder: "Pilih Sub Output",
			allowClear: true
		});

		$('#m_komponen').select2({
			placeholder: "Pilih Komponen",
			allowClear: true
		});

		$('#m_kegiatan').change(function() {
			$('#m_output').val(null).trigger('change');
			loadoutput();
		});

		$('#m_output').change(function() {
			$('#m_sub_output').val(null).trigger('change');
			loadsuboutput();
		});

		$('#m_sub_output').change(function() {
			$('#m_komponen').val(null).trigger('change');
			loadkomponen();
		});

		$('#btn_tampilkan').click(function() {

			var idkomponen = $('#m_komponen').val();
			var kdrka = $('#m_rka').val();
			var kdprogram = $('#m_program').val();
			console.log(idkomponen.length);
			if (idkomponen.length == 0) {
				alert('Komponen belum dipilih.');
			} else {
				var link = "revisi_pok/load_sub_komponen/" + idkomponen + "/" + kdrka + "/" + kdprogram;

				LoadData(link, "tabelx");


			}

		});

	});

	function loadoutput() {
		var idgiat = $('#m_kegiatan').val();
		$.ajax({
			url: '<?= base_url("revisi_pok/load_output") ?>',
			type: 'post',
			data: {
				idgiat: idgiat
			},
			datatype: 'json',
			success: function(response) {
				var data = JSON.parse(response);
				var len = data.length;
				$("#m_output").empty();
				$('#m_output').append("<option value=''></option>");
				for (var i = 0; i < len; i++) {
					var id = data[i]['idoutput'];
					var kode = data[i]['kdoutput'];
					var nama = data[i]['nmoutput'];
					$("#m_output").append("<option value='" + id + "'>" + kode + "-" + nama + "</option>");
				}
			}
		});
	}

	function loadsuboutput() {
		var idoutput = $('#m_output').val();
		$.ajax({
			url: '<?= base_url("revisi_pok/load_sub_output") ?>',
			type: 'post',
			data: {
				idoutput: idoutput
			},
			datatype: 'json',
			success: function(response) {
				var data = JSON.parse(response);
				var len = data.length;
				$("#m_sub_output").empty();
				$('#m_sub_output').append("<option value=''></option>");
				for (var i = 0; i < len; i++) {
					var id = data[i]['idsuboutput'];
					var kode = data[i]['kdsuboutput'];
					var nama = data[i]['nmsuboutput'];
					$("#m_sub_output").append("<option value='" + id + "'>" + kode + "-" + nama + "</option>");
				}
			}
		});
	}

	function loadkomponen() {
		var idsuboutput = $('#m_sub_output').val();
		$.ajax({
			url: '<?= base_url("revisi_pok/load_komponen") ?>',
			type: 'post',
			data: {
				idsuboutput: idsuboutput
			},
			datatype: 'json',
			success: function(response) {
				var data = JSON.parse(response);
				var len = data.length;
				$("#m_komponen").empty();
				$('#m_komponen').append("<option value=''></option>");
				for (var i = 0; i < len; i++) {
					var id = data[i]['idkomponen'];
					var kode = data[i]['kdkomponen'];
					var nama = data[i]['nmkomponen'];
					$("#m_komponen").append("<option value='" + id + "'>" + kode + "-" + nama + "</option>");
				}
			}
		});
	}

	function loadsubkomponen() {
		var kdrka = $('#m_rka').val();
		var kdprogram = $('#m_program').val();
		var idkomponen = $('#m_komponen').val();
		$.ajax({
			url: '<?= base_url("revisi_pok/load_sub_komponen") ?>',
			type: 'post',
			data: {
				idkomponen: idkomponen,
				kdrka: kdrka,
				kdprogram: kdprogram
			},
			datatype: 'json',
			success: function(response) {
				var data = JSON.parse(response);
				var len = data.length;
				$("#m_komponen").empty();
				$('#m_komponen').append("<option value=''></option>");
				for (var i = 0; i < len; i++) {
					var id = data[i]['idkomponen'];
					var kode = data[i]['kdkomponen'];
					var nama = data[i]['nmkomponen'];
					$("#m_komponen").append("<option value='" + id + "'>" + kode + "-" + nama + "</option>");
				}
			}
		});
	}
</script>