<?php
$CI = &get_instance();
?>
<table class="table table-striped- table-bordered table-hover" id="tabel">
	<thead>
		<tr>
			<th>No</th>
			<th>Nama</th>
			<th>Keterangan</th>
			<th>Sub Komponen</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php $no = 0;
		foreach ($data->result() as $i) : $no++; ?>
			<tr>
				<td><?= $no ?></td>
				<td><?= $i->nama ?></td>
				<td><?= $i->keterangan ?></td>
				<td>
					<?php
					if (empty($i->subkomponen_ids)) {
						$where = "where a1.idsubkomponen = 0";
					} else {
						$where = "where a1.idsubkomponen in ($i->subkomponen_ids)";
					}
					$query = "
						select
							concat_ws('.', a5.kdgiat, a4.kdoutput, a3.kdsuboutput, a2.kdkomponen, a1.kdsubkomponen) as kode,
							a1.ursubkomponen as nama
						from rt_subkomponen a1
						left join rt_komponen a2 on a2.idkomponen = a1.idkomponen
						left join rt_suboutput a3 on a3.idsuboutput = a2.idsuboutput
						left join rm_output a4 on a4.idoutput = a3.idoutput
						left join rm_giat a5 on a5.idgiat = a4.idgiat
						$where
					";

					//var_dump($query);

					//cek akun per idsubkomponen
					$CI = &get_instance();
					$baris = $CI->db->query($query);
					echo "<ul>";
					foreach ($baris->result() as $k) :
						echo "<li>" . $k->kode . " - " . $k->nama . "</li>";
					endforeach;
					echo "</ul>";
					?>
				</td>
				<td class="text-center">
					<div class="btn-group" id="dropdown-menu">
						<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
						<ul class="dropdown-menu" role="menu">
							<li><a class='dropdown-item' href='#' onclick="CallModal('unit_kerja/edit/<?= $i->id; ?>', 'tmpModal', 'modalEdit')">Edit</a></li>
							<li><a class='dropdown-item' href="#" onclick="hapus('<?= $i->id ?>')">Hapus</a></li>
						</ul>
					</div>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<style>
	.dropdown-menu {
		position: relative;
		float: none;
		width: 160px;
	}

	.approval {
		position: relative;
		font-size: 20px;
	}
</style>

<script>
	function hapus(id) {
		swal({
				title: "Hapus unit kerja",
				text: "Apakah Anda yakin untuk menghapus unit kerja ini?",
				type: "warning",
				showCancelButton: true,
				cancelButtonClass: "btn-danger",
				cancelButtonText: "Tidak",
				confirmButtonText: "Ya",
				closeOnConfirm: false,
				closeOnCancel: false
			},
			function(isConfirm) {
				if (isConfirm) {
					$.ajax({
						url: '<?= base_url() ?>unit_kerja/hapus',
						type: 'post',
						data: {
							id: id
						},
						error: function() {
							alert('Something is wrong');
						},
						success: function(data) {
							var link = "unit_kerja/load_unit_kerja";
							LoadData(link, "tabelx");
							swal("", "Unit kerja berhasil dinonaktifkan", "success");
						}
					});
				} else {
					swal("", "Hapus dibatalkan", "error");
				}
			});
	}

	$(document).ready(function() {
		$('#tabel thead tr').clone(true).appendTo('#tabel thead');
		$('#tabel thead tr:eq(1) th').each(function(i) {
			var title = $(this).text();
			switch (title) {
				case "No":
				case "Nama":
				case "Keterangan":
				case "Sub Komponen":
					$(this).html('<input type="text" class="form-control form-control-sm form-filter m-input" placeholder="Cari..." />');
					break;
			};

			$('input', this).on('keyup change', function() {
				if (table.column(i).search() !== this.value) {
					table
						.column(i)
						.search(this.value)
						.draw();
				}
			});
		});
		var table = $('#tabel').DataTable({
			orderCellsTop: true,
			fixedHeader: true,
			responsive: !0,
			"columnDefs": [{
				"targets": -1,
				"orderable": false
			}]
		});
	});
</script>