<div class="modal fade" id="modalEdit" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tambah Unit Kerja</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i aria-hidden="true" class="ki ki-close"></i>
				</button>
			</div>
			<!--begin::Form-->
			<form id="form_modal" method="post">
				<div class="modal-body">

					<div class="form-group" id='err_div_nama'>
						<label class="control-label">Nama *</label>
						<input type="text" class="form-control" id="nama" name="nama" required></input>
						<label class='control-label' id="err_nama"></label>
					</div>

					<div class="form-group" id='err_div_keterangan'>
						<label>Keterangan *</label>
						<textarea class="form-control" id="keterangan" name="keterangan" required></textarea>
						<label class='control-label' id="err_keterangan"></label>
					</div>
					<div class="form-group" id='err_div_subkomponen_ids'>
						<label class="control-label">Pilih Sub Komponen *</label>
						<div class="input-group col-md-12">
							<select class="form-control select2" id="subkomponen_ids" name="subkomponen_ids" multiple required>
								<option></option>
								<?php
								foreach ($subkomponens->result() as $r) :
									echo '<option value="' . $r->idsubkomponen . '">' . $r->kode . '-' . $r->nama . '</option>';
								endforeach;
								?>
							</select>
						</div>
						<label class='control-label' id="err_subkomponen_ids"></label>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
				</div>
			</form>
			<!--end::Form-->
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {

		$('#subkomponen_ids').select2({});

		$('#form_modal').submit(function(e) {
			e.preventDefault(); //gunanya untuk supaya ga refresh

			var form_data = new FormData();

			form_data.append('nama', $('#nama').val());
			form_data.append('keterangan', $('#keterangan').val());
			form_data.append('subkomponen_ids', $('#subkomponen_ids').val());

			$.ajax({
				url: '<?php echo base_url(); ?>unit_kerja/save/add',
				dataType: 'json',
				data: form_data,
				type: 'post',
				cache: false,
				contentType: false,
				processData: false,
				success: function(response) {
					if (response.success == true) {
						toastr["success"]("Data berhasil disimpan.", "Perhatian");
						$('#modalEdit').modal('hide');
						var link = "unit_kerja/load_unit_kerja";
						LoadData(link, "tabelx");
					} else {
						$.each(response.messages, function(nama_field, value) {
							//console.log('nama field: ' + nama_field);
							//console.log('pesan field: ' + value);
							$('#err_div_' + nama_field).addClass('has-error');
							$('#err_' + nama_field).html(value);

						});

					}
				}

			});
		});
	});
</script>