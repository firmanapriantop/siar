</br>
<h3 class="page-title">
	Unit Kerja <small>Manajamen unit kerja</small>
</h3>
<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">Data </span>
				</div>
				<div class="tools">
					<button type="button" class="btn btn-success" id="btn_add"><i class="fa fa-plus-square"></i> Tambah</button>
				</div>
			</div>
			<div class="portlet-body" id="tabelx">
			</div>
		</div>
	</div>
</div>

<script src="<?= base_url() ?>assets/muds.js" type="text/javascript"></script>

<script>
	$(document).ready(function() {

		LoadData('<?= base_url('unit_kerja/load_unit_kerja') ?>', "tabelx");

		$("#pilihbanyak").select2({});
		$("#pilihbanyak").on('change', function(e) {
			console.log($('#pilihbanyak').val());
			alert($('#pilihbanyak').val());

		});

		$('#btn_add').click(function() {
			CallModal('<?= base_url() ?>unit_kerja/add', 'tmpModal', 'modalEdit')
		});


	});
</script>