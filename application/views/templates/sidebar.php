<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler">
                </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler">
                </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>
            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
            <li class="heading">
                <h3 class="uppercase">-</h3>
            </li>
            <?php
            $link = $this->uri->segment(1);
            ?>
            <li class="<?php echo ($link == 'dashboard' or empty($link)) ?  'active open' : ''; ?>">
                <a href="javascript:;">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <span class="arrow "></span>
                    <ul class="sub-menu">
                        <li class="<?php echo ($link == 'dashboard' or empty($link)) ?  'active' : ''; ?>">
                            <a href="<?= base_url() ?>">
                                <i class="icon-bulb"></i>
                                New Dashboard</a>
                        </li>
                    </ul>
                </a>
            </li>
            <li class="<?php echo ($link == 'lihat_pok' or $link == 'revisi_pok') ?  'active open' : ''; ?>">
                <a href="javascript:;">
                    <i class="icon-rocket"></i>
                    <span class="title">Anggaran</span>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                </a>
                <ul class="sub-menu">
                    <li  class="<?php echo ($link == 'lihat_pok' or $link == 'revisi_pok') ?  'active' : ''; ?>">
                        <a href="<?= base_url('lihat_pok') ?>">
                            Lihat POK</a>
                    </li>
                    <li class="<?php echo ($link == 'lihat_pok' or $link == 'revisi_pok') ?  'active' : ''; ?>">
                        <a href=" <?= base_url('revisi_pok') ?>">
                            Revisi POK</a>
                    </li>
                </ul>
            </li>
            <li class="<?php echo ($link == 'dokumen_koordinasi') ?  'active open' : ''; ?>">
                <a href="javascript:;">
                    <i class="fa fa-book"></i>
                    <span class="title">Dokumen</span>
                    <span class="arrow "></span>
                    <ul class="sub-menu">
                        <li class="<?php echo ($link == 'dokumen_koordinasi') ?  'active' : ''; ?>">
                            <a href="<?= base_url('dokumen_koordinasi') ?>">Dokumen Koordinasi</a>
                        </li>
                    </ul>
                </a>
            </li>
            <li class="<?php echo ($link == 'reviu') ?  'active open' : ''; ?>">
                <a href="javascript:;">
                    <i class="icon-puzzle"></i>
                    <span class="title">Laporan</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li class="<?php echo ($link == 'reviu') ?  'active' : ''; ?>">
                        <a href="<?= base_url('reviu') ?>">
                            Reviu</a>
                    </li>

                </ul>
            </li>
            <!-- END ANGULARJS LINK -->
            <?php if ($is_admin) { ?>
                <li class="heading">
                    <h3 class="uppercase">Administrasi</h3>
                </li>
                <li class="<?php echo ($link == 'user' || $link == 'role' || $link == 'akun' || $link == 'unit_kerja') ?  'active open' : ''; ?>">
                    <a href="javascript:;">
                        <i class="icon-settings"></i>
                        <span class="title">Manajemen</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="<?php echo ($link == 'user') ?  'active' : ''; ?>"><a href="<?= base_url('user') ?>">User</a></li>
                        <li class="<?php echo ($link == 'role') ?  'active' : ''; ?>"><a href="<?= base_url('role') ?>">User Role</a></li>
                        <li class="<?php echo ($link == 'unit_kerja') ?  'active' : ''; ?>"><a href="<?= base_url('unit_kerja') ?>">Unit Kerja</a></li>
                        <li class="<?php echo ($link == 'akun') ?  'active' : ''; ?>"><a href="<?= base_url('akun') ?>">Kode Akun</a></li>
                    </ul>
                </li>
            <?php } ?>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
<!-- END SIDEBAR -->