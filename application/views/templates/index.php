<!DOCTYPE html>
<html lang="en">

<!-- begin::Head -->

<head>
	<meta charset="utf-8" />
	<title><?= $title; ?></title>
	<meta name="description" content="Latest updates and statistic charts">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

	<?php
	$this->load->view('templates/css');
	$this->load->view('templates/js');
	?>
	<script src="<?= base_url() ?>assets/muds.js" type="text/javascript"></script>
	<link rel="shortcut icon" href="<?= base_url() ?>/assets/img/favicon/favicon.ico" />
</head>

<!-- end::Head -->

<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->

<body class="page-header-fixed page-quick-sidebar-over-content">
	<div id="tmpModal"></div>
	<?php $this->load->view('templates/header') ?>
	<div class="clearfix">
	</div>
	<!-- BEGIN CONTAINER -->
	<div class="page-container-bg-solid">
		<?php $this->load->view('templates/sidebar') ?>
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<?php
				$page = isset($page) ? $page : 'templates/content_empty';
				$this->load->view($page);
				?>
			</div>
		</div>
	</div>


	<!-- begin::Footer -->
	<?php $this->load->view('templates/footer') ?>
	<!-- end::Footer -->



</body>

<!-- end::Body -->

</html>