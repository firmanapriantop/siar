
<!-- BEGIN: Left Aside -->
<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
<?php
	$role_id = $this->session->userdata('role_id');
	$snav = get_left_navigation($role_id);
	
	//print_r($snav);
?>  
	<!-- BEGIN: Aside Menu -->
	<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
		<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
			
			<?php 
			// $no=1;
				foreach( $snav as $row ){ 
				
					if( empty($row->sub) )
					{// echo 'single'; 
						if ($row->sitemapid_parent=='0') {

							if ($this->uri->segment(1) == $row->url) {
								$menu_aktif = ' m-menu__item--active';
							} else {
								$menu_aktif = '';
							}
			?>
							<li class="m-menu__item<?= $menu_aktif ?>" aria-haspopup="true" id="<?= $row->sitemapid ?>">
								<a href="<?= base_url() . $row->url ?>" id="<?= $row->name ?> " class="m-menu__link ">
									<i class="m-menu__link-<?= $row->icon ?>"></i>
									<span class="m-menu__link-title"> 
										<span class="m-menu__link-wrap"> 
											<span class="m-menu__link-text"><?= $row->displayname ?></span>
										</span>
									</span>
								</a>
							</li>
			<?php
						}
					}else{
						//echo'doble';
						//echo $this->uri->segment(1);
						//echo '<'.$row->url;
						//print_r($this->uri-segment(1));

						foreach( $row->sub as $r ){ 
									
							if ($this->uri->segment(1) == $r->url) {
								$sub_menu = ' m-menu__item--open m-menu__item--expanded';
							} else {
								$sub_menu='';
							}
						}

			?>
												
					<li class="m-menu__item  m-menu__item--submenu<?= $sub_menu ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
						<a href="javascript:;" class="m-menu__link m-menu__toggle">
							<i class="m-menu__link-<?= $row->icon ?>"></i>
							<span class="m-menu__link-text"><?= $row->displayname ?></span>
							<i class="m-menu__ver-arrow la la-angle-right"></i>
						</a>
						<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
							<ul class="m-menu__subnav">
								<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
									<span class="m-menu__link">
										<span class="m-menu__link-text"><?= $row->displayname ?></span>
									</span>
								</li>
							<?php 
								foreach( $row->sub as $r ){ 
									
									if ($this->uri->segment(1) == $r->url) {
										$sub_menu_aktif = ' m-menu__item--active';
									} else {
										
										$sub_menu_aktif = '';
									}
								?>

								
								<li class="m-menu__item<?= $sub_menu_aktif ?>" aria-haspopup="true">
									<a href="<?= base_url() . $r->url ?>" id="<?= $r->name ?> " class="m-menu__link ">
										<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
										<span class="m-menu__link-text"><?= $r->displayname ?></span>
									</a>
								</li>
							<?php } ?>
							</ul>
						</div>
					</li>
			<?php
					}
					//echo $no=$no+1;
				}
			?>
<!--
			<li class="m-menu__item  m-menu__item--submenu m-menu__item--open m-menu__item--expanded" aria-haspopup="true" m-menu-submenu-toggle="hover">
				<a href="javascript:;" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-exclamation"></i>
					<span class="m-menu__link-text">FAQS</span>
					<i class="m-menu__ver-arrow la la-angle-right"></i>
				</a>
				<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">
						<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
							<span class="m-menu__link">
								<span class="m-menu__link-text">FAQS</span>
							</span>
						</li>
						<li class="m-menu__item  m-menu__item--active" aria-haspopup="true">
							<a href="../../snippets/faq/faq-1.html" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">User</span>
							</a>
						</li>
					</ul>
				</div>
			</li>
-->
		</ul>
	</div>

	

	<!-- END: Aside Menu -->
</div>

<!-- END: Left Aside -->