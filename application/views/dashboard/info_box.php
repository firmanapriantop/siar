<div class="col-xl-3">
    <div class="m-portlet m--bg-accent m-portlet--bordered-semi m-portlet--skin-dark m-portlet--full-height ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Jumlah Formulasi
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="m-widget7 m-widget7--skin-dark">
                <h1 class="m-widget27__title m--font-light text-right">
                    <span><?= number_format($jlh_formulasi, 2) ?></span>
                </h1>
            </div>
        </div>
    </div>
</div>

<div class="col-xl-9">
    <div class="row">

        <div class="col-xl-3">
            <div class="m-portlet m--bg-primary m-portlet--bordered-semi m-portlet--skin-dark m-portlet--full-height ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Jumlah Stock Awal
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="m-widget7 m-widget7--skin-dark">
                        <h4 class="m-widget27__title m--font-light text-right">
                            <span><?php
                                        foreach($jlh_byk->result() as $row):
                                            echo number_format($row->jlh_stock, 2).' '.$row->satuan,'<br />'; 
                                        endforeach;
                                    ?></span>
                        </h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-3">
            <div class="m-portlet m--bg-success m-portlet--bordered-semi m-portlet--skin-dark m-portlet--full-height ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Jumlah Pengadaan
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="m-widget7 m-widget7--skin-dark">
                        <h4 class="m-widget27__title m--font-light text-right">
                        <span><?php
                                        foreach($jlh_byk->result() as $row):
                                            echo number_format($row->jlh_pengadaan, 2).' '.$row->satuan,'<br />'; 
                                        endforeach;
                                    ?></span>
                        </h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-3">
            <div class="m-portlet m--bg-danger m-portlet--bordered-semi m-portlet--skin-dark m-portlet--full-height ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Jumlah Penyaluran
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="m-widget7 m-widget7--skin-dark">
                        <h4 class="m-widget27__title m--font-light text-right">
                        <span><?php
                                        foreach($jlh_byk->result() as $row):
                                            echo number_format($row->jlh_penyaluran, 2).' '.$row->satuan,'<br />'; 
                                        endforeach;
                                    ?></span>
                        </h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-3">
            <div class="m-portlet m--bg-danger m-portlet--bordered-semi m-portlet--skin-dark m-portlet--full-height ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Sisa Stock
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="m-widget7 m-widget7--skin-dark">
                        <h4 class="m-widget27__title m--font-light text-right">
                        <span><?php
                                        foreach($jlh_byk->result() as $row):
                                            echo number_format($row->jlh_penyaluran - ($row->jlh_stock - $row->jlh_pengadaan), 2).' '.$row->satuan,'<br />'; 
                                        endforeach;
                                    ?></span>
                        </h4>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

