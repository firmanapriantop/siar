<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon m--hide">
                <i class="la la-gear"></i>
            </span>
            <h3 class="m-portlet__head-text">
                Sebaran Jumlah Usulan
            </h3>
        </div>
    </div>
</div>
<div class="m-portlet__body">
    <div id="myline" style="height:350px;">
    </div>
</div>

<script src="<?= base_url()?>/theme/vendors/jquery/dist/jquery.js" type="text/javascript"></script>

<script>
    $(document).ready(function(){

        var data = [
			<?php foreach($hasil_line->result() as $row): ?>
				{ x: '<?= $row->bulan2 ?>', y: <?= $row->jlh ?>},
			<?php endforeach; ?>
		];


		new Morris.Line({
            element: 'myline',
            data: data,
            xkey: ['x'],
            ykeys: ['y'],
            labels: ['Jumlah Usulan'],
            parseTime : false
		});

	});
</script> 