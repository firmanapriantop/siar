
<div class="col-xl-6">
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Perbandingan Jumlah Formulasi dengan Formulasi Terlapor
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div id="myfirstchart" style="height:350px;">
            </div>
        </div>
    </div>
</div>

<div class="col-xl-6">
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Perbandingan Jumlah Perusahaan dengan Perusahaan Terlapor
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div id="perusahaan" style="height:350px;">
            </div>
        </div>
    </div>
</div>



<script src="<?= base_url()?>/theme/vendors/jquery/dist/jquery.js" type="text/javascript"></script>
<script src="<?= base_url()?>/assets/muds.js" type="text/javascript"></script>
<script>
    $(document).ready(function(){
		
		var data = [
			{
                label: 'Jlh Formulasi',
                value: <?= $jlh_formulasi ?>,
				labelColor:"blue"
				 
            },
            {
                label: 'Jlh Formulasi Terlapor',
                value: <?= $jlh_formulasi_terlapor ?>,
				labelColor:"green"
            }
		];

        var data2 = [
			{
                label: 'Jlh Perusahaan',
                value: <?= $jlh_perusahaan ?>
            },
            {
                label: 'Jlh Perusahaan Terlapor',
                value: <?= $jlh_perusahaan_terlapor ?>
            }
		];
		
		new Morris.Donut({
			element: 'myfirstchart',
			data: data,
			labelColor: '#9CC4E4	',
			  colors: [
				'#0BA462',
				'rgb(0, 188, 212)', 'rgb(255, 152, 0)', 'rgb(0, 150, 136)'
			  ]
		});

        new Morris.Donut({
			element: 'perusahaan',
			data: data2,
			labelColor: "#9CC4E4",
            colors: ['#E53935', 'rgb(0, 188, 212)', 'rgb(255, 152, 0)', 'rgb(0, 150, 136)'] 	
		});

	});
</script> 