<!DOCTYPE html>
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
	<meta charset="utf-8" />
	<title>SIAR</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<meta content="" name="Sistem Informasi Koperasi Simpan Pinjam" />
	<meta content="" name="develop by MUDS.COM >> @firmanapriantop @renhardsitorus @pandisianturi" />
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
	<?php echo $CSS_JS; ?>
	<link rel="shortcut icon" href="<?= base_url() ?>/assets/img/favicon/favicon.ico" />
</head>

<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->

<body class="page-header-fixed page-quick-sidebar-over-content ">

	<!-- BEGIN HEADER -->
	<div class="page-header -i navbar navbar-fixed-top">

		<!-- BEGIN HEADER INNER -->
		<div class="page-header-inner">

			<!-- BEGIN LOGO -->
			<div class="page-logo">
				<a href="<?php echo base_url(); ?>">
					<img src="<?php echo base_url(); ?>/assets/admin/layout/img/logo.png" alt="logo" class="logo-default" />
				</a>
				<!--
				<a href="" class="h5 text-white">
					<span class="h3 font-w600 text-bold">KRISNA</span>
				</a>
				-->
				<div class="menu-toggler sidebar-toggler hide">
					<!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
				</div>
			</div>
			<!-- END LOGO -->

			<!-- BEGIN RESPONSIVE MENU TOGGLER -->
			<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
			</a>
			<!-- END RESPONSIVE MENU TOGGLER -->

			<!-- BEGIN TOP NAVIGATION MENU -->
			<?php $this->view('layout/top-menu'); ?>
			<!-- END TOP NAVIGATION MENU -->

		</div>
		<!-- END HEADER INNER -->

	</div>
	<!-- END HEADER -->

	<div class="clearfix">
	</div>

	<!-- BEGIN CONTAINER -->
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
		<?php $this->view('layout/sidebar'); ?>
		<!-- END SIDEBAR -->

		<!-- BEGIN CONTENT -->
		<?php $this->load->view($content); ?>
		<!-- END CONTENT -->

		<!-- BEGIN QUICK SIDEBAR -->
		<?php //$this->view('layout/quick-sidebar'); 
		?>
		<!-- END QUICK SIDEBAR -->
	</div>
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	<div class="page-footer">
		<div class="page-footer-inner">
			2018 &copy; MUDSCODE.COM.
		</div>
		<div class="scroll-to-top">
			<i class="icon-arrow-up"></i>
		</div>
	</div>
	<!-- END FOOTER -->

	<script>
		jQuery(document).ready(function() {
			// initiate layout and plugins
			Metronic.init(); // init metronic core components
			Layout.init(); // init current layout
			QuickSidebar.init(); // init quick sidebar
			Demo.init(); // init demo features
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->

</html>