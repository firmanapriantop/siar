<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
	<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
	<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->

	<!--- QUERY VIEW -->
	<?php
		$role_id = $this->session->userdata('role_id');
		
	$snav = get_left_navigation($role_id);
	
	//print_r($snav);
	?>  

	<div class="page-sidebar navbar-collapse collapse">
		<ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
			
			


			<li class="sidebar-toggler-wrapper">
				<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				<div class="sidebar-toggler">
				</div>
				<!-- END SIDEBAR TOGGLER BUTTON -->
			</li>
			

			<li class="sidebar-search-wrapper">
				&nbsp;
			</li>
			
			 <?php 
			// $no=1;
				foreach( $snav as $row ){ 
				
					if( empty($row->sub) )
					{// echo 'single'; 
						if ($row->sitemapid_parent=='0') {
			?>
				<li id="<?= $row->sitemapid ?>">
					<a href="<?= base_url() . $row->url ?>" id="<?= $row->name ?> ">
						<i class="<?= $row->icon ?>"></i>
						<span class="title"><?= $row->displayname ?></span>
					</a>
				</li>
			<?php
						}
					}else{
						//echo'doble';
			?>
					<li>
						<a href="javascript:;">
						<i class="icon-basket"></i>
						<span class="title"><?= $row->displayname ?></span>
						<span class="arrow "></span>
						</a>
						<ul class="sub-menu">
							 <?php foreach( $row->sub as $r ){ ?>
								<li id="<?= $r->sitemapid ?>">
									<a href="<?= base_url() . $r->url ?>" id="<?= $r->name ?> ">
									<i class="<?= $r->icon ?>"></i>
									<?= $r->displayname ?></a>
								</li>
							<?php } ?>
						</ul>
					</li>
			<?php
					}
					//echo $no=$no+1;
				}
			?>
			
			
		</ul>
		<!-- END SIDEBAR MENU -->
	</div>
</div>
<!-- END SIDEBAR -->