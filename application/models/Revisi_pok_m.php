<?php


class Revisi_pok_m extends CI_Model
{

    //put your code here
    var $tables = "dokumen_koordinasi";
    var $view = "dokumen_koordinasi_vw";
    var $primary_key = "id";

    public function set_data($id = 0, $data)
    {
        if ($id == 0) {
            return $this->db->insert($this->tables, $data);
        } else {
            $this->db->where($this->primary_key, $id);
            return $this->db->update($this->tables, $data);
        }
    }

    public function get_by_id($id)
    {
        $this->db->where($this->primary_key, $id);
        $query = $this->db->get($this->view);
        return $query->row();
    }

    public function delete_data($id)
    {
        $this->db->where($this->primary_key, $id);
        return $this->db->delete($this->tables);
    }

    public function get_all_formulasi()
    {
        $this->db->order_by("nama", "asc");
        $query = $this->db->get('rt_formulasi_vw');
        return $query->result_array();
    }
}
