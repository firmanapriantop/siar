<?php


class Perusahaan_m extends CI_Model {

    //put your code here
    var $tables = "ms_perusahaan";
    var $view = "ms_perusahaan_vw";
    var $primary_key = "perusahaanid";
	
	public function set_data($id = 0,$data)
    {
        if ($id == 0) {
            return $this->db->insert($this->tables, $data);
        } else {
            $this->db->where($this->primary_key, $id);
            return $this->db->update($this->tables, $data);
        }
    }
	
	public function get_by_id($id)
    {
			$this->db->where($this->primary_key, $id);
            $query = $this->db->get($this->view);
            return $query->row();
    }
	
	public function delete_data($id)
    {
        $this->db->where($this->primary_key, $id);
        return $this->db->delete($this->tables);
    }
	
	public function get_pendidikan()
	{
		$query = $this->db->get('sys_pendidikan');
		return $query->result_array();
	}
	
	public function get_marital()
	{
		$query = $this->db->get('sys_marital');
		return $query->result_array();
	}
}