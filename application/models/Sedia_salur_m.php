<?php


class Sedia_salur_m extends CI_Model {

    //put your code here
    var $tables = "tx_sedia_salur_pestisida";
    var $view = "sedia_salur_vw";
    var $primary_key = "sediasalurid";
	
	public function set_data($id = 0,$data)
    {
        if ($id == 0) {
            return $this->db->insert($this->tables, $data);
        } else {
            $this->db->where($this->primary_key, $id);
            return $this->db->update($this->tables, $data);
        }
    }
	
	public function get_by_id($id)
    {
			$this->db->where($this->primary_key, $id);
            $query = $this->db->get($this->view);
            return $query->row();
    }
	
	public function delete_data($id)
    {
        $this->db->where($this->primary_key, $id);
        return $this->db->delete($this->tables);
    }
	
	public function get_all_formulasi($id)
	{
		if ($this->session->userdata('role_id')=='3'){
				$perusahaan=$this->session->userdata('perusahaanid');
				 $this->db->where("perusahaanid", $perusahaan);
		}
		
		
		 if ($id <>"") {
            $this->db->where("perijinanid", $id);
        }else {
			$this->db->where('expired_year >', 'year(CURDATE())-2' );
		}
		$this->db->order_by("nama", "asc");
		$query = $this->db->get('tx_perijinan_vw');
		return $query->result_array();
	}
}