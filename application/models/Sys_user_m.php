<?php


class Sys_user_m extends CI_Model {

    //put your code here
    var $tables = "sys_user";
    var $view = "sys_user_vw";
    var $primary_key = "userid";
	
	public function set_data($id = 0,$data)
    {
        if ($id == 0) {
            return $this->db->insert($this->tables, $data);
        } else {
            $this->db->where($this->primary_key, $id);
            return $this->db->update($this->tables, $data);
        }
    }
	
	public function get_by_id($id)
    {
			$this->db->where($this->primary_key, $id);
            $query = $this->db->get($this->view);
            return $query->row();
    }
	
	public function delete_data($id)
    {
        $this->db->where($this->primary_key, $id);
        return $this->db->delete($this->tables);
    }
	
}