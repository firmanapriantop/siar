<?php


class Formulasi_m extends CI_Model {

    //put your code here
    var $tables = "rt_formulasi";
    var $view = "rt_formulasi_vw";
    var $primary_key = "formulasiid";
	
	public function set_data($id = 0,$data)
    {
        if ($id == 0) {
            return $this->db->insert($this->tables, $data);
        } else {
            $this->db->where($this->primary_key, $id);
            return $this->db->update($this->tables, $data);
        }
    }
	
	public function get_by_id($id)
    {
			$this->db->where($this->primary_key, $id);
            $query = $this->db->get($this->view);
            return $query->row();
    }
	
	public function delete_data($id)
    {
        $this->db->where($this->primary_key, $id);
        return $this->db->delete($this->tables);
    }
	
	public function get_all_jenis()
	{
		$this->db->order_by("jenis", "asc");
		$query = $this->db->get('ms_jenis_formula');
		return $query->result_array();
	}
	public function get_all_penggunaan()
	{
		$this->db->order_by("bidang", "asc");
		$query = $this->db->get('ms_bidang');
		return $query->result_array();
	}
	
	public function get_all_perusahaan()
	{
		$this->db->order_by("nama", "asc");
		$query = $this->db->get('ms_perusahaan');
		return $query->result_array();
	}
}