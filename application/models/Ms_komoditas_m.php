<?php


class Ms_komoditas_m extends CI_Model {

    //put your code here
    var $tables = "ms_komoditas";
    var $view = "";
    var $primary_key = "komoditasid";
	
	public function set_data($id = 0,$data)
    {
        if ($id == 0) {
            return $this->db->insert($this->tables, $data);
        } else {
            $this->db->where($this->primary_key, $id);
            return $this->db->update($this->tables, $data);
        }
    }
	
	public function get_all_data()
		{
			$query = $this->db->get($this->tables);
			return $query->result_array();
		}
	
	public function get_by_id($id)
    {
			$this->db->where($this->primary_key, $id);
            $query = $this->db->get($this->tables);
            return $query->row();
    }
	
	public function delete_data($id)
    {
        $this->db->where($this->primary_key, $id);
        return $this->db->delete($this->tables);
    }
	
}