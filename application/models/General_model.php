<?php

class General_model extends CI_Model
{

	function get_data($query)
	{
		return $this->db->query($query);
	}

	function get_datax($query)
	{
		$q = $this->db->query($query);
		return $q->result();
	}

	function get_dataz($query)
	{
		$q = $this->db->query($query);
		return $q->result_array();
	}

	public function get_all_perusahaan()
	{
		$query = $this->db->get('ms_perusahaan_vw');
		return $query->result_array();
	}

	public function get_all_prov()
	{
		$query = $this->db->get('ms_wil_provinsi');
		return $query->result_array();
	}

	public function get_all_kab_by_provId($id = 0)
	{
		if ($id <> 0)
			$this->db->where('provid', $id);

		$query = $this->db->get('ms_wil_kabupaten');

		return $query->result_array();
	}

	public function get_all_kec_by_kabId($id = 0)
	{
		if ($id <> 0)
			$this->db->where('kabid', $id);

		$query = $this->db->get('ms_wil_kecamatan');

		return $query->result_array();
	}

	public function get_all_desa_by_kecId($id = 0)
	{
		if ($id <> 0)
			$this->db->where('kecid', $id);

		$query = $this->db->get('ms_wil_desa');

		return $query->result_array();
	}

	public function get_all_debitur_by_desaId($id = 0)
	{
		if ($id <> 0)
			$this->db->where('desaid', $id);

		$query = $this->db->get('ms_debitur_vw');

		return $query->result_array();
	}

	public function get_all_role()
	{

		$query = $this->db->get('sys_role');

		return $query->result_array();
	}

	public function save_data($nama_table, $primary_key, $id = 0, $data)
	{
		if ($id == 0) {
			return $this->db->insert($nama_table, $data);
		} else {
			$this->db->where($primary_key, $id);
			return $this->db->update($nama_table, $data);
		}
	}

	public function save_log($nama_table, $aksi, $data)
	{
		$data['aksi'] = $aksi;
		return $this->db->insert($nama_table, $data);
	}

	public function save_tmp($nama_table, $revisi, $data)
	{
		//$data['revisi_ke'] = $revisi;
		$revisiku = $revisi;
		return $this->db->insert($nama_table, $data);
	}


	public function hapus_data($nama_table, $nama_field, $id)
	{
		$this->db->where($nama_field, $id);
		return $this->db->delete($nama_table);
	}
	
	function get_data_by_id($query)
	{
		$q = $this->db->query($query);
		return $q->row();
	}
}
