<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	function asset_url($param)
	{
		if ($param == 'dashboard' || $param == '' || $param='user'){

			$css_global = array(
				'global/plugins/font-awesome/css/font-awesome.min.css',
				'global/plugins/simple-line-icons/simple-line-icons.min.css',
				'global/plugins/bootstrap/css/bootstrap.min.css',
				'global/plugins/uniform/css/uniform.default.css',
				'global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
				'global/css/components.css',
				'global/css/plugins.css',
				'admin/layout/css/layout.css',
				'admin/layout/css/themes/darkblue.css',
				'admin/layout/css/custom.css',
				'vendors/custom/datatables/datatables.bundle.css'
			);
	
			$js_global = array(
				'global/plugins/jquery.min.js',
				'global/plugins/jquery-migrate.min.js',
				'global/plugins/jquery-ui/jquery-ui.min.js',
				'global/plugins/bootstrap/js/bootstrap.min.js',
				'global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
				'global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
				'global/plugins/jquery.blockui.min.js',
				'global/plugins/jquery.cokie.min.js',
				'global/plugins/uniform/jquery.uniform.min.js',
				'global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
				'global/scripts/metronic.js',
				'admin/layout/scripts/layout.js',
				'admin/layout/scripts/quick-sidebar.js',
				'admin/layout/scripts/demo.js',
				/* Datatables */
				'plugins/tables/datatables/datatables.min.js',
				'plugins/tables/datatables/extensions/responsive.min.js',
				'plugins/forms/selects/select2.min.js',
				'plugins/search-options/column-search.js'
				/* /Datatables */
			);
			
		
		} elseif ($param == 'login'){

			$css_global = array(
				'icons/icomoon/styles.css',
				'bootstrap.css',
				'core.css',
				'components.css',
				'colors.css'
			);
			$js_global = array(
				/* Core JS files */
				//'plugins/loaders/pace.min.js',
				'core/libraries/jquery.min.js',
				'core/libraries/bootstrap.min.js',
				'plugins/loaders/blockui.min.js',
				/* /Core JS files */
				/* Footable */
				'plugins/tables/footable/footable.min.js',
				
				/* Datatables */
				'plugins/tables/datatables/datatables.min.js',
				'plugins/tables/datatables/extensions/responsive.min.js',
				'plugins/forms/selects/select2.min.js',
				'plugins/search-options/column-search.js',
				/* /Datatables */
				
				/* Theme JS files */
				'core/app.js',
				/* /Theme JS files */

				/* Form Validation */
				'plugins/forms/validation/validate.min.js',
				/* PNotify */
				'plugins/notifications/pnotify.min.js',
				/* /PNotify */
				//'plugins/forms/styling/uniform.min.js',
				//'plugins/forms/styling/switchery.min.js',
				//'plugins/forms/styling/switch.min.js',

				/* Sweet Alert */
				'plugins/swal/sweetalert.min.js',
				/* /Sweet Alert */
				'plugins/ui/moment/moment.min.js',
				'plugins/pickers/daterangepicker.js',
				//'plugins/tables/footable/footable.min.js',
				'plugins/media/fancybox.min.js',

				/* Theme JS files  */
				'plugins/forms/styling/uniform.min.js',
				'plugins/forms/styling/switchery.min.js',
				'plugins/forms/styling/switch.min.js'
				
			);
		}

		$return = '';
		
		foreach($css_global as $filecss) { $return .='<link href="'.base_url().'assets/'.$filecss.'" rel="stylesheet" type="text/css" />'; }
		foreach($js_global as $filejs) { $return .='<script type="text/javascript" src="'.base_url().'assets/'.$filejs.'"></script>'; }
			
		return $return;
	}
?>