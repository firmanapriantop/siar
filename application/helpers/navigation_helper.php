<?php

	function get_left_navigation($roleid = NULL) {
		$ci = & get_instance();
		$ci->load->model('Sys_privilege_m');
		$where = array('roleid' => $roleid);
		$order = 'sortno';
		$nav = $ci->Sys_privilege_m->get_data($where, $order);

		foreach ($nav as $row) {
			$sitemapid = $row->sitemapid;
			$row->sub = $ci->Sys_privilege_m->get_data(array('sitemapid_parent' => $sitemapid, 'roleid' => $roleid), 'sortno');
		}
			//print_r($nav).'test';
			//exit();
		return $nav;
	}