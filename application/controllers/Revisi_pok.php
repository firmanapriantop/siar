<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Revisi_pok extends CI_Controller
{

    var $tmp_path = 'templates/index';
    var $main_path = 'pages/revisi_pok/';
    var $role = '';
    var $subkomponen_ids = '';
    var $state = '';
    var $is_admin = false;

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('masuk') != TRUE) {

            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">You need login first.</div>');

            redirect('auth/index');
        }

        $this->load->model(array('general_model' => 'gm', 'dokumen_koordinasi_m' => 'dkm'));

        //cek role
        $userid = $this->session->userdata('userid');
        $q = "
            select 
                r.user_id as user_id,
                r.role as role,
                u.subkomponen_ids as subkomponen_ids
            from role r
            left join unit_kerja u on u.id = r.unit_kerja_id
            left join sys_user s on s.userid = r.user_id 
            where r.user_id = '$userid'
        ";
        $hasil = $this->gm->get_data($q)->row();
        $this->role = $hasil->role;
        $this->subkomponen_ids = $hasil->subkomponen_ids;

        $y = $this->gm->get_data("select * from state order by id desc")->row();

        $this->state = $y->nama;
        $this->is_admin = $this->role === 'admin' || $this->session->userdata('userid') === '1';
    }

    public function index()
    {

        $data['title'] = 'SIAR / Revisi POK';
        $data["isActive"]   = 'Revisi POK';
        $data['page'] = $this->main_path . 'index';

        $query = "select * from rt_rka order by tahun desc";

        $data['rka'] = $this->gm->get_data($query);

        $query = "select * from rm_program";

        $data['program'] = $this->gm->get_data($query);

        $query = "select idgiat, kdgiat, nmgiat from rm_giat
        order by kdgiat";

        $data['kegiatan'] = $this->gm->get_data($query);

        //$data['state'] = $this->gm->get_data("select * from state order by id desc limit 1")->row(1);
        $data['state'] = $this->state;
        $data['role'] = $this->role;
        $data['subkomponen_ids'] = $this->subkomponen_ids;

        $data['is_admin'] = $this->is_admin;

        $this->load->view($this->tmp_path, $data);
    }

    function ubah_state()
    {
        $siapa = $this->input->post('siapa');
        $nilai = $this->input->post('nilai');
        $aksi = $this->input->post('aksi');

        $validasi = array('success' => false, 'messages' => array());

        //cari dulu apakah masih ada yg pending

        $cek_sk = $this->gm->get_data("Select count(*) as jlh from rt_subkomponen where status in ('pending-add', 'pending-edit', 'pending-remove')")->row();
        //cek lagi rt_item apakah masih ada yg status pending
        $cek_i = $this->gm->get_data("Select count(*) as jlh from rt_item where status in ('pending-add', 'pending-edit', 'pending-remove')")->row();

        if ($aksi == 'submit' && $siapa == 'direktur') {
            if ($cek_sk->jlh == 0 && $cek_i->jlh == 0) {
                $data['nama'] = "locked";
                $data['submit_direktur'] = $nilai;
                $data['userinput']  = $this->session->userdata('userid');
                $data['dateinput']    = date('Y-m-d H:i:s');

                $hasil = $this->gm->save_data('state', 'id', 0, $data);
                echo '1';
            } else {
                echo '0';
            }
        } elseif ($aksi == 'submit' && $siapa == 'bagcan') {
            $data['nama'] = "closed";
            $data['submit_bagcan'] = $nilai;
            $data['userinput']  = $this->session->userdata('userid');
            $data['dateinput']    = date('Y-m-d H:i:s');

            $hasil = $this->gm->save_data('state', 'id', 0, $data);

            //ubah revisi ke
            $p = $this->gm->get_data("select * from rt_subkomponen_tmp where revisi_ke = ''")->result();
            foreach ($p as $q) {

                $this->update_tmp($q->idsubkomponen);
            }

            echo '1';
        } elseif ($aksi == 'unsubmit') {
            $data['nama'] = "open";
            $data['submit_direktur'] = $nilai;
            $data['submit_bagcan'] = $nilai;
            $data['userinput']  = $this->session->userdata('userid');
            $data['dateinput']    = date('Y-m-d H:i:s');

            $hasil = $this->gm->save_data('state', 'id', 0, $data);

            //unsubmit sub_komponen
            $userid = $this->session->userdata('userid');
            $updatedate = date('Y-m-d H:i:s');
            $q = "
                update rt_subkomponen 
                set submit = 0, userupdate = '$userid', dateupdate = '$updatedate'
            ";

            $this->gm->get_data($q);

            echo '1';
        }
    }

    public function update_tmp($idsubkomponen)
    {

        //cek apakah revisi_ke masih ''
        $hasil2 = $this->gm->get_data("select count(*) as jlh from rt_subkomponen_tmp where idsubkomponen = '$idsubkomponen' and revisi_ke = ''")->row();
        $ha = $hasil2->jlh;

        $jumlah = 0;
        if ($ha == 0) {
        } elseif ($ha == 1) {
            $jumlah = $ha;

            $this->gm->get_data("update rt_subkomponen_tmp set revisi_ke = '$jumlah' where revisi_ke = '' and idsubkomponen = '$idsubkomponen'");


            //save item tmp
            $p = $this->gm->get_data("select * from rt_item where idsubkomponen = '$idsubkomponen'")->result();
            foreach ($p as $q) {

                $this->gm->get_data("update rt_item_tmp set revisi_ke = '$jumlah' where revisi_ke = '' and iditem = '$q->iditem'");
            }
        } elseif ($ha > 1) {
            //cek lagi sudah berapa kali revisi
            $hasil3 = $this->gm->get_data("select count(*) as jlh from rt_subkomponen_tmp where idsubkomponen = '$idsubkomponen'")->row();
            $ha3 = $hasil3->jlh;

            $jumlah = $ha3 + 1;

            $this->gm->get_data("update rt_subkomponen_tmp set revisi_ke = '$jumlah' where revisi_ke = '' and idsubkomponen = '$idsubkomponen'");


            //save item tmp
            $p = $this->gm->get_data("select * from rt_item where idsubkomponen = '$idsubkomponen'")->result();
            foreach ($p as $q) {

                $this->gm->get_data("update rt_item_tmp set revisi_ke = '$jumlah' where revisi_ke = '' and iditem = '$q->iditem'");
            }
        }
    }

    function load_output()
    {
        $idgiat = $this->input->post('idgiat');
        //$query = "select idoutput, kdoutput, nmoutput from rm_output
        //          where idgiat = '$idgiat'
        //        order by kdoutput";

        $query = "
                    select 
                        DISTINCT(idoutput) as idoutput,
                        CONCAT(kdgiat,'.',kdoutput) as kdoutput, 
                        nmoutput as nmoutput 
                    from vw_rka_trans 
                    where tahun='2020' and idgiat='$idgiat'
                    order by kdoutput, nmoutput";

        $data = $this->gm->get_dataz($query);
        echo json_encode($data);
    }

    function load_sub_output()
    {
        $idoutput = $this->input->post('idoutput');

        /*$query = "select 
                        idsuboutput, idoutput, kdsuboutput, ursuboutput as nmsuboutput 
                    from rt_suboutput
                    where idoutput = '$idoutput'
                    order by kdsuboutput";
        */
        $query = "
                    select 
                        DISTINCT(idsuboutput) as idsuboutput,
                        CONCAT(kdgiat,'.',kdoutput,'.',
                        kdsuboutput) as kdsuboutput,
                        ursuboutput as nmsuboutput 
                    from vw_rka_trans 
                    where tahun='2020' and idoutput='$idoutput'
                    order by kdsuboutput, nmsuboutput";

        $data = $this->gm->get_dataz($query);

        echo json_encode($data);
    }

    function load_komponen()
    {
        $idsuboutput = $this->input->post('idsuboutput');
        /*$query = "select idkomponen, kdkomponen, urkomponen as nmkomponen from rt_komponen
                    where idsuboutput = '$idsuboutput'
                    order by kdkomponen";
        */
        $query = "
                    select 
                        DISTINCT(idkomponen) as idkomponen,
                        CONCAT(kdgiat,'.',kdoutput,'.',kdsuboutput,'.',kdkomponen) as kdkomponen,
                        urkomponen as nmkomponen 
                    from vw_rka_trans 
                    where tahun='2020' and idsuboutput='$idsuboutput'
                    order by kdkomponen, nmkomponen";

        $data = $this->gm->get_dataz($query);
        echo json_encode($data);
    }

    function load_sub_komponen()
    {
        //cek role 

        $idkomponen = $this->uri->segment(3);
        if (is_null($this->subkomponen_ids)) {
            $where = " != ''";
        } else {
            $where = " in ($this->subkomponen_ids) ";
        }
        $query =
            "select 
                s.idsubkomponen as id,
                s.kdsubkomponen as kode,
                s.ursubkomponen as nama,
                (select sum(jumlah) from rt_item where idsubkomponen = s.idsubkomponen) as nilai,
                s.status,
                k.kdkomponen as kdkomponen,
                so.kdsuboutput as kdsuboutput,
                o.kdoutput as kdoutput,
                ib.kdib as kdib,
                keg.kdgiat as kdgiat,
                s.status,
                s.submit as submit,
                s.approval_kasubdit,
                s.cat_approval_kasubdit,
                s.approval_pokjacan,
                s.cat_approval_pokjacan
            from    rt_subkomponen as s 
            left join rt_komponen k on k.idkomponen = s.idkomponen
            left join rt_suboutput so on so.idsuboutput = k.idsuboutput
            left join rm_output o on o.idoutput = so.idoutput
            left join rm_ib ib on ib.idib = so.idib
            left join rm_giat keg on keg.idgiat = o.idgiat
            where s.idkomponen = '$idkomponen' and s.idsubkomponen $where
            order by  s.kdsubkomponen, s.ursubkomponen
            ";

        // var_dump($query);

        $data['data']    = $this->gm->get_data($query);
        $data['kdrka'] =  $this->uri->segment(4);
        $data['kdprogram'] = $this->uri->segment(5);
        $data['kdprogram'] = $this->uri->segment(5);
        $data['idkomponen'] = $idkomponen;
        $data['state'] = $this->gm->get_data("select * from state order by id desc limit 1")->row(1);

        $data['role'] = $this->role;
        $data['state'] = $this->state;
        $data['subkomponen_ids'] = $this->subkomponen_ids;

        $this->load->view($this->main_path . 'list', $data);
    }

    //sub komponen
    function add_subkomponen()
    {
        /*
        $id             = $this->uri->segment(3);
        $data['data']   = $this->gm->get_data("SELECT * FROM rt_subkomponen WHERE idsubkomponen = '$id'")->row();
*/
        $this->load->view('pages/revisi_pok/add_subkomponen');
    }

    function edit_subkomponen()
    {
        $id             = $this->uri->segment(3);
        $data['data']   = $this->gm->get_data("SELECT * FROM rt_subkomponen WHERE idsubkomponen = '$id'")->row();
        $data['idsubkomponen'] = $id;
        $this->load->view('pages/revisi_pok/edit_subkomponen', $data);
    }

    public function save_log($jenis, $aksi, $id)
    {
        //var_dump($jenis);

        if ($jenis == 'sub_komponen') {
            $t = $this->gm->get_data("select * from rt_subkomponen where idsubkomponen = '$id'")->row();

            $datax['aksi'] = $aksi;
            $datax['idsubkomponen'] = $t->idsubkomponen;
            $datax['idkomponen'] = $t->idkomponen;
            $datax['kdsubkomponen'] = $t->kdsubkomponen;
            $datax['ursubkomponen'] = $t->ursubkomponen;
            $datax['dateinput'] = $t->dateinput;
            $datax['userinput'] = $t->userinput;
            $datax['userupdate']  =  $t->userupdate;
            $datax['dateupdate']    = $t->dateupdate;
            $datax['status']    = $t->status;
            $datax['submit']    = $t->submit;
            $datax['approval_kasubdit'] = $t->approval_kasubdit;
            $datax['cat_approval_kasubdit'] = $t->cat_approval_kasubdit;
            $datax['approval_pokjacan'] = $t->approval_pokjacan;
            $datax['cat_approval_pokjacan'] = $t->cat_approval_pokjacan;

            $result = $this->gm->save_data('rt_subkomponen_log', 'id', 0, $datax);
        } elseif ($jenis == 'item') {
            // var_dump($id);
            $t = $this->gm->get_data("select * from rt_item where iditem = '$id'")->row();

            $datax['aksi'] = $aksi;
            $datax['iditem'] = $t->iditem;
            $datax['idsubkomponen'] = $t->idsubkomponen;
            $datax['idakun'] = $t->idakun;
            $datax['noitem'] = $t->noitem;
            $datax['nmitem'] = $t->nmitem;
            $datax['volkeg'] = $t->volkeg;
            $datax['satkeg'] = $t->satkeg;
            $datax['hargasat'] = $t->hargasat;
            $datax['jumlah'] = $t->jumlah;
            $datax['dateinput'] = $t->dateinput;
            $datax['userinput'] = $t->userinput;
            $datax['userupdate']  =  $t->userupdate;
            $datax['dateupdate']    = $t->dateupdate;
            $datax['status']    = $t->status;
            $datax['approval_kasubdit'] = $t->approval_kasubdit;
            $datax['cat_approval_kasubdit'] = $t->cat_approval_kasubdit;
            $datax['approval_pokjacan'] = $t->approval_pokjacan;
            $datax['cat_approval_pokjacan'] = $t->cat_approval_pokjacan;

            $result = $this->gm->save_data('rt_item_log', 'id', 0, $datax);
        }
    }

    public function save_tmp($idsubkomponen)
    {
        //cek dulu ada gak tmp di
        $hasil = $this->gm->get_data("select count(*) as jlh, revisi_ke from rt_subkomponen_tmp where idsubkomponen = '$idsubkomponen' and revisi_ke <> '' and (status <> 'ready' or status = '')")->row();
        $revisi_ke = $hasil->revisi_ke;
        $jlh = $hasil->jlh;

        if ($revisi_ke == '') {

            if ($jlh == 0) {
                $revisi_ke = '';
            } else {
                $revisi_ke = $jlh + 1;
            }

            //cek apakah revisi_ke masih ''
            $hasil2 = $this->gm->get_data("select count(*) as jlh from rt_subkomponen_tmp where idsubkomponen = '$idsubkomponen' and revisi_ke = ''")->row();
            $ha = $hasil2->jlh;

            if ($ha == 1) {
            } else {
                $t = $this->gm->get_data("select * from rt_subkomponen where idsubkomponen = '$idsubkomponen'")->row();

                $datax['revisi_ke'] = $revisi_ke;
                $datax['idsubkomponen'] = $t->idsubkomponen;
                $datax['idkomponen'] = $t->idkomponen;
                $datax['kdsubkomponen'] = $t->kdsubkomponen;
                $datax['ursubkomponen'] = $t->ursubkomponen;
                $datax['dateinput'] = $t->dateinput;
                $datax['userinput'] = $t->userinput;
                $datax['userupdate']  =  $t->userupdate;
                $datax['dateupdate']    = $t->dateupdate;
                $datax['status']    = $t->status;
                $datax['submit']    = $t->submit;
                $datax['approval_kasubdit'] = $t->approval_kasubdit;
                $datax['cat_approval_kasubdit'] = $t->cat_approval_kasubdit;
                $datax['approval_pokjacan'] = $t->approval_pokjacan;
                $datax['cat_approval_pokjacan'] = $t->cat_approval_pokjacan;

                $this->gm->save_tmp('rt_subkomponen_tmp', $revisi_ke, $datax);

                //save item tmp
                $p = $this->gm->get_data("select * from rt_item where idsubkomponen = '$idsubkomponen'")->result();
                foreach ($p as $q) {
                    $z['revisi_ke'] = $revisi_ke;
                    $z['iditem'] = $q->iditem;
                    $z['idsubkomponen'] = $q->idsubkomponen;
                    $z['idakun'] = $q->idakun;
                    $z['noitem'] = $q->noitem;
                    $z['nmitem'] = $q->nmitem;
                    $z['volkeg'] = $q->volkeg;
                    $z['satkeg'] = $q->satkeg;
                    $z['hargasat'] = $q->hargasat;
                    $z['jumlah'] = $q->jumlah;
                    $z['is_no_related'] = $q->is_no_related;
                    $z['userinput'] = $q->userinput;
                    $z['userupdate']  =  $q->userupdate;
                    $z['dateupdate']    = $q->dateupdate;
                    $z['status']    = $q->status;
                    $z['item_id']    = $q->item_id;
                    $z['approval_kasubdit'] = $q->approval_kasubdit;
                    $z['cat_approval_kasubdit'] = $q->cat_approval_kasubdit;
                    $z['approval_pokjacan'] = $q->approval_pokjacan;
                    $z['cat_approval_pokjacan'] = $q->cat_approval_pokjacan;

                    $this->gm->save_tmp('rt_item_tmp', $revisi_ke, $z);
                }
            }
            //cek dulu apakah ada row


        } else {
            $revisi_ke = $revisi_ke + 1;
        }
    }

    function save_subkomponen()
    {
        $aksi = $this->uri->segment(3); //add or edit

        $validasi = array('success' => false, 'messages' => array());

        $this->form_validation->set_rules("kdsubkomponen", "Kode Sub Komponen", "trim|required");
        $this->form_validation->set_rules("ursubkomponen", "Nama", "trim|required");

        if ($this->form_validation->run() == false) {
            foreach ($_POST as $key => $value) {
                $validasi['messages'][$key] = form_error($key);
            }
        } else {

            if ($aksi == 'add') {

                $data['status'] = 'pending-add';
                $data['userinput']  = $this->session->userdata('userid');
                $data['dateinput']    = date('Y-m-d H:i:s');
                $id = 0;
                $aksi = 'add';
            } elseif ($aksi == 'edit') {
                //cek apakah sebelumnya di-add baru
                $status = $this->input->post('status');
                if ($status == 'pending-add') {
                    $data['status'] = 'pending-add';
                } else {
                    $data['status'] = 'pending-edit';
                }
                $id = $this->input->post('idsubkomponen');

                $aksi = 'edit';

                $this->save_tmp($id);
            }
            $idsubkomponen = $this->input->post('idsubkomponen');
            $data['idkomponen'] = $this->input->post('idkomponen');
            $data['kdsubkomponen'] = $this->input->post('kdsubkomponen');
            $data['ursubkomponen'] = $this->input->post('ursubkomponen');
            $data['approval_kasubdit'] = 0;
            $data['cat_approval_kasubdit'] = '';
            $data['approval_pokjacan'] = 0;
            $data['cat_approval_pokjacan'] = '';
            $data['userupdate']  = $this->session->userdata('userid');
            $data['dateupdate']    = date('Y-m-d H:i:s');
            // var_dump($data);
            //var_dump($aksi);
            $result = $this->gm->save_data('rt_subkomponen', 'idsubkomponen', $id, $data);

            if ($aksi == 'add') {
                //cek data subkomponen
                $w = $this->gm->get_data("select * from rt_subkomponen where status = 'pending-add' order by idsubkomponen desc")->row();
                $idsubkomponen = $w->idsubkomponen;
                $this->save_tmp($idsubkomponen);
            }

            $this->save_log('sub_komponen', $aksi, $idsubkomponen);

            //add log
            $validasi['success'] = true;
        }

        echo json_encode($validasi);
    }

    function hapus_sub_komponen()
    {
        $idsubkomponen = $this->input->post('id');
        $status = $this->input->post('status');

        if ($status == 'pending-add') {
            $this->gm->hapus_data("rt_subkomponen", "idsubkomponen", $idsubkomponen);

            $hsl = $this->db->query("select iditem from rt_item where idsubkomponen = '$idsubkomponen'");
            foreach ($hsl->result() as $i) {
                $this->gm->hapus_data("rt_item", "iditem", $i->iditem);
            }
            echo "done";
        } else {
            $data['status'] = 'pending-remove';
            $data['userupdate']  = $this->session->userdata('userid');
            $data['dateupdate']    = date('Y-m-d H:i:s');
            $this->gm->save_data('rt_subkomponen', 'idsubkomponen', $idsubkomponen, $data);

            $this->save_log('sub_komponen', "delete", $idsubkomponen);

            //cari items dgn idsubkomponen
            $hsl = $this->db->query("select iditem from rt_item where idsubkomponen = '$idsubkomponen'");
            foreach ($hsl->result() as $i) {
                $datax['status'] = 'pending-remove';
                $datax['userupdate']  = $this->session->userdata('userid');
                $datax['dateupdate']    = date('Y-m-d H:i:s');
                $this->gm->save_data('rt_item', 'iditem', $i->iditem, $datax);
                $this->save_log('item', "delete", $i->item);
            }
        }
    }

    function submit_sub_komponen()
    {
        $idsubkomponen = $this->input->post('id');

        $data['submit'] = $this->input->post('value');
        $data['userupdate']  = $this->session->userdata('userid');
        $data['dateupdate']    = date('Y-m-d H:i:s');

        $this->gm->save_data('rt_subkomponen', 'idsubkomponen', $idsubkomponen, $data);

        $this->save_log('sub_komponen', "submit", $idsubkomponen);
    }

    //--item
    function add_item()
    {
        $data['idsubkomponen'] = $this->uri->segment(3);
        $this->load->view('pages/revisi_pok/add_item', $data);
    }

    function load_akun()
    {
        $query  =   "select * from rm_akun
                    order by kdakun, nmakun";

        $data = $this->gm->get_dataz($query);
        echo json_encode($data);
    }

    function edit_item()
    {
        $id             = $this->uri->segment(3);
        $query = "
                select i.*, a.kdakun as kdakun, a.nmakun as nmakun
                from rt_item i
                left join rm_akun a on a.idakun = i.idakun
                where i.iditem ='$id'";
        $data['data']   = $this->gm->get_data($query)->row();

        $this->load->view('pages/revisi_pok/edit_item', $data);
    }

    function save_item()
    {
        $aksi = $this->uri->segment(3); //add or edit

        $validasi = array('success' => false, 'messages' => array());

        $this->form_validation->set_rules("noitem", "Kode item", "trim|required");
        $this->form_validation->set_rules("idakun", "Kode akun", "trim|required");
        $this->form_validation->set_rules("nmitem", "Nama item", "trim|required");
        $this->form_validation->set_rules("volkeg", "Volume kegiatan", "trim|required");
        $this->form_validation->set_rules("hargasat", "Harga Satuan", "trim|required");
        $this->form_validation->set_rules("jumlah", "Nilai", "trim|required");

        if ($this->form_validation->run() == false) {

            foreach ($_POST as $key => $value) {
                $validasi['messages'][$key] = form_error($key);
            }
        } else {

            if ($aksi == 'add') {

                $data['status'] = 'pending-add';
                $data['userinput']  = $this->session->userdata('userid');
                $data['dateinput']    = date('Y-m-d H:i:s');
                $id = 0;
            } elseif ($aksi == 'edit') {

                $id = $this->input->post('iditem');
                $data['iditem'] = $this->input->post('iditem');
                $data['status'] = 'pending-edit';
                $data['approval_kasubdit'] = 0;
                $data['cat_approval_kasubdit'] = '';
                $data['approval_pokjacan'] = 0;
                $data['cat_approval_pokjacan'] = '';

                $this->save_tmp($this->input->post('idsubkomponen'));
            }

            $data['idakun'] = $this->input->post('idakun');
            $data['idsubkomponen'] = $this->input->post('idsubkomponen');
            $data['noitem'] = $this->input->post('noitem');
            $data['nmitem'] = $this->input->post('nmitem');
            $data['volkeg'] = $this->input->post('volkeg');
            $data['satkeg'] = $this->input->post('satkeg');
            $data['hargasat'] = $this->input->post('hargasat');
            $data['jumlah'] = $this->input->post('jumlah');
            $data['userupdate']  = $this->session->userdata('userid');
            $data['dateupdate']    = date('Y-m-d H:i:s');
            //var_dump($data);

            $this->gm->save_data('rt_item', 'iditem', $id, $data);

            if ($aksi == 'add') {
                //cek data subkomponen
                $w = $this->gm->get_data("select * from rt_item where status = 'pending-add' order by iditem desc limit 1")->row();
                $id = $w->iditem;
                $this->save_tmp($this->input->post('idsubkomponen'));
            }

            //var_dump($id);
            //log
            $this->save_log('item', $aksi, $id);

            $validasi['success'] = true;
        }

        echo json_encode($validasi);
    }

    function hapus_item()
    {
        $iditem = $this->input->post('id');
        $status = $this->input->post('status');

        if ($status == 'pending-add') {
            $this->gm->hapus_data("rt_item", "iditem", $iditem);
            echo "done";
        } else {
            $data['status'] = 'pending-remove';
            $data['userupdate']  = $this->session->userdata('userid');
            $data['dateupdate']    = date('Y-m-d H:i:s');
            $this->gm->save_data('rt_item', 'iditem', $iditem, $data);
            $this->save_log('item', "delete", $iditem);
            echo "donex";
        }
    }


    //approval
    function approval_kasubdit()
    {
        $tipe = $this->uri->segment(3); //subkomponen or item
        $id = $this->uri->segment(4);

        //cek sudah ada approval 
        if ($tipe == "subkomponen") {
            $query = "
                    select 
                        idsubkomponen as id,
                        approval_kasubdit, 
                        case
                            when approval_kasubdit = 1 then 'Disetujui'
                            when approval_kasubdit = 2 then 'Ditolak'
                            else 'Belum ada persetujuan' 
                        end as aksi,
                        cat_approval_kasubdit
                    from rt_subkomponen where idsubkomponen ='$id'";
        } else {
            $query = "
                    select 
                        iditem as id,
                        approval_kasubdit, 
                        case
                            when approval_kasubdit = 1 then 'Disetujui'
                            when approval_kasubdit = 2 then 'Ditolak'
                            else 'Belum ada persetujuan' 
                        end as aksi,
                        cat_approval_kasubdit
                    from rt_item where iditem ='$id'";
        }


        $data['data'] = $this->gm->get_data($query)->row();
        $data['tipe'] = $tipe;

        $this->load->view('pages/revisi_pok/approval_kasubdit', $data);
    }

    function save_approval_kasubdit()
    {
        $tipe = $this->uri->segment(3); //subkomponen or item
        $id = $this->input->post('id');

        $validasi = array('success' => false, 'messages' => array());

        $this->form_validation->set_rules("approval_kasubdit", "Persetujuan", "trim|required");
        $this->form_validation->set_rules("cat_approval_kasubdit", "Catatan Persetujuan", "trim|required");

        if ($this->form_validation->run() == false) {

            foreach ($_POST as $key => $value) {
                $validasi['messages'][$key] = form_error($key);
            }
        } else {

            $data['approval_kasubdit'] = $this->input->post('approval_kasubdit');
            $data['cat_approval_kasubdit'] = $this->input->post('cat_approval_kasubdit');
            $data['userupdate']  = $this->session->userdata('userid');
            $data['dateupdate']    = date('Y-m-d H:i:s');

            if ($tipe == 'subkomponen') {
                $this->gm->save_data('rt_subkomponen', 'idsubkomponen', $id, $data);
                $this->save_log('sub_komponen', "approval-kasubdit", $id);
            } elseif ($tipe == 'item') {

                $this->gm->save_data('rt_item', 'iditem', $id, $data);
                $this->save_log('item', "approval-kasubdit", $id);
            }



            $validasi['success'] = true;
        }

        echo json_encode($validasi);
    }

    function approval_pokjacan()
    {
        $tipe = $this->uri->segment(3); //subkomponen or item
        $id = $this->uri->segment(4);

        //cek sudah ada approval 
        if ($tipe == 'subkomponen') {
            $query = "
            select 
                idsubkomponen as id,
                approval_kasubdit,
                approval_pokjacan, 
                case
                    when approval_pokjacan = 1 then 'Disetujui'
                    when approval_pokjacan = 2 then 'Ditolak'
                    else 'Belum ada persetujuan' 
                end as aksi,
                cat_approval_pokjacan,
                status
            from rt_subkomponen where idsubkomponen ='$id'";
        } else {
            $query = "
                    select 
                        iditem as id,
                        approval_kasubdit,
                        approval_pokjacan, 
                        case
                            when approval_pokjacan = 1 then 'Disetujui'
                            when approval_pokjacan = 2 then 'Ditolak'
                            else 'Belum ada persetujuan' 
                        end as aksi,
                        cat_approval_pokjacan,
                        status
                    from rt_item where iditem ='$id'";
        }


        $data['data'] = $this->gm->get_data($query)->row();
        $data['tipe'] = $tipe;

        $this->load->view('pages/revisi_pok/approval_pokjacan', $data);
    }

    function save_approval_pokjacan()
    {
        $tipe = $this->uri->segment(3); //subkomponen or item
        $id = $this->input->post('id');

        $validasi = array('success' => false, 'messages' => array());

        $this->form_validation->set_rules("approval_pokjacan", "Persetujuan", "trim|required");
        $this->form_validation->set_rules("cat_approval_pokjacan", "Catatan Persetujuan", "trim|required");

        if ($this->form_validation->run() == false) {

            foreach ($_POST as $key => $value) {
                $validasi['messages'][$key] = form_error($key);
            }
        } else {
            if ($this->input->post('approval_kasubdit') == 1 && $this->input->post('approval_pokjacan') == 1) {
                if ($this->input->post('status') == 'pending-add' || $this->input->post('status') == 'pending-edit') {
                    $data['status'] = 'ready';
                } elseif ($this->input->post('status') == 'pending-remove') {
                    $data['status'] = 'suspended';
                }
            }

            $data['approval_pokjacan'] = $this->input->post('approval_pokjacan');
            $data['cat_approval_pokjacan'] = $this->input->post('cat_approval_pokjacan');
            $data['userupdate']  = $this->session->userdata('userid');
            $data['dateupdate']    = date('Y-m-d H:i:s');

            if ($tipe == 'subkomponen') {
                $this->gm->save_data('rt_subkomponen', 'idsubkomponen', $id, $data);
                $this->save_log('sub_komponen', "approval-pokjacan", $id);
            } elseif ($tipe == 'item') {
                $this->gm->save_data('rt_item', 'iditem', $id, $data);
                $this->save_log('item', "approval-pokjacan", $id);
            }



            $validasi['success'] = true;
        }

        echo json_encode($validasi);
    }

    function lihat_rekam()
    {
        $jenis = $this->uri->segment(3);
        $id = $this->uri->segment(4);

        if ($jenis == 'subkomponen') {
            $namafile = 'lihat_rekam_subkomponen';
            $query = "
                select
                    sk.*,
                    case
                        when sk.approval_kasubdit = 0 then 'fa-square text-primary' 
                        when sk.approval_kasubdit = 1 then 'fa-check-square text-success' 
                        when sk.approval_kasubdit = 2 then 'fa-times-circle text-danger' 
                        else 'fa-square'
                    end as kasubdit,
                    case
                        when sk.approval_pokjacan = 0 then 'fa-square text-primary' 
                        when sk.approval_pokjacan = 1 then 'fa-check-square text-success' 
                        when sk.approval_pokjacan = 2 then 'fa-times-circle text-danger' 
                        else 'fa-square'
                    end as pokjacan,
                    u.fullname as user
                from rt_subkomponen_log sk 
                left join sys_user u on u.userid = sk.userupdate 
                where idsubkomponen = '$id' 
                order by sk.id desc
            ";
        } elseif ($jenis == 'item') {
            $namafile = 'lihat_rekam_item';
            $query = "
                select
                    sk.*,
                    case
                        when sk.approval_kasubdit = 0 then 'fa-square text-primary' 
                        when sk.approval_kasubdit = 1 then 'fa-check-square text-success' 
                        when sk.approval_kasubdit = 2 then 'fa-times-circle text-danger' 
                        else 'fa-square'
                    end as kasubdit,
                    case
                        when sk.approval_pokjacan = 0 then 'fa-square text-primary' 
                        when sk.approval_pokjacan = 1 then 'fa-check-square text-success' 
                        when sk.approval_pokjacan = 2 then 'fa-times-circle text-danger' 
                        else 'fa-square'
                    end as pokjacan,
                    u.fullname as user,
                    concat_ws('-', a.kdakun, a.nmakun) as akun
                from rt_item_log sk 
                left join sys_user u on u.userid = sk.userupdate 
                left join rm_akun a on a.idakun = sk.idakun 
                where iditem = '$id' 
                order by sk.id desc
            ";
        }

        $data['data']   = $this->gm->get_data($query);

        $this->load->view('pages/revisi_pok/' . $namafile, $data);
    }
}
