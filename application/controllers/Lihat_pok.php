<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lihat_pok extends CI_Controller {

	var $tmp_path = 'templates/index';
    var $main_path = 'pages/lihat_pok/';
	var $is_admin= false;

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('masuk') != TRUE){

            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">You need login first.</div>');

            redirect('auth/index');
        }
		
		$this->load->model(array('general_model' => 'gm','revisi_m'=>'rm'));
		
		 //cek role
        $userid = $this->session->userdata('userid');
        $q = "
            select 
                r.user_id as user_id,
                r.role as role,
                u.subkomponen_ids as subkomponen_ids
            from role r
            left join unit_kerja u on u.id = r.unit_kerja_id
            left join sys_user s on s.userid = r.user_id 
            where r.user_id = '$userid'
        ";
        $hasil = $this->gm->get_data($q)->row();
        $this->role = $hasil->role;
        $this->subkomponen_ids = $hasil->subkomponen_ids;

        $y = $this->gm->get_data("select * from state order by id desc")->row();

        $this->state = $y->nama;
        $this->is_admin = $this->role === 'admin' || $this->session->userdata('userid') === '1';

        
    }

    public function index()
    {    
     
		
		$data['title'] = 'SIAR / Lihat POK';
        $data["isActive"]   = 'Lihat POK';
        $data['page'] = $this->main_path . 'index';
        
		
        $query = "select * from rt_rka order by tahun desc";

        $data['rka'] = $this->gm->get_data($query);


        $query = "select idgiat, kdgiat, nmgiat from rm_giat
        order by kdgiat";

        $data['kegiatan'] = $this->gm->get_data($query);
		
        //$data["isActive"]   = 'revisi_anggaran';
       // $data['CSS_JS']     = asset_url('revisi_anggaran');
		 $data['is_admin'] = $this->is_admin;
		
		$this->load->view($this->tmp_path, $data);
		

    }
	
	function show_data()
	{
		//$idgiat = $this->input->get('x');
		//echo $idgiat.'test';
      
		$data["isActive"]   = 'revisi';
        
        
		$this->load->view($this->main_path.'list', $data);
        
    }
	
	public function get_data_by_json() {
        try{
			$idgiat = $this->input->post('idgiat');
			$tahun = $this->input->post('kdrka');
			//echo $id.'test';
			$data["isActive"]   = 'revisi';
			
			$q="select kdgiat from rm_giat where idgiat='$idgiat'";
			
			$kdgiat = $this->gm->get_data_by_id($q);
			
			$query = "CALL pr_rka_all_data_by_tahun(". $tahun. ",".$kdgiat->kdgiat.")";
			
			//echo $idgiat.$kdgiat->kdgiat.'test';
			//exit();
		
			$data	= $this->gm->get_datax($query);
			//print_r($data);
			
			print_r(json_encode($data));
			//exit();
			//redirect('alokasi');
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
}