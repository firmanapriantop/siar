<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class sedia_salur_pestisida extends CI_Controller {

	var $tmp_path = 'templates/index';
    var $main_path = 'pages/sedia_salur/';

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('masuk') != TRUE){

            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">You need login first.</div>');

            redirect('auth/index');
        }

        $this->load->model(array('general_model' => 'gm','sedia_salur_m'=>'sm'));
    }

    public function index()
    {    
     
		
		$data["isActive"]   = 'pengadaan dan penyaluran pestisida';
		$data['page'] = $this->main_path . 'index';
        
		$this->load->view($this->tmp_path, $data);
		

    }
	
	function show_data()
	{
      
		$data["isActive"]   = 'pengadaan dan penyaluran pestisida';
        //$perusahaan=$this->session->userdata('perusahaanid');
        
		//if ($this->session->userdata('role_id')=='3'){
		
			//$query = "
			//	select * from sedia_salur_vw  where perusahaanid=$perusahaan order by tahun desc
			//	";
		//}else {
		//	$query = "
         //   select * from sedia_salur_vw order by tahun desc
         //   ";		
		////}
		//echo $query;
		//$data['data']	= $this->gm->get_data($query);
		
		$this->load->view($this->main_path.'list', $data);
        
    }
	
	public function get_data_by_json() {
        try{
			//echo $id.'test';
			$data["isActive"]   = 'pengadaan dan penyaluran pestisida';
        
			 $perusahaan=$this->session->userdata('perusahaanid');
			
			if ($this->session->userdata('role_id')=='3'){
			
				$query = "
					select * from sedia_salur_vw  where perusahaanid=$perusahaan 
					";
			}else {
				$query = "
				select * from sedia_salur_vw 
				";		
			}


			$data	= $this->gm->get_datax($query);
			//print_r($data);
			print_r(json_encode($data));
			//exit();
			//redirect('alokasi');
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	public function add()
    {

        $data["isActive"]   = 'penyediaan dan penyaluran pestisida';
					
				
		$data['page'] = $this->main_path . 'add';
        
        $this->load->view($this->tmp_path, $data);
    }
	
	public function edit()
    {
		$id=$this->uri->segment(3);
		$data["isActive"]   = 'penyediaan dan penyaluran pestisida';
		
		$data['data'] = $this->sm->get_by_id($id);	

		$data['page'] = $this->main_path . 'edit';
        
		//echo $provid.$kabid.$kecid;
        $this->load->view($this->tmp_path, $data);
    }
	
	public function save(){
		try{
			//$data["content"] = 'content/alokasi/index';
			$data['isActive'] = 'penyediaan';
			
			$id=$this->uri->segment(3);
			$xData="";
						
			$dataForm = array(
						'stockawal' => $this->input->post('stock_awal'),
						'formulasiid' => $this->input->post('nama'),
						'stockakhir' => $this->input->post('stock_akhir'),
						'pengadaan' => $this->input->post('pengadaan'),
						'penyaluran' => $this->input->post('penyaluran'),
						'tahun' => $this->input->post('tahun'),
						'satuan' => $this->input->post('satuan'),
						'keterangan' => $this->input->post('keterangan')
					);
				if ($id==""){
					$dataForms = array(
						
						'created_by' => $this->session->userdata('userid'),
						'created_date' => date('Y-m-d H:m:s')
					);
				}else{
					$dataForms = array(
					
						'modified_by' => $this->session->userdata('userid'),
						'modified_date' => date('Y-m-d H:m:s')
					);
				}				
			
			$xData=array_merge($dataForm,$dataForms);
			
			//print_r($xData);
			$this->sm->set_data($id,$xData);
			redirect('sedia_salur_pestisida');
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
    }
	
	public function delete(){
		try{
			$id=$this->uri->segment(3);
			
			$this->sm->delete_data($id);			
			
			redirect('sedia_salur_pestisida');
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
    }
	
	public function get_formulas_jason()
	{
		try{
			$id = $this->input->get('perijinanid');
			
			$data = $this->sm->get_all_formulasi($id);	
			
				echo json_encode($data);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
		
	}
}