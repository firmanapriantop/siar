<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
	}

	public function index()
	{
		
		$data["isActive"]   = 'dashboard';
		$data['CSS_JS'] 	= asset_url('dashboard');
		$data["page"]    = 'dashboard/index';
		
		$this->load->view('templates/index', $data);
	}
	
	public function dashboard()
	{
		$data["isActive"]   = 'dashboard';
		$data['CSS_JS'] 	= asset_url('dashboard');
		$data["content"]    = 'dashboard/index';
		
		$this->load->view('templates/index', $data);
	}
}
