<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_per_wilayah extends CI_Controller {

	var $tmp_path = 'templates/index';
    var $main_path = 'pages/laporan/';

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('masuk') != TRUE){

            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">You need login first.</div>');

            redirect('auth/index');
        }

        $this->load->model(array('general_model' => 'gm','usulan_m'=>'um'));
    }

    public function index()
    {    
     
		
		$data["isActive"]   = 'Laporan per wilayah';
		$data['page'] = $this->main_path . 'index';
        
		$this->load->view($this->tmp_path, $data);
		

    }
	
	function show_data()
	{
      
		$data["isActive"]   = 'Laporan per wilayah';
        
        $query = "
            select * from usulan_vw
            ";

		$data['data']	= $this->gm->get_data($query);
		
		$this->load->view($this->main_path.'list', $data);
        
    }
	
	public function get_data_by_id_json() {
        try{
			$id = $this->input->get('provinsi_id');
			//echo $id.'test';
			
			$data = $this->gm->get_all_kab_by_provId($id);
			echo json_encode($data);
			//redirect('alokasi');
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
}