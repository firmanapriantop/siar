<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perijinan extends CI_Controller {

	var $tmp_path = 'templates/index';
    var $main_path = 'pages/perijinan/';

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('masuk') != TRUE){

            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">You need login first.</div>');

            redirect('auth/index');
        }

        $this->load->model(array('general_model' => 'gm','perijinan_m'=>'pm'));
    }

    public function index()
    {    
     
		
		$data["isActive"]   = 'perijinan';
		$data['page'] = $this->main_path . 'index';
        
		$this->load->view($this->tmp_path, $data);
		

    }
	
	function show_data()
	{
      
		$data["isActive"]   = 'perijinan';
        
        $query = "
            select * from tx_perijinan_vw order by tgl_akhir_ijin desc
            ";

		//$data['data']	= $this->gm->get_data($query);
		
		$this->load->view($this->main_path.'list', $data);
        
    }
	
	public function get_data_by_json() {
        try{
			//echo $id.'test';
			$data["isActive"]   = 'Perijinan';
        
        $query = "
            select * from tx_perijinan_vw order by tgl_akhir_ijin desc
            ";

			$data	= $this->gm->get_datax($query);
			//print_r($data);
			print_r(json_encode($data));
			//exit();
			//redirect('alokasi');
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	
	public function add()
    {

        $data["isActive"]   = 'perijinan';
					
				
		$data['page'] = $this->main_path . 'add';
        
        $this->load->view($this->tmp_path, $data);
    }
	
	public function edit()
    {
		$id=$this->uri->segment(3);
        $data["isActive"]   = 'perijinan';
		
		$data['data'] = $this->pm->get_by_id($id);	

		$data['page'] = $this->main_path . 'edit';
        
		//echo $provid.$kabid.$kecid;
        $this->load->view($this->tmp_path, $data);
    }
	
	public function save(){
		try{
			//$data["content"] = 'content/alokasi/index';
			$data['isActive'] = 'perijinan';
			
			$id=$this->uri->segment(3);
			$xData="";
			$tgl=tglSql($this->input->post('tanggal'));
						
			$dataForm = array(
						'no_ijin' => $this->input->post('nomor'),
						'formulasiid' => $this->input->post('nama'),
						'jenis_ijin' => $this->input->post('jenis'),
						'tgl_akhir_ijin' => $tgl,
						'keterangan' => $this->input->post('keterangan')
					);
				if ($id==""){
					$dataForms = array(
						
						'created_by' => $this->session->userdata('userid'),
						'created_date' => date('Y-m-d H:m:s')
					);
				}else{
					$dataForms = array(
					
						'modified_by' => $this->session->userdata('userid'),
						'modified_date' => date('Y-m-d H:m:s')
					);
				}				
			
			$xData=array_merge($dataForm,$dataForms);
			
			//print_r($xData);
			$this->pm->set_data($id,$xData);
			redirect('perijinan');
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
    }
	
	public function delete(){
		try{
			$id=$this->uri->segment(3);
			
			$this->pm->delete_data($id);			
			
			redirect('perijinan');
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
    }
	
	public function get_formulas_jason()
	{
		try{
			$data = $this->pm->get_all_formulasi();
				echo json_encode($data);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
		
	}
}