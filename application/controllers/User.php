<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

	var $tmp_path = 'templates/index';
	var $main_path = 'pages/user/';
	var $role = '';
	var $is_admin = false;

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		if ($this->session->userdata('masuk') != TRUE) {

			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">You need login first.</div>');

			redirect('auth/index');
		}

		$this->load->model(array('general_model' => 'gm', 'Sys_user_m' => 'user_m'));

		//cek role
		$userid = $this->session->userdata('userid');
		$q = "
             select *
             from role r
             where r.user_id = '$userid'
         ";
		$hasil = $this->gm->get_data($q)->row();
		$this->role = $hasil->role;
		$this->is_admin = $this->role === 'admin' || $this->session->userdata('userid') === '1';
	}

	public function index()
	{

		$data["isActive"]   = 'user';
		$data['title'] = 'SIAR / User ';
		$data['page'] = $this->main_path . 'index';

		$data['is_admin'] = $this->is_admin;

		$this->load->view($this->tmp_path, $data);
	}

	public function show_data()
	{
		if ($this->is_admin) {
			$query = "
            select * from sys_user
            ";

			$data['data']	= $this->gm->get_data($query);

			$this->load->view($this->main_path . 'list', $data);
		} else {
			$this->load->view('templates/not_role');
		}
	}

	function add()
	{
		$this->load->view($this->main_path . 'add');
	}

	function save()
	{
		$aksi = $this->uri->segment(3); //add or edit

		$validasi = array('success' => false, 'messages' => array());

		$this->form_validation->set_rules("fullname", "Nama Lengkap", "trim|required");
		if ($aksi == 'add') {
			$this->form_validation->set_rules("email", "Alamat Surel", "required|valid_email|is_unique[sys_user.email]", array('is_unique' => '%s sudah terdaftar. Silahkan menggunakan alamat surel lainnya.'));
		}
		$this->form_validation->set_rules("password", "Kata Kunci", "required|min_length[5]");
		$this->form_validation->set_rules("confirm_password", "Konfirmasi Kata Kunci", "required|matches[password]", array('matches' => '%s tidak sama.'));
		$this->form_validation->set_rules("no_hp", "Nomor Seluler", "trim|required");

		if ($this->form_validation->run() == false) {

			foreach ($_POST as $key => $value) {
				$validasi['messages'][$key] = form_error($key);
			}
		} else {
			if ($aksi == 'add') {
				$data['is_active'] = 1;
				$data['date_created']    = date('Y-m-d H:i:s');
				$id = 0;
				$data['password'] = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
			} elseif ($aksi == 'edit') {
				$id = $this->input->post('userid');
				$data['userid'] = $this->input->post('userid');
				//cek password lama
				$password_lama = $this->gm->get_data("select password from sys_user where userid='$id'")->row(1);
				$password_baru = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
				if ($password_lama->password == $password_baru) {
				} else {
					$data['password'] = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
				}
			}
			$data['fullname'] = $this->input->post('fullname');
			$data['email'] = $this->input->post('email');
			$data['no_hp'] = $this->input->post('no_hp');
			$data['image'] = 'default.png';
			$data['date_modified']    = date('Y-m-d H:i:s');

			$this->gm->save_data('sys_user', 'userid', $id, $data);
			$validasi['success'] = true;
		}

		echo json_encode($validasi);
	}

	public function edit()
	{
		$id             = $this->uri->segment(3);
		$data['data']   = $this->gm->get_data("SELECT * FROM sys_user WHERE userid = '$id'")->row();
		$this->load->view('pages/user/edit', $data);
	}

	public function hapus()
	{
		try {

			$id = $this->input->post('userid');
			$data['is_active'] = 0;
			$this->gm->save_data('sys_user', 'userid', $id, $data);
			echo "done";
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}
}
