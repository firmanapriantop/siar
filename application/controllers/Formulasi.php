<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formulasi extends CI_Controller {

	var $tmp_path = 'templates/index';
    var $main_path = 'pages/formulasi/';

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('masuk') != TRUE){

            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">You need login first.</div>');

            redirect('auth/index');
        }

        $this->load->model(array('general_model' => 'gm','formulasi_m'=>'fm'));
    }

    public function index()
    {    
     
		
		$data["isActive"]   = 'formulasi';
		$data['page'] = $this->main_path . 'index';
        
		$this->load->view($this->tmp_path, $data);
		

    }
	
	function show_data()
	{
      
		$data["isActive"]   = 'formulasi';
        
        $query = "
            select * from rt_formulasi_vw order by created_date desc
            ";

		//$data['data']	= $this->gm->get_data($query);
		
		$this->load->view($this->main_path.'list', $data);
        
    }
	
	public function get_data_by_json() {
        try{
			//echo $id.'test';
			$data["isActive"]   = 'formulasi';
        
         $query = "
            select * from rt_formulasi_vw order by created_date desc
            ";

			$data	= $this->gm->get_datax($query);
			//print_r($data);
			print_r(json_encode($data));
			//exit();
			//redirect('alokasi');
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	
	public function add()
    {

        $data["isActive"]   = 'formulasi';
					
		
		$data['get_jenis'] = $this->fm->get_all_jenis();	
		$data['get_penggunaan'] = $this->fm->get_all_penggunaan();	
		$data['get_perusahaan'] = $this->fm->get_all_perusahaan();	
		
		$data['page'] = $this->main_path . 'add';
        
        $this->load->view($this->tmp_path, $data);
    }
	
	public function edit()
    {
		$id=$this->uri->segment(3);
        $data["isActive"]   = 'usulan';
		
		$data['data'] = $this->fm->get_by_id($id);	

		
		$data['get_jenis'] = $this->fm->get_all_jenis();	
		$data['get_penggunaan'] = $this->fm->get_all_penggunaan();	
		$data['get_perusahaan'] = $this->fm->get_all_perusahaan();	
		
		$data['page'] = $this->main_path . 'edit';
        
		//echo $provid.$kabid.$kecid;
        $this->load->view($this->tmp_path, $data);
    }
	
	public function save(){
		try{
			//$data["content"] = 'content/alokasi/index';
			$data['isActive'] = 'usulan';
			
			$id=$this->uri->segment(3);
			$xData="";
			
						
			$dataForm = array(
						'no_pendaftaran' => $this->input->post('nomor'),
						'nama' => $this->input->post('nama'),
						'jenisformulaid' => $this->input->post('jenis'),
						'penggunaanid' => $this->input->post('penggunaan'),
						'perusahaanid' => $this->input->post('perusahaan'),
						'kategori' => $this->input->post('kategori'),
						'deskripsi' => $this->input->post('deskripsi'),
						'peringatan' => $this->input->post('peringatan')
					);
				if ($id==""){
					$dataForms = array(
						
						'created_by' => $this->session->userdata('userid'),
						'created_date' => date('Y-m-d H:m:s')
					);
				}else{
					$dataForms = array(
					
						'modified_by' => $this->session->userdata('userid'),
						'modified_date' => date('Y-m-d H:m:s')
					);
				}				
			
			$xData=array_merge($dataForm,$dataForms);
			
			//print_r($xData);
			$this->fm->set_data($id,$xData);
			redirect('formulasi');
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
    }
	
	public function delete(){
		try{
			$id=$this->uri->segment(3);
			
			$this->fm->delete_data($id);			
			
			redirect('formulasi');
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
    }
	
}