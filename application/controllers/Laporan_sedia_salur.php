<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_sedia_salur extends CI_Controller {

	var $tmp_path = 'templates/index';
    var $main_path = 'pages/laporan/sedia_salur/';

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('masuk') != TRUE){

            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">You need login first.</div>');

            redirect('auth/index');
        }

        $this->load->model(array('general_model' => 'gm'));
    }

    public function index()
    {    
     
		
		$data["isActive"]   = 'Laporan Pengadaan Dan Penyaluran';
		$data['page'] = $this->main_path . 'index';
        
		$this->load->view($this->tmp_path, $data);
		

    }
	
	function show_data()
	{
      
		$data["isActive"]   = 'Laporan Pengadaan Dan Penyaluran';
        
        //$query = "
        //    select * from sedia_salur_vw order by created_date, tahun desc
        //    ";

		//$data['data']	= $this->gm->get_data($query);
		
		$this->load->view($this->main_path.'list', $data);
        
    }
	
	public function get_data_by_json() {
        try{
			//echo $id.'test';
			$data["isActive"]   = 'Laporan Pengadaan Dan Penyaluran';
        
        $query = "
            select * from sedia_salur_vw
            ";

			$data	= $this->gm->get_datax($query);
			//print_r($data);
			print_r(json_encode($data));
		//	exit();
			//redirect('alokasi');
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
}