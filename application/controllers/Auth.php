<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
	}

	public function index()
	{

		if ($this->session->userdata('masuk') != TRUE) {

			$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
			$this->form_validation->set_rules('password', 'Password', 'required|trim');

			if ($this->form_validation->run() == false) {
				$data['title'] = 'SIAR / Login';

				$this->load->view('layout/auth_header', $data);
				$this->load->view('auth/login');
				$this->load->view('layout/auth_footer');
			} else {
				$this->_login();
			}
		} else {
			redirect('dashboard');
		}
	}

	private function _login()
	{
		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$user = $this->db->get_where('sys_user_vw', ['email' => $email])->row_array();

		if ($user) {
			//user exist
			if ($user['is_active'] == 1) {
				//cek password
				if (password_verify($password, $user['password'])) {

					$data = [
						'userid' => $user['userid'],
						'fullname'	=> $user['fullname'],
						'image'	=> $user['image'],
						'email' => $user['email'],
						'role_id' => $user['role_id'],
						'masuk' => true
					];

					$this->session->set_userdata($data);

					redirect('dashboard');
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Wrong password.</div>');

					redirect('auth');
				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">This email has not been activated.</div>');

				redirect('auth');
			}
		} else {

			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email is not registered</div>');

			redirect('auth');
		}
	}

	public function registration()
	{
		$this->form_validation->set_rules('fullname', 'Full Name', 'required|trim');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[sys_user.email]', ['is_unique' => 'This email has already registered']);
		$this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[3]|matches[rpassword]', ['matches' => 'Password dont match', 'min_length' => 'Password too short']);
		$this->form_validation->set_rules('rpassword', 'Password', 'required|trim|matches[password]');

		if ($this->form_validation->run() == false) {
			$data['title'] = 'MUDSCODE User Registration';
			$this->load->view('layout/auth_header', $data);
			$this->load->view('auth/registration');
			$this->load->view('layout/auth_footer');
		} else {
			$data = [
				'fullname' => htmlspecialchars($this->input->post('fullname', true)),
				'email' => htmlspecialchars($this->input->post('email', true)),
				'image' => 'default.png',
				'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
				'role_id' => 2, //staff
				'is_active' => 1,
				'date_created' => time()
			];

			$this->db->insert('sys_user', $data);

			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Congratulation! You account has been created. Please login!</div>');

			redirect('auth');
		}
	}

	public function logout()
	{
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('role_id');
		$this->session->unset_userdata('masuk');
		$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">You have been logout.</div>');

		redirect('auth');
	}
}
