<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Akun extends CI_Controller
{

    var $tmp_path = 'templates/index';
    var $main_path = 'pages/akun/';

    var $is_admin = false;

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('masuk') != TRUE) {

            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">You need login first.</div>');

            redirect('auth/index');
        }

        $this->load->model(array('general_model' => 'gm', 'akun_m' => 'am'));

        //cek role
        $userid = $this->session->userdata('userid');
        $q = "
            select 
                r.user_id as user_id,
                r.role as role,
                u.subkomponen_ids as subkomponen_ids
            from role r
            left join unit_kerja u on u.id = r.unit_kerja_id
            left join sys_user s on s.userid = r.user_id 
            where r.user_id = '$userid'
        ";
        $hasil = $this->gm->get_data($q)->row();
        $this->role = $hasil->role;
        $this->subkomponen_ids = $hasil->subkomponen_ids;

        $y = $this->gm->get_data("select * from state order by id desc")->row();

        $this->state = $y->nama;
        $this->is_admin = $this->role === 'admin' || $this->session->userdata('userid') === '1';
    }

    public function index()
    {


        $data['title'] = 'SIAR / Akun';
        $data["isActive"]   = 'Akun';
        $data['page'] = $this->main_path . 'index';

        $data['is_admin'] = $this->is_admin;


        $this->load->view($this->tmp_path, $data);
    }

    function show_data()
    {
        //$idgiat = $this->input->get('x');
        //echo $idgiat.'test';

        $data["isActive"]   = 'Akun';


        $this->load->view($this->main_path . 'list', $data);
    }

    public function get_data_by_json()
    {
        try {

            //echo $id.'test';
            $data["isActive"]   = 'Akun';


            $query = "select * from rm_akun";


            $data    = $this->gm->get_datax($query);
            //print_r($data);

            print_r(json_encode($data));
            //exit();
            //redirect('alokasi');
        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }
    }

    function add()
    {
        $this->load->view($this->main_path . 'add');
    }

    function edit()
    {
        $id             = $this->uri->segment(3);

        $data['data']   = $this->gm->get_data("select * from rm_akun where idakun ='$id'")->row();

        $this->load->view('pages/akun/edit', $data);
    }

    function save()
    {
        $aksi = $this->uri->segment(3); //add or edit

        $validasi = array('success' => false, 'messages' => array());
        $this->form_validation->set_rules("kdakun", "User ", "required");
        $this->form_validation->set_rules("nmakun", "Keterangan", "required");

        if ($aksi == 'add') {
            $this->form_validation->set_rules("kdakun", "User ", "required|is_unique[rm_akun.kdakun]");
        }

        if ($this->form_validation->run() == false) {

            foreach ($_POST as $key => $value) {
                $validasi['messages'][$key] = form_error($key);
            }
        } else {
            if ($aksi == 'add') {
                $id = 0;
                $data['userinput'] = $this->session->userdata('userid');
                $data['dateinput']    = date('Y-m-d H:i:s');
            } elseif ($aksi == 'edit') {
                $id = $this->input->post('idakun');
                $data['idakun'] = $this->input->post('idakun');
            }

            $data['kdakun'] = $this->input->post('kdakun');
            $data['nmakun'] = $this->input->post('nmakun');
            $data['userupdate'] = $this->session->userdata('userid');
            $data['dateupdate']    = date('Y-m-d H:i:s');

            $this->gm->save_data('rm_akun', 'idakun', $id, $data);
            $validasi['success'] = true;
        }

        echo json_encode($validasi);
    }
}
