<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dokumen_koordinasi extends CI_Controller
{

    var $tmp_path = 'templates/index';
    var $main_path = 'pages/dokumen_koordinasi/';
    var $role = '';

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('masuk') != TRUE) {

            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">You need login first.</div>');

            redirect('auth/index');
        }

        $this->load->model(array('general_model' => 'gm', 'dokumen_koordinasi_m' => 'dkm'));

        //cek role
        $userid = $this->session->userdata('userid');
        $q = "
             select *
             from role r
             where r.user_id = '$userid'
         ";
        $hasil = $this->gm->get_data($q)->row();
        $this->role = $hasil->role;
    }

    public function index()
    {

        $data['title'] = 'SIAR / Dokumen Koordinasi';
        $data["isActive"]   = 'Dokumen Koordinasi';
        $data['page'] = $this->main_path . 'index';

        $data['can_edit'] = $this->role === 'staf' || $this->role === 'admin';
        $data['is_admin'] = $this->role === 'admin' || $this->session->userdata('userid') === '1';

        $this->load->view($this->tmp_path, $data);
    }

    function show_data()
    {

        $data["isActive"]   = 'Laporan per wilayah';

        $query = "
                    select * from dokumen_koordinasi
            ";

        $data['data']    = $this->gm->get_data($query);

        $data['can_edit'] = $this->role === 'staf' || $this->role === 'admin';

        $this->load->view($this->main_path . 'list', $data);
    }

    public function add()
    {

        $this->load->view('pages/dokumen_koordinasi/add');
    }

    function save()
    {
        $aksi = $this->uri->segment(3); //add or edit

        $validasi = array('success' => false, 'messages' => array());

        $this->form_validation->set_rules("kategori", "Kategori Dokumen", "trim|required");
        $this->form_validation->set_rules("nomor", "Nomor Dokumen", "trim|required");
        $this->form_validation->set_rules("perihal", "Perihal Dokumen", "trim|required");
        $this->form_validation->set_rules("dari", "Dari", "trim|required");
        //$this->form_validation->set_rules("tanggal", "Tanggal Dokumen", "trim|required");
        //$this->form_validation->set_rules("file", "Unggahan File", "trim|required");
        $this->form_validation->set_rules("keterangan", "Keterangan", "trim|required");

        $file = $this->input->post('file');
        $butuh_upload = true;

        if ($this->form_validation->run() == false) {

            foreach ($_POST as $key => $value) {
                $validasi['messages'][$key] = form_error($key);
            }
        } else {
            if ($aksi == 'add') {
                $data['userinput']  = $this->session->userdata('userid');
                $data['dateinput']    = date('Y-m-d H:i:s');
                $id = 0;

                $butuh_upload = true;
            } elseif ($aksi == 'edit') {
                $id = $this->input->post('id');
                if (!empty($this->input->post('file'))) {
                    $butuh_upload = false;
                }
            }

            if ($butuh_upload) {
                //upload file
                $config['upload_path'] = "./uploads/dokumen_koordinasi/";
                $config['allowed_types'] = '*';
                //$config['allowed_types'] = 'gif|jpg|png|pdf'; 
                //$config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);
                if ($this->upload->do_upload('file')) {
                    //$data = array('upload_data' => $this->upload->data());
                    $upload = $this->upload->data();
                    //$validasi['messages']['file'] = $_FILES['file']['name'];

                    $data['kategori'] = $this->input->post('kategori');
                    $data['nomor'] = $this->input->post('nomor');
                    $data['perihal'] = $this->input->post('perihal');
                    $data['dari'] = $this->input->post('dari');
                    $data['tanggal'] = date_format(date_create($this->input->post('tanggal')), "Y-m-d");
                    $data['file'] = $upload['file_name'];
                    $data['keterangan'] = $this->input->post('keterangan');
                    $data['userupdate']  = $this->session->userdata('userid');
                    $data['dateupdate']    = date('Y-m-d H:i:s');

                    $this->gm->save_data('dokumen_koordinasi', 'id', $id, $data);


                    $validasi['success'] = true;
                } else {
                    $validasi['messages']['file'] = $this->upload->display_errors();
                    $validasi['success'] = false;
                }
            } else {
                $data['id'] = $this->input->post('id');
                $data['kategori'] = $this->input->post('kategori');
                $data['nomor'] = $this->input->post('nomor');
                $data['perihal'] = $this->input->post('perihal');
                $data['dari'] = $this->input->post('dari');
                $data['tanggal'] = date_format(date_create($this->input->post('tanggal')), "Y-m-d");
                $data['keterangan'] = $this->input->post('keterangan');
                $data['userupdate']  = $this->session->userdata('userid');
                $data['dateupdate']    = date('Y-m-d H:i:s');



                $this->gm->save_data('dokumen_koordinasi', 'id', $id, $data);
                $validasi['success'] = true;
            }
        }

        echo json_encode($validasi);
    }

    public function savess()
    {
        try {
            //cek upload files

            //$data["content"] = 'content/alokasi/index';
            $data['isActive'] = 'dokumen_koordnasi';

            $id = $this->uri->segment(3);
            $xData = "";
            $tgl = tglSql($this->input->post('tanggal'));

            $dataForm = array(
                'kategori' => $this->input->post('kategori'),
                'nomor' => $this->input->post('nomor'),
                'perihal' => $this->input->post('perihal'),
                'dari' => $this->input->post('dari'),
                'tanggal' => $tgl,
                'keterangan' => $this->input->post('keterangan')
            );

            if ($id == "") {
                $dataForms = array(

                    'created_by' => $this->session->userdata('userid'),
                    'created_date' => date('Y-m-d H:m:s')
                );
            } else {
                $dataForms = array(

                    'modified_by' => $this->session->userdata('userid'),
                    'modified_date' => date('Y-m-d H:m:s')
                );
            }

            $xData = array_merge($dataForm, $dataForms);

            //print_r($xData);
            $this->dkm->set_data($id, $xData); //save data to table

            redirect('dokumen_koordinasi');
        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }
    }
    function upload()
    {
        $validasi = array('success' => false, 'messages' => array());
        $nama_file = $this->input->post('nama_file');


        if (empty($this->input->post('nama_file'))) {

            $msg = "Please enter a title";
        } else {

            $config['upload_path'] = './uploads/dokumen_koordinasi/';
            //$config['allowed_types'] = 'pdf';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload($nama_file)) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {
                $data = $this->upload->data();
                $validasi['success'] = true;
                $msg = $data['file_name'] . "File successfully uploaded";
            }
            @unlink($_FILES['userfile']);
        }

        $validasi['messages']['nama_file'] = $msg;

        echo json_encode($validasi);
    }

    public function upload_file()
    {
        $status = "";
        $msg = "";
        $file_element_name = 'input_gambar';

        /*
        if (empty($_POST['title'])) {
            $status = "error";
            $msg = "Please enter a title";
        }
*/

        if ($status != "error") {
            $config['upload_path'] = './uploads/dokumen_koordinasi/';
            $config['allowed_types'] = 'jpg|png|pdf';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload($file_element_name)) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {
                $data = $this->upload->data();
                $status = "success";
                $msg = $data['file_name'] . "File successfully uploaded";
            }
            @unlink($_FILES[$file_element_name]);
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
    }

    public function edit()
    {
        $id             = $this->uri->segment(3);
        $data['data']   = $this->gm->get_data("SELECT * FROM dokumen_koordinasi WHERE id = '$id'")->row();
        $this->load->view('pages/dokumen_koordinasi/edit', $data);
    }

    public function hapus()
    {
        try {

            $id = $this->input->post('id');
            $this->gm->hapus_data("dokumen_koordinasi", "id", $id);
            echo "done";
        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }
    }
}
