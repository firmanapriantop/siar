<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data["content"]    = 'layout/content';
		$data["isActive"]   = 'dashboard';
		$data['CSS_JS'] 	= asset_url('dashboard');
		//echo $css_js;
		$this->load->view('layout/index', $data);
	}
	
	public function asli()
	{
		$this->load->view('template/ui_tiles');
		//asd
	}
	
	public function ok()
	{
		$this->load->view('template/a-header.php');
		$this->load->view('template/b-menu.php');
		$this->load->view('template/content.php');
		$this->load->view('template/footer.php');
	}
}
