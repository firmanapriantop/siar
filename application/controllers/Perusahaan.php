<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perusahaan extends CI_Controller {

	var $tmp_path = 'templates/index';
    var $main_path = 'pages/perusahaan/';

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('masuk') != TRUE){

            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">You need login first.</div>');

            redirect('auth/index');
        }

        $this->load->model(array('general_model' => 'gm','perusahaan_m'=>'pm'));
    }

    public function index()
    {    
     
		
		$data["isActive"]   = 'perusahaan';
		$data['page'] = $this->main_path . 'index';
        
		$this->load->view($this->tmp_path, $data);
		

    }
	
	function show_data()
	{
      
		//$data["isActive"]   = 'perusahaan';
         $query = "
            select * from ms_perusahaan_vw order by created_date desc
            ";

		$data['data']	= $this->gm->get_data($query);
		
		
		$this->load->view($this->main_path.'list',$data);
        
    }
	
	function show_data_json()
	{
      
		//$data["isActive"]   = 'perusahaan';
        
        $query = "
            select * from ms_perusahaan_vw order by created_date desc
            ";

		$data['data']	= $this->gm->get_data($query);
		print_r ($data['data']).'test';
		 exit();
		 //echo json_encode($data);
		//$this->load->view($this->main_path.'list', $data);
        
    }
	
  	
	public function add()
    {

        $data["isActive"]   = 'perusahaan';
		//$data['CSS_JS'] 	= asset_url('dashboard');
		//$data["content"]    = 'user/add';
					
		$data['get_prov'] = $this->gm->get_all_prov();		
		$data['page'] = $this->main_path . 'add';
        
        $this->load->view($this->tmp_path, $data);
    }
	
	public function edit()
    {
		$id=$this->uri->segment(3);
        $data["isActive"]   = 'perusahaan';
		
		$data['data'] = $this->pm->get_by_id($id);	
		$data['get_prov'] = $this->gm->get_all_prov();
		
		$provid=$data['data']->provid;
		
		$data['get_kab'] = $this->gm->get_all_kab_by_provId($provid);
		$kabid=$data['data']->kabid;
		
		
		$data['page'] = $this->main_path . 'edit';
        
        $this->load->view($this->tmp_path, $data);
    }
	
	public function save(){
		try{
			//$data["content"] = 'content/alokasi/index';
			$data['isActive'] = 'perusahaan';
			
			$id=$this->uri->segment(3);
			
			$dataForm = array(
						'nama' => $this->input->post('nama'),
						'alamat' => $this->input->post('alamat'),
						'kode_pos' => $this->input->post('kodepos'),
						'email' => $this->input->post('email'),
						'no_telp' => $this->input->post('phone'),
						'no_fax' => $this->input->post('fax'),
						'kabid' => $this->input->post('kabupaten')
						);
			
				if ($id==""){
					$dataForms = array(
						'created_date' => date('Y-m-d H:m:s')
					);
					
					$xData=array_merge($dataForm,$dataForms);
				}else {
					$xData=array_merge($dataForm);
						
				}
			
			//echo 'test'.$id;
			//print_r($xData);
			$this->pm->set_data($id,$xData);
			redirect('perusahaan');
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
    }
	
	public function delete(){
		try{
			$id=$this->uri->segment(3);
			
			$this->pm->delete_data($id);			
			
			redirect('perusahaan');
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
    }
	
	public function get_data_by_id_json() {
        try{
			$id = $this->input->get('desaid');
			//echo $id.'test';
			
			$data = $this->gm->get_all_debitur_by_desaId($id);
			echo json_encode($data);
			//redirect('alokasi');
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
}