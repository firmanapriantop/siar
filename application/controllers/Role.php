<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Role extends CI_Controller
{

	var $tmp_path = 'templates/index';
	var $main_path = 'pages/role/';
	var $role = '';
	var $is_admin = false;

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		if ($this->session->userdata('masuk') != TRUE) {

			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">You need login first.</div>');

			redirect('auth/index');
		}

		$this->load->model(array('general_model' => 'gm', 'Sys_user_m' => 'user_m'));

		$userid = $this->session->userdata('userid');
		$q = "
             select *
             from role r
             where r.user_id = '$userid'
         ";
		$hasil = $this->gm->get_data($q)->row();
		$this->role = $hasil->role;

		$this->is_admin = $this->role === 'admin' || $this->session->userdata('userid') === '1';
	}

	public function index()
	{


		$data["isActive"]   = 'role';
		//$data['CSS_JS'] 	= asset_url('user');
		// $data["content"]    = 'user/index';

		$data['title'] = 'SIAR / User Role ';

		$data['page'] = $this->main_path . 'index';
		$data['is_admin'] = $this->is_admin;

		$this->load->view($this->tmp_path, $data);
	}

	function load_role()
	{
		$query = "
			select 
				r.id,
				u.fullname,
				u.email,
				r.role,
				uk.nama as unit_kerja,
				uk.subkomponen_ids
			from role r 
			left join sys_user u on u.userid = r.user_id 
			left join unit_kerja uk on uk.id = r.unit_kerja_id 
			";
		$data['data'] = $this->gm->get_data($query);

		$this->load->view($this->main_path . 'list', $data);
	}

	function load_sub_komponen()
	{

		$query	=
			"
		select
			concat_ws('.', a5.kdgiat, a4.kdoutput, a3.kdsuboutput, a2.kdkomponen, a1.kdsubkomponen) as kode,
			a1.ursubkomponen as nama,
			a1.
		from rt_subkomponen a1
		left join rt_komponen a2 on a2.idkomponen = a1.idkomponen
		left join rt_suboutput a3 on a3.idsuboutput = a2.idsuboutput
		left join rm_output a4 on a4.idoutput = a3.idoutput
		left join rm_giat a5 on a5.idgiat = a5.idgiat
		";

		echo "adalah";
	}

	function add_role()
	{
		$query_user = "
			select * from sys_user order by fullname
		";

		$quey_sub_komponen = "
			select
				a1.idsubkomponen,
				concat_ws('.', a5.kdgiat, a4.kdoutput, a3.kdsuboutput, a2.kdkomponen, a1.kdsubkomponen) as kode,
				a1.ursubkomponen as nama
			from rt_subkomponen a1
			left join rt_komponen a2 on a2.idkomponen = a1.idkomponen
			left join rt_suboutput a3 on a3.idsuboutput = a2.idsuboutput and a3.idsatker = '985'
			left join rm_output a4 on a4.idoutput = a3.idoutput
			left join rm_giat a5 on a5.idgiat = a4.idgiat
			left join rm_satker sat on sat.idsatker = a3.idsatker
			where sat.kdsatker = '633656'
		";

		$data['user'] = $this->gm->get_data($query_user);
		$data['unit_kerja'] = $this->gm->get_data("select * from unit_kerja");

		$this->load->view($this->main_path . 'add', $data);
	}

	function save()
	{
		$aksi = $this->uri->segment(3); //add or edit

		$validasi = array('success' => false, 'messages' => array());
		if ($aksi == 'add') {
			$this->form_validation->set_rules("user_id", "User ", "required|is_unique[role.user_id]", array('is_unique' => '%s ini telah digunakan.'));
		}
		$this->form_validation->set_rules("role", "Peran", "required");


		if ($this->form_validation->run() == false) {

			foreach ($_POST as $key => $value) {
				$validasi['messages'][$key] = form_error($key);
			}
		} else {
			if ($aksi == 'add') {
				$id = 0;
			} elseif ($aksi == 'edit') {
				$id = $this->input->post('id');
				$data['id'] = $this->input->post('id');
			}

			$data['user_id'] = $this->input->post('user_id');
			$data['role'] = $this->input->post('role');
			$data['unit_kerja_id'] = $this->input->post('unit_kerja_id');

			$this->gm->save_data('role', 'id', $id, $data);
			$validasi['success'] = true;
		}

		echo json_encode($validasi);
	}

	public function edit()
	{
		$id             = $this->uri->segment(3);

		$query = "
			select 
				r.id, 
				r.user_id, 
				r.role,
				r.unit_kerja_id,
				su.fullname, 
				su.email,
				uk.nama as unit_kerja
			from
				role r
			left join sys_user su on su.userid = r.user_id
			left join unit_kerja uk on uk.id = r.unit_kerja_id
			where r.id = '$id'
		";
		$data['data']   = $this->gm->get_data($query)->row();

		$data['user'] = $this->gm->get_data("select * from sys_user order by fullname");

		$data['unit_kerja'] = $this->gm->get_data("select * from unit_kerja");

		$this->load->view('pages/role/edit', $data);
	}

	public function hapus()
	{
		try {

			$id = $this->input->post('id');
			$this->gm->hapus_data('role', 'id', $id);
			echo "done";
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}
}
