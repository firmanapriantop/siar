<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Revisi_anggaran extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('general_model' => 'gm'));

        if ($this->session->userdata('masuk') != TRUE) {

            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">You need login first.</div>');

            redirect('auth/index');
        }
    }

    public function index()
    {
        $data['title'] = 'SIAR / Revisi Anggaran';
        $data["isActive"]   = 'revisi_anggaran';
        $data['CSS_JS']     = asset_url('revisi_anggaran');
        $data['page'] = 'templates/content_empty';

        $this->load->view('templates/index', $data);
    }
}
