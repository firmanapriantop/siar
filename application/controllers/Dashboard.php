<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

	var $tmp_path = 'templates/index';
	var $main_path = 'pages/dashboard/';
	var $role = '';
	var $is_admin = false;
	var $subkomponen_ids = '';
	var $unit_kerja = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('general_model' => 'gm'));

		if ($this->session->userdata('masuk') != TRUE) {

			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">You need login first.</div>');

			redirect('auth/index');
		}

		$userid = $this->session->userdata('userid');
		$q = "
             select *
			 from role r 
			 left join unit_kerja uk on uk.id = r.unit_kerja_id
             where r.user_id = '$userid'
         ";
		$hasil = $this->gm->get_data($q)->row();
		$this->role = $hasil->role;
		$this->subkomponen_ids = $hasil->subkomponen_ids;
		$this->unit_kerja = $hasil->nama;

		$this->is_admin = $this->role === 'admin' || $this->session->userdata('userid') === '1';
	}

	public function index()
	{
		$data["isActive"]   = 'role';
		//$data['CSS_JS'] 	= asset_url('user');
		// $data["content"]    = 'user/index';

		$data['title'] = 'SIAR / Dashboard ';

		$data['page'] = $this->main_path . 'index';
		$data['is_admin'] = $this->is_admin;

		$data['subkomponen_ids'] = $this->subkomponen_ids;

		if (is_null($this->subkomponen_ids)) {
			$where = " != ''";
		} else {
			$where = " in ($this->subkomponen_ids)";
		}

		//load nama sub komponen
		$data['nmsubkomponen'] = $this->gm->get_data("select distinct(concat(kdgiat,'.',kdoutput,'.',kdsuboutput,'.',kdkomponen,'.',kdsubkomponen,' - ',ursubkomponen)) as nmsubkomponen from vw_rka_trans where idsubkomponen $where");
		$data['unit_kerja'] = $this->unit_kerja;

		//load alokasi
		$data['total_alokasi'] = $this->gm->get_data("select sum(jumlah) as jlh from vw_rka_trans where idsubkomponen $where")->row('jlh');

		//info box revisi ke sub komponen

		if (empty($this->gm->get_data("select max(revisi_ke) as max from rt_subkomponen_tmp where idsubkomponen $where")->row('max'))) {
			$data['revisi_ke'] = 0;
		} else {
			$data['revisi_ke'] = $this->gm->get_data("select max(revisi_ke) as max from rt_subkomponen_tmp where idsubkomponen $where")->row('max') + 1;
		}

		$query = "
			select 
				count(*) as jlh
			from rt_subkomponen 
			where status in ('pending-add', 'pending-edit', 'pending-remove') 
		";
		$data['total_sub_komponen'] = $this->gm->get_data($query)->row('jlh');

		//revisi ke
		$query = "
			select 
			count(*) as jlh
			from rt_item 
			where status in ('pending-add', 'pending-edit', 'pending-remove') and 
			idsubkomponen $where
		";

		$data['total_item'] = $this->gm->get_data($query)->row('jlh');

		//load belanja akun
		$query = "
			select 
				concat_ws('-', a.kdakun, a.nmakun) as akun,
				a.kdakun as kdakun,
				(sum(i.jumlah)/1000000) as jumlah
			from rt_item i
			left join rm_akun a on a.idakun = i.idakun 
			left join rt_subkomponen sk on sk.idsubkomponen = i.idsubkomponen 
			where i.idsubkomponen $where
			group by i.idakun 
			order by jumlah desc 
			limit 10
		";
		$hasil = $this->gm->get_data($query);
		foreach ($hasil->result() as $i) :
			$cat[] = $i->kdakun;
			$nama[] = $i->akun;
			$nilai[] = (float) $i->jumlah;
		endforeach;

		$data['cat'] = $cat;
		$data['nama'] = $nama;
		$data['nilai'] = $nilai;

		$this->load->view($this->tmp_path, $data);
	}

	public function load_info_box()
	{
		$tahun = $this->uri->segment(3);

		//if admin
		$userid = $this->session->userdata('userid');
		$perusahaanid = $this->session->userdata('perusahaanid');

		if ($this->session->userdata('role_id') == '1' || $this->session->userdata('role_id') == '2') {
			$WHERE = ' where year(t.tgl_akhir_ijin) >= ' . $tahun . '';
		} else {
			$WHERE = ' where year(t.tgl_akhir_ijin) > ' . $tahun . ' and r.perusahaanid = ' . $perusahaanid . '';
		}

		$query =
			"select 
				sum(jlh) as jlh 
			from 
			(select 
				year(t.tgl_akhir_ijin) as tgl_akhir_ijin, r.perusahaanid, count(r.formulasiid) as jlh
			from rt_formulasi r
			left join tx_perijinan t on t.formulasiid = r.formulasiid
			$WHERE
			group by t.tgl_akhir_ijin, r.perusahaanid) w
			";

		$data['jlh_formulasi'] = $this->gm->get_data($query)->row('jlh');

		$query =
			"select 
			tahun as tahun, 
			case
				when satuan = 1 then 'KG'
				when satuan = 2 then 'LTR'
			end as satuan, sum(stockawal) as jlh_stock, sum(pengadaan) as jlh_pengadaan, sum(penyaluran) as jlh_penyaluran
			from tx_sedia_salur_pestisida t 
			where (satuan  = 1 or satuan = 2) and tahun = $tahun
			group by tahun, satuan
			";

		$data['jlh_byk'] = $this->gm->get_data($query);

		$this->load->view('dashboard/info_box', $data);
	}

	public function load_bar_chart()
	{
		$tahun = $this->uri->segment(3);

		//if admin
		$userid = $this->session->userdata('userid');
		$perusahaanid = $this->session->userdata('perusahaanid');

		if ($this->session->userdata('role_id') == '1' || $this->session->userdata('role_id') == '2') {
			$WHERE = ' where year(t.tgl_akhir_ijin) >= ' . $tahun . '';
			$WHERE2 = ' where t.tahun = ' . $tahun . '';
		} else {
			$WHERE = ' where year(t.tgl_akhir_ijin) > ' . $tahun . ' and r.perusahaanid = ' . $perusahaanid . '';
			$WHERE2 = ' where t.tahun = ' . $tahun . ' and r.perusahaanid = ' . $perusahaanid . '';
		}

		$query =
			"select 
				 count(r.formulasiid) as jlh
			from rt_formulasi r
			left join tx_perijinan t on t.formulasiid = r.formulasiid
			$WHERE
			";

		$data['jlh_formulasi'] = $this->gm->get_data($query)->row('jlh');

		$query =
			"select 
				count(t.formulasiid) as jlh 
			from 
				tx_sedia_salur_pestisida t
			left join rt_formulasi r on r.formulasiid = t.formulasiid
			$WHERE2
			";

		$data['jlh_formulasi_terlapor'] = $this->gm->get_data($query)->row('jlh');

		$query =
			"select 
				count(distinct(r.perusahaanid)) as jlh
			from rt_formulasi r
			left join tx_perijinan t on t.formulasiid = r.formulasiid
			$WHERE
			";

		$data['jlh_perusahaan'] = $this->gm->get_data($query)->row('jlh');

		$query =
			"select 
				 count(distinct(r.perusahaanid)) as jlh 
			from 
				tx_sedia_salur_pestisida t
			left join rt_formulasi r on r.formulasiid = t.formulasiid
			$WHERE2
			";

		$data['jlh_perusahaan_terlapor'] = $this->gm->get_data($query)->row('jlh');


		$this->load->view('dashboard/bar_chart', $data);
	}

	public function load_line_chart()
	{
		$tahun = $this->uri->segment(3);

		//if admin
		$userid = $this->session->userdata('userid');
		$kabid = $this->session->userdata('kabid');

		if ($this->session->userdata('role_id') == '1' || $this->session->userdata('role_id') == '2') {

			$WHERE = 'where tahun_date = ' . $tahun . '';
		} else {

			$WHERE = 'where createdby = ' . $userid . ' and tahun_date = ' . $tahun . '';
		}

		$query =
			"select 
				bulan, 
				case
					when bulan = 1 then 'Januari' 
					when bulan = 2 then 'Februari' 
					when bulan = 3 then 'Maret' 
					when bulan = 4 then 'April' 
					when bulan = 5 then 'Mei'
					when bulan = 6 then 'Juni' 
					when bulan = 7 then 'Juli' 
					when bulan = 8 then 'Agustus' 
					when bulan = 9 then 'September' 
					when bulan = 10 then 'Oktober' 
					when bulan = 11 then 'November' 
					when bulan = 12 then 'Desember' 
				end as bulan2,
				tahun_date, 
				createdby, 
				count(usulanid) as jlh 
			from 
				(select 
				month(created_date) as bulan, 
				year(created_date) as tahun_date, 
				createdby, 
				usulanid
			from tx_usulan) as u
			$WHERE
			group by bulan, tahun_date, createdby
			";

		$data['hasil_line'] = $this->gm->get_data($query);

		$this->load->view('dashboard/line_chart', $data);
	}
}
